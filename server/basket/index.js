const Moment = require('moment')
const MomentRange = require('moment-range')

const moment = MomentRange.extendMoment(Moment)

class Basket {

  constructor (soket) {
    this.soket = soket
    this.baskets = []
    this.init()
  }

  changeStatus (time, timeEnd) {
    return moment().isBetween(time, timeEnd) ? 'wait' : 'fail'
  }

  add (data) {
    this.baskets.push({
      ...data,
      time: moment().toDate(),
      timeEnd: moment().add(5, 'm').toDate(),
      status: 'wait', coefficient: 0
    })
  }

  get () {
    return this.baskets
  }

  done (key) {
    if (this.baskets[key]) {
      this.baskets[key].status = 'done'
      this.baskets[key].basket.coefficient = this.baskets[key].coefficient
      this.soket.emit('doneBasket', this.baskets[key])
      this.baskets = this.baskets.filter(basket => basket.status !== 'done')
      this.soket.emit('getBasket', this.get())
    }
  }

  set (data) {
    this.baskets[data.key].basket.events[data.index].coefficient = +data.val
    this.baskets[data.key].coefficient = 0
    this.baskets[data.key].basket.events.forEach((event, index) => {
      if (index === 0) {
        this.baskets[data.key].coefficient = event.coefficient
      } else {
        this.baskets[data.key].coefficient *= event.coefficient
      }
      this.baskets[data.key].coefficient = this.baskets[data.key].coefficient.toFixed(2)
    })
    this.soket.emit('getBasket', this.get())
  }

  init () {
    setInterval(() => {
      if (this.baskets.length > 0) {
        this.baskets = this.baskets.map(basket => ({...basket, status: this.changeStatus(basket.time, basket.timeEnd)}))

        this.baskets.forEach(basket => {
          if (basket.status === 'fail') {
            this.soket.emit('failBasket', basket)
          }
        })
        this.baskets = this.baskets.filter(basket => basket.status !== 'fail')
        this.soket.emit('getBasket', this.get())
      }
    }, 3000)
  }

}

module.exports = {Basket}