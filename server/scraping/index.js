const cheerio = require('cheerio')
const needle = require('needle')

let getGame = async url => {
  const res = await needle('get', url)
  if (res.statusCode == 200) {
    const $ = cheerio.load(res.body)
    let games = $('table.dt.twp tbody:not(.props) tr.bk')
    let arr = []
    games.each(function () {
      let teams = $(this).find('a.om').length ? $(this).find('a.om').contents() : $(this).find('.l').contents()
      arr.push({
        teamOne: teams.eq(0).text(),
        teamTwo: teams.eq(2).text(),
        date: $(this).find('td').eq(1).html().match('[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9]\ [0-9][0-9]:[0-9][0-9]')[0],
      })
    })
    return arr
  } else {
    return []
  }
}
let getTournament = async url => {
  const res = await needle('get', url)
  if (res.statusCode == 200) {
    const $ = cheerio.load(res.body)
    const sports = $('ul#sports li')
    let SPORTS = []

    sports.each(function () {
      let f = $(this).find('a')
      let arr = f.text().split('. ')
      SPORTS.push({description: arr, url: f.attr('href')})
    })
    SPORTS = await Promise.all(SPORTS.map(async s => ({
      ...s,
      games: await getGame('https://www.parimatch.com/' + s.url)
    })))
    return SPORTS
  }
}
const search = async types => {
  games = await Promise.all(types.map(async type => ({
    label: type,
    tournament: await getTournament('https://www.parimatch.com/en/sport/' + type)
  })))
  return games
}

module.exports = {search}

