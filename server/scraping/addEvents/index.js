const {Users, Outcomes, Type, Tournament, Event} = require('../../db')
const moment = require('moment')

const addEvents = data => {
  data.forEach(async d => {
    let type = await Type.findOne({name: d.tournament[0].description[0]})

    if (!type) {
      type = await new Type({name: d.tournament[0].description[0]}).save()
    }

    if (d.tournament.length) {

      d.tournament.forEach(async tour => {
          let tournament = await Tournament.findOne({name: tour.description.join('. '), type: type._id})
          if (!tournament) {
            tournament = await Tournament({name: tour.description.join('. '), type: type._id})
            await  tournament.save()
          }
          if (tournament._id) {
            if (tour.games.length) {
              tour.games.forEach(async game => {
                let event = await Event.findOne({
                  tournament: tournament._id,
                  teamTwo: game.teamTwo,
                  teamOne: game.teamOne
                })
                if (!event) {
                  let date = moment(game.date, 'DD/MM/YY HH:mm')
                  event = await Event({
                    data: {
                      time: date.format('HH:mm'),
                      calendar: date.format('MM/DD/YY'),
                    },
                    tournament: tournament._id, teamTwo: game.teamTwo, teamOne: game.teamOne,
                  })
                  await event.save()
                }
              })
            }
          } else {
            console.log('Erorr', tournament)
          }
        }
      )
    }
  })
}

module.exports = {addEvents}
