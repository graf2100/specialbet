const Router = require('koa-router')
const serve = require('koa-static')
const send = require('koa-send')
const jwt = require('jsonwebtoken')
const sha256 = require('sha256')
const fs = require('fs')
const util = require('util')
const path = require('path')
const {Users, Outcomes, Type, Tournament, Event, Finance, Bets, Transaction, Images} = require('../db')
const {server} = require('../../config')
const Mail = require('../mail')

const router = new Router()

const readFile = util.promisify(fs.readFile)

router.post('/api/signup/', async ctx => {
  let {firstName, lastName, email, phone, password, birthday} = ctx.request.body
  if (firstName && lastName && email && phone && password && birthday) {
    let check = await Users.findOne({email})
    console.log(check)
    if (!check) {
      password = sha256(password)
      let user = new Users({firstName, lastName, email, phone, password, birthday})
      Mail.send(email, Mail.templateActive(server.LINK_TO_ACTIVE_USER + email))
      await user.save()
      const token = jwt.sign({user}, server.secret, {expiresIn: '7d'})
      ctx.body = {status: 'Done', token, user}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/upload/images', async ctx => {
  let {url, user} = ctx.request.body
  let img = await new Images({url, user}).save()
  ctx.body = {status: 'Done', img}
})
router.post('/api/find/images', async ctx => {
  let {user} = ctx.request.body
  let imgs = await Images.find({user})
  if (imgs) {
    ctx.body = {status: 'Done', imgs}
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.post('/api/auth/', async ctx => {
  let {email, phone, password} = ctx.request.body
  if ((email || phone) && password) {
    password = sha256(password)
    console.log(email, phone, password)
    let user = await Users.findOne({
      $or: [
        {email, password},
        {phone, password}
      ]
    })
    if (user) {
      const token = jwt.sign({user}, server.secret, {expiresIn: '7d'})
      ctx.body = {status: 'Done', token, user}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.post('/api/user/change/', async ctx => {
  let {token, firstName, lastName, phone, email, birthday} = ctx.request.body
  if (token) {
    await jwt.verify(token, server.secret, async (err, decoded) => {
      if (err) {
        ctx.body = {status: 'Fail', massage: 'Ошибка безопастности', err}
      } else {
        let {_id} = decoded.user
        let flag = await Users.updateOne({_id}, {firstName, lastName, phone, email, birthday})
        if (flag) {
          let user = await Users.findOne({_id})
          if (user) {
            const token = jwt.sign({user}, server.secret, {expiresIn: '7d'})
            ctx.body = {status: 'Done', token, user}
          } else {
            ctx.body = {status: 'Fail', massage: 'Ошибка базы данных'}
          }
        }
      }
    })

  } else {
    ctx.body = {status: 'Fail', massage: 'Ошибка безопастности'}
  }
})
router.post('/api/transfer', async ctx => {
  let {token, id, sum} = ctx.request.body
  sum = parseInt(sum)
  if (token) {
    await jwt.verify(token, server.secret, async (err, decoded) => {
      if (err) {
        ctx.body = {status: 'Fail', massage: 'Ошибка безопастности', err}
      } else {
        let {_id} = decoded.user
        await  Users.findOne({_id}, async (err, doc) => {
          if (doc) {
            if (doc.balance >= sum) {
              doc.balance -= sum
              doc.save()
              await  Users.findOne({_id: id}, (err, doc) => {
                if (doc) {
                  doc.balance += sum
                  doc.save()
                  ctx.body = {status: 'Done'}
                } else {
                  ctx.body = {status: 'Fail'}
                }
              })
            } else {
              ctx.body = {status: 'Fail', massage: 'Мало денег на счету'}
            }
          }
        })
      }
    })
  } else {
    ctx.body = {status: 'Fail', massage: 'Ошибка безопастности'}
  }
})

router.post('/api/get/finance/', async ctx => {
  let {key} = ctx.request.body
  if (key === 'myKeysTop2019') {
    let finance = await Finance.find({})
    if (finance) {
      ctx.body = {status: 'Done', finance}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.post('/api/add/finance/', async ctx => {
  let {key, type, id, sum, note, payment} = ctx.request.body
  if (key === 'myKeysTop2019' && type && id && sum && note && payment) {
    let user = await Users.findOne({_id: id})

    if (user) {
      if (type === 'Withdraw') {
        if (user.balance >= sum)
          user.balance -= sum
      } else if (type === 'Deposit') {
        user.balance += sum
      }
      user.save()
    }

    let finance = await new Finance({
      type,
      id,
      sum,
      note,
      payment
    }).save()
    finance = await Finance.find({})
    if (finance) {
      ctx.body = {status: 'Done', finance}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/pay/finance/', async ctx => {
  let {key} = ctx.request.body
  if (key === 'myKeysTop2019') {
    let finance = await Finance.find({type: 'Withdraw'})
    if (finance) {
      let sum = 0
      finance.forEach(a => {
        sum += a.sum
      })
      ctx.body = {status: 'Done', pay: sum}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.get('/api/finance/:id', async ctx => {
  let {id} = ctx.params
  if (id) {
    let finance = await Finance.find({id})
    if (finance) {
      ctx.body = {status: 'Done', finance}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.post('/api/replenishment/finance/', async ctx => {
  let {key} = ctx.request.body
  if (key === 'myKeysTop2019') {
    let finance = await Finance.find({type: 'Deposit'})
    if (finance) {
      let sum = 0
      finance.forEach(a => {
        sum += a.sum
      })
      ctx.body = {status: 'Done', replenishment: sum}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/remove/finance/', async ctx => {
  let {key, _id} = ctx.request.body
  if (key === 'myKeysTop2019' && _id) {
    let users = await Finance.deleteOne({_id})
    if (users) {
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/user/changePass/', async ctx => {
  let {pass, newPass, token} = ctx.request.body
  if (token) {
    await jwt.verify(token, server.secret, async (err, decoded) => {
      if (err) {
        ctx.body = {status: 'Fail', massage: 'Ошибка безопастности', err}
      } else {
        let {_id, password} = decoded.user
        if (password === sha256(pass)) {
          let flag = await Users.updateOne({_id}, {password: sha256(newPass)})
          if (flag) {
            let user = await Users.findOne({_id})
            if (user) {
              const token = jwt.sign({user}, server.secret, {expiresIn: '7d'})
              ctx.body = {status: 'Done', token, user}
            } else {
              ctx.body = {status: 'Fail', massage: 'Ошибка базы данных'}
            }
          }
        } else {
          ctx.body = {status: 'Fail', massage: 'Ошибка! Пароль неверный!'}
        }
      }
    })

  } else {
    ctx.body = {status: 'Fail', massage: 'Ошибка безопастности'}
  }
})
router.post('/api/cashout/', async ctx => {
  let {_id} = ctx.request.body
  if (_id) {
    let bets = await Bets.updateOne({_id}, {isСashOut: true})
    if (bets.ok) {
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail', massage: 'Ошибка базы данных'}
    }
  } else {
    ctx.body = {status: 'Fail', massage: 'Ошибка! Пароль неверный!'}
  }
})
router.post('/api/active/cashout/', async ctx => {
  let {id, sum, key} = ctx.request.body
  if (key === 'myKeysTop2019' && id && sum) {
    let bet = await  Bets.findOne({_id: id})
    if (bet) {
      bet.result = parseInt(sum)
      Users.findOne({_id: parseInt(bet.user)}, (err, user) => {
        user.balance += parseInt(sum)
        user.save()
      })
      bet.isDone = true
      bet.save()
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail', massage: 'Ошибка базы данных'}
    }
  } else {
    ctx.body = {status: 'Fail', massage: 'Ошибка! Пароль неверный!'}
  }
})

router.get('/api/cashout/:key', async ctx => {
  let {key} = ctx.params
  if (key === 'myKeysTop2019') {
    let bets = await Bets.find({isСashOut: true, isDone: false})
    if (bets) {
      ctx.body = {status: 'Done', bets}
    } else {
      ctx.body = {status: 'Fail', massage: 'Ошибка базы данных'}
    }
  } else {
    ctx.body = {status: 'Fail', massage: 'Ошибка! Пароль неверный!'}
  }
})

router.get('/api/active/:email', async ctx => {
  let {email} = ctx.params
  if (email) {
    console.log(email)
    let user = await Users.updateOne({email}, {isActive: true})
    if (user) {
      ctx.redirect('/')
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.get('/api/outcomes', async ctx => {
  let data = await Outcomes.find({})
  if (data) {
    ctx.body = {status: 'Done', data}
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/add/ItemOutcomes/', async ctx => {
  let {key, category, valItem} = ctx.request.body
  if (key === 'myKeysTop2019' && category && valItem) {
    let outcome = await Outcomes.findOne({_id: category})
    outcome.values.push(valItem)
    let obj = await outcome.save()
    if (obj) {
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/remove/ItemOutcomes/', async ctx => {
  let {key, _id, index} = ctx.request.body
  if (key === 'myKeysTop2019' && index && _id) {
    let outcome = await Outcomes.findOne({_id})
    outcome.values.splice(index, 1)
    let obj = await outcome.save()
    if (obj) {
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.post('/api/add/Outcomes/', async ctx => {
  let {key, typeId, valCategory} = ctx.request.body
  if (key === 'myKeysTop2019' && typeId && valCategory) {
    let outcome = await new Outcomes({label: valCategory, type: typeId, values: []}).save()
    if (outcome) {
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/remove/Outcomes/', async ctx => {
  let {key, _id} = ctx.request.body
  if (key === 'myKeysTop2019' && _id) {
    let outcome = await Outcomes.deleteOne({_id})
    if (outcome) {
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.get('/api/type', async ctx => {
  let data = await Type.find({})
  if (data) {
    ctx.body = {status: 'Done', data}
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.get('/api/tournament', async ctx => {
  let data = await Tournament.find({})
  if (data) {
    ctx.body = {status: 'Done', data}
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/remove/tournament/', async ctx => {
  let {key, _id} = ctx.request.body
  if (key === 'myKeysTop2019' && _id) {
    let users = await Tournament.deleteOne({_id})
    if (users) {
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/add/tournament/', async ctx => {
  let {key, name, country, type} = ctx.request.body
  if (key === 'myKeysTop2019' && name && country && type) {
    let tournament = await Tournament({name, сountry: country, type})
    let obj = await tournament.save()
    if (obj) {
      ctx.body = {status: 'Done', obj}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/update/tournament/', async ctx => {
  let {key, _id, name, country, type} = ctx.request.body
  if (key === 'myKeysTop2019' && _id && name && country && type) {
    let tournament = await Tournament.updateOne({_id}, {name, сountry: country, type})
    if (tournament) {
      ctx.body = {status: 'Done', obj: await Tournament.findOne({_id})}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.post('/api/update/events/', async ctx => {
  let {time, calendar, tour, teamTwo, teamOne, _id, key} = ctx.request.body
  if (key === 'myKeysTop2019' && time && calendar && tour && teamTwo && teamOne && _id) {
    let tournament = await Event.updateOne({_id}, {
      data: {
        time,
        calendar
      },
      tournament: tour, teamTwo, teamOne,
    })
    if (tournament) {
      ctx.body = {status: 'Done', obj: await Event.findOne({_id})}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.get('/api/events', async ctx => {
  let data = await Event.find({})
  if (data) {
    ctx.body = {status: 'Done', data}
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/add/events/', async ctx => {
  let {
    key, time,
    calendar,
    tour,
    teamTwo,
    teamOne
  } = ctx.request.body
  if (key === 'myKeysTop2019' && time && calendar && tour && teamTwo && teamOne) {
    let type = await Event({
      data: {
        time,
        calendar
      },
      tournament: tour,
      teamTwo,
      teamOne,
    })
    let obj = await type.save()
    if (obj) {
      ctx.body = {status: 'Done', obj}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/remove/events/', async ctx => {
  let {key, _id} = ctx.request.body
  if (key === 'myKeysTop2019' && _id) {
    let users = await Event.deleteOne({_id})
    if (users) {
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.post('/api/get/users', async ctx => {
  let {key} = ctx.request.body
  if (key === 'myKeysTop2019') {
    let users = await Users.find({})
    users = await Promise.all(users.map(async u => {
      let isImages = await Images.findOne({user: u._id.toString()})
      return ({...u._doc, isImages: !isImages})
    }))
    if (users) {
      ctx.body = {status: 'Done', users}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/remove/user/', async ctx => {
  let {key, _id} = ctx.request.body
  if (key === 'myKeysTop2019' && _id) {
    let users = await Users.deleteOne({_id})
    if (users) {
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.post('/api/remove/type/', async ctx => {
  let {key, _id} = ctx.request.body
  if (key === 'myKeysTop2019' && _id) {
    let users = await Type.deleteOne({_id})
    if (users) {
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/add/type/', async ctx => {
  let {key, name} = ctx.request.body
  if (key === 'myKeysTop2019' && name) {
    let type = await Type({name})
    let obj = await type.save()
    if (obj) {
      ctx.body = {status: 'Done', obj}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.post('/api/verification/user/', async ctx => {
  let {key, _id} = ctx.request.body
  if (key === 'myKeysTop2019' && _id) {
    let u = await Users.findOne({_id})
    if (u) {
      let user = await Users.updateOne({_id}, {isVerification: !u.isVerification})
      if (user) {
        ctx.body = {status: 'Done'}
      } else {
        ctx.body = {status: 'Fail'}
      }
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/add/transaction/', async ctx => {
  let {user, text} = ctx.request.body
  if (user && text) {
    let transaction = await Transaction({user, text}).save()
    if (transaction) {
      Mail.send('special1bet@gmail.com', Mail.templatePayOrWithdraw(), 'Запрос на ввод/вывод')
      ctx.body = {status: 'Done'}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.get('/api/transaction/', async ctx => {
  let transaction = await Transaction.find()
  if (transaction) {
    ctx.body = {status: 'Done', transaction}
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/remove/transaction/', async ctx => {
  let {_id} = ctx.request.body
  if (_id) {
    let transaction = await Transaction.deleteOne({_id})
    if (transaction) {
      ctx.body = {status: 'Done', transaction}
    } else {
      ctx.body = {status: 'Fail'}
    }

  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.post('/api/active/events/', async ctx => {
  let {key, _id} = ctx.request.body
  if (key === 'myKeysTop2019' && _id) {
    let e = await Event.findOne({_id})
    if (e) {
      let event = await Event.updateOne({_id}, {isActive: !e.isActive})
      if (event) {
        ctx.body = {status: 'Done'}
      } else {
        ctx.body = {status: 'Fail'}
      }
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.get('/api/bets/:key', async ctx => {
  let {key} = ctx.params
  if (key === 'myKeysTop2019') {
    let bets = await Bets.find({isDone: true})
    if (bets) {
      ctx.body = {status: 'Done', bets}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.get('/api/bets/:key', async ctx => {
  let {key} = ctx.params
  if (key === 'myKeysTop2019') {
    let bets = await Bets.find({isDone: true})
    if (bets) {
      ctx.body = {status: 'Done', bets}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})
router.post('/api/user/balance', async ctx => {
  let {_id} = ctx.request.body
  if (_id) {
    let fund = 0
    let balance = 0
    let user = await Users.findOne({_id: +_id})
    if (user) {
      let bets = await Bets.find({user: _id.toString(), isDone: false})
      bets.forEach(b => {fund += b.sum})
      balance = user.balance
      ctx.body = {status: 'Done', balance: {balance, fund}}
    } else {
      ctx.body = {status: 'Fail User'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.get('/api/getBets/:user', async ctx => {
  let {user} = ctx.params
  if (user) {
    let bets = await Bets.find({user})
    if (bets) {
      ctx.body = {status: 'Done', bets}
    } else {
      ctx.body = {status: 'Fail'}
    }
  } else {
    ctx.body = {status: 'Fail'}
  }
})

router.get('/google7ef03d5ba46fce2b.html', async ctx => {
  ctx.type = 'html'
  ctx.body = await readFile(path.join(__dirname, '..', 'static', 'google7ef03d5ba46fce2b.html'))
})

router.get('*', async ctx => {
  ctx.type = 'html'
  ctx.body = await readFile(path.join(__dirname, '..', '..', 'client', 'build', 'index.html'))
})
module.exports = router
