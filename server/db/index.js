const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')
const {server} = require('../../config')

mongoose.set('useCreateIndex', true)

const connection = mongoose.createConnection(server.DATABASE, {useNewUrlParser: true})
autoIncrement.initialize(connection)
const usersSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  phone: String,
  password: String,
  birthday: Date,
  dateCreation: {type: Date, default: Date.now},
  isActive: {type: Boolean, default: false},
  isVerification: {type: Boolean, default: false},
  balance: {type: Number, default: 0}
})

usersSchema.plugin(autoIncrement.plugin, {model: 'Users', startAt: 2100})

const transactionsSchema = new mongoose.Schema({
  user: mongoose.Schema.Types.ObjectId,
  date: {type: Date, default: Date.now},
  type: {type: String, default: ''},
  number: {type: String, default: ''},
  provider: {type: String, default: ''},
  sum: {type: Number, default: 0},
  isStatus: {type: String, default: ''},
  node: {type: String, default: ''},
})

const tournamentSchema = new mongoose.Schema({
  name: String,
  сountry: {type: String, default: ''},
  type: String
})
tournamentSchema.plugin(autoIncrement.plugin, {model: 'Tournament', startAt: 1})

const eventSchema = new mongoose.Schema({
  data: {time: String, calendar: String},
  tournament: String,
  status: String,
  teamOne: String,
  teamTwo: String,
  isActive: {type: Boolean, default: true},
  score: {type: String, default: ''},
})
tournamentSchema.plugin(autoIncrement.plugin, {model: 'Event', startAt: 1})

const betsSchema = new mongoose.Schema({
  user: {type: String},
  events: [],
  sum: Number,
  coefficient: Number,
  result: {type: Number, default: 0},
  date: {type: Date, default: Date.now},
  isDone: {type: Boolean, default: false},
  isСashOut: {type: Boolean, default: false},
})
betsSchema.plugin(autoIncrement.plugin, {model: 'Bets', startAt: 5201})

const outcomesSchema = new mongoose.Schema({
  label: String,
  type: String,
  values: [String]
})

const typeSchema = new mongoose.Schema({
  name: String,
})

const financeSchema = new mongoose.Schema({
  date: {type: Date, default: Date.now},
  type: String,
  id: String,
  sum: {type: Number, default: 0},
  note: {type: String, default: ''},
  payment: {type: String, default: ''},
})

const transactionSchema = new mongoose.Schema({
  date: {type: Date, default: Date.now},
  text: {type: String, default: ''},
  user: {type: Number},
})
const imagesSchema = new mongoose.Schema({
  user: {type: String, default: ''},
  url: {type: String, default: ''},
})

transactionSchema.plugin(autoIncrement.plugin, {model: 'Transaction', startAt: 1})

const Users = connection.model('Users', usersSchema)
const Tournament = connection.model('Tournament', tournamentSchema)
const Event = connection.model('Event', eventSchema)
const Transactions = connection.model('Transactions', transactionsSchema)
const Outcomes = connection.model('Outcomes', outcomesSchema)
const Type = connection.model('Type', typeSchema)
const Bets = connection.model('Bets', betsSchema)
const Finance = connection.model('Finance', financeSchema)
const Transaction = connection.model('Transaction', transactionSchema)
const Images = connection.model('Images', imagesSchema)

module.exports = {Users, Transactions, Outcomes, Type, Tournament, Event, Bets, Finance, Transaction, Images}
