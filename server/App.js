const Koa = require('koa')

const http = require('http')

const bodyParser = require('koa-bodyparser')
const serve = require('koa-static')
const logger = require('koa-logger')
const session = require('koa-session')
const cors = require('koa-cors')
const {server} = require('../config')
const router = require('./router')
const {search} = require('./scraping')
const {addEvents} = require('./scraping/addEvents')
const {Basket} = require('./basket')

const {Bets, Users} = require('./db')
const Mail = require('./mail')

const app = new Koa()

app.use(cors())

app.keys = ['some secret hurr']

app.use(session(app))
app.use(serve(server.html))
app.use(logger())
app.use(bodyParser())
app.use(router.routes()).use(router.allowedMethods())

const s = http.createServer(app.callback())
const io = require('socket.io')(s)
const baskets = new Basket(io)

io.on('connection', function (socket) {
  socket.on('scraping', async types => {
    if (types)
      io.emit('scraping', await search(types))
  })
  socket.on('addEvents', data => {
    addEvents(data)
    io.emit('addEvents', true)
  })
  socket.on('addBasket', data => {
    baskets.add(data)
    Mail.send('special1bet@gmail.com', Mail.templateRequest(), 'Запрос на коэффициент')
  })
  socket.on('getBasket', () => {
    socket.emit('getBasket', baskets.get())
  })
  socket.on('sendBasket', key => {
    baskets.done(key)
  })
  socket.on('setCoefficient', data => {
    baskets.set(data)
  })
  socket.on('sendBets', async data => {
    let {basket: {events, sum, coefficient}, user} = data
    let u = await Users.findOne({_id: parseInt(user)})

    if (u) {
      if (u.balance >= sum) {

        u.balance -= sum
        u.save()

        let bet = await new Bets({user, events, sum, coefficient}).save()

        if (bet) {
          let bets = await Bets.find({isDone: false})
          if (bets) {
            io.emit('getBets', bets)
          }
        }
      }
    }

  })
  socket.on('getBets', async () => {
    let bets = await Bets.find({isDone: false})
    if (bets) {
      io.emit('getBets', bets)
    }
  })
  socket.on('setStatusBet', async ({index, id, val}) => {
    let {events} = await  Bets.findOne({_id: id})
    if (events) {
      events[index].status = val
      let {ok} = await Bets.updateOne({_id: id}, {events: JSON.parse(JSON.stringify(events))})
      if (ok) {
        let bets = await Bets.find({isDone: false})
        if (bets) {
          io.emit('getBets', bets)
        }
      }
    }
  })
  socket.on('doneBet', async _id => {

    let bet = await  Bets.findOne({_id})
    if (bet) {
      bet.coefficient = 0

      bet.events.filter(event => event.status !== 'Return').forEach((event, index) => {
        if (index === 0) {
          bet.coefficient = event.coefficient
        } else {
          bet.coefficient *= event.coefficient
        }
        bet.coefficient = bet.coefficient.toFixed(2)
      })
      bet.events.forEach(event => {
        if (event.status === 'Lose') {
          bet.coefficient = 0
        }
      })
      bet.isDone = true

      bet.result = bet.coefficient * bet.sum

      if (bet.events.length === 1 && bet.events[0].status === 'Return') {
        bet.result = bet.sum
      }

      if (bet.user) {
        Users.findOne({_id: parseInt(bet.user)}, (err, user) => {
          if (err) {
            console.log(err)
          }
          if (user) {
            user.balance += bet.result
            user.save()
          }
        })
      }
      let {ok} = await Bets.updateOne({_id}, bet)
      if (ok) {
        let bets = await Bets.find({isDone: false})
        if (bets) {
          io.emit('getBets', bets)
        }
      }
    }
  })
})

s.listen(server.port, () => {
  console.log('Http running on %s', server.port)
})
