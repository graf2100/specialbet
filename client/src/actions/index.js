import { get, post } from 'axios'
import moment        from 'moment'

export const OPEN_MODAL = 'OPEN_MODAL'
export const REDIRECT_MODAL = 'REDIRECT_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'

export const openModal = modal => ({type: OPEN_MODAL, modal})
export const redirectModal = modal => ({type: REDIRECT_MODAL, modal})
export const closeModal = () => ({type: CLOSE_MODAL})

export const SIGN_IN_USER = 'SIGN_IN_USER'
export const SIGN_OUT_USER = 'SIGN_OUT_USER'
export const CHANGE_USER_FIELD = 'CHANGE_USER_FIELD'

export const signIn = user => ({type: SIGN_IN_USER, user})
export const signOut = () => ({type: SIGN_OUT_USER})
export const changeUserField = (val, field) => ({type: CHANGE_USER_FIELD, val, field})

export const PUSH_TYPES = 'PUSH_TYPES'
export const CHANGE_ACTIVE_TYPE = 'CHANGE_ACTIVE_TYPE'
export const changeType = key => ({type: CHANGE_ACTIVE_TYPE, key})
export const pushTypes = data => ({type: CHANGE_ACTIVE_TYPE, data})

export const GET_OUTCOMES_REQUEST = 'GET_OUTCOMES_REQUEST'
export const GET_OUTCOMES_SUCCESS = 'GET_OUTCOMES_SUCCESS'
export const GET_OUTCOMES_FAILURE = 'GET_OUTCOMES_FAILURE'

export const requestOutComes = () => async dispatch => {
  dispatch({type: GET_OUTCOMES_REQUEST})
  let {data: {data, status}} = await get('/api/outcomes')
  if (status === 'Done') {
    dispatch({type: GET_OUTCOMES_SUCCESS, data: data})
  } else {
    dispatch({type: GET_OUTCOMES_FAILURE})
  }
}
export const GET_TYPE_REQUEST = 'GET_TYPE_REQUEST'
export const GET_TYPE_SUCCESS = 'GET_TYPE_SUCCESS'
export const GET_TYPE_FAILURE = 'GET_TYPE_FAILURE'

export const requestType = () => async dispatch => {
  dispatch({type: GET_TYPE_REQUEST})
  let {data: {data, status}} = await get('/api/type')
  if (status === 'Done') {
    dispatch({type: GET_TYPE_SUCCESS, data: data})
  } else {
    dispatch({type: GET_TYPE_FAILURE})
  }
}

export const ADD_EVENT_BASKET = 'ADD_EVENT_BASKET'
export const SET_SELECT_BASKET = 'SET_SELECT_BASKET'
export const REMOVE_EVENT_BASKET = 'REMOVE_EVENT_BASKET'
export const CHANGE_SUM_BASKET = 'CHANGE_SUM_BASKET'
export const CLEAR_BASKET = 'CLEAR_BASKET'
export const DONE_BASKET = 'DONE_BASKET'

export const addEventBasket = event => ({type: ADD_EVENT_BASKET, event})
export const setSelectBasket = select => ({type: SET_SELECT_BASKET, select})
export const clearBasket = () => ({type: CLEAR_BASKET})
export const doneBasket = basket => ({type: DONE_BASKET, basket})
export const changeSum = sum => ({type: CHANGE_SUM_BASKET, sum: parseInt(sum)})
export const removeEvent = index => ({type: REMOVE_EVENT_BASKET, index})

export const GET_TOURNAMENT_SUCCESS = 'GET_TOURNAMENT_SUCCESS'
export const GET_TOURNAMENT_FAILURE = 'GET_TOURNAMENT_FAILURE'

export const requestTournament = () => async dispatch => {
  let {data: {data, status}} = await get('/api/tournament')
  if (status === 'Done') {
    dispatch({type: GET_TOURNAMENT_SUCCESS, data: data})
  } else {
    dispatch({type: GET_TOURNAMENT_FAILURE})
  }
}

export const GET_EVENTS_SUCCESS = 'GET_EVENTS_SUCCESS'
export const GET_EVENTS_FAILURE = 'GET_EVENTS_FAILURE'

export const requestEvents = () => async dispatch => {
  let {data: {data, status}} = await get('/api/events')
  if (status === 'Done') {
    data = data.filter(d => {
      return !moment(d.data.calendar + ' ' + d.data.time, 'MM/DD/YY h:mm').subtract(1, 'h').isSameOrBefore(moment())
    })
    dispatch({type: GET_EVENTS_SUCCESS, data: data})
  } else {
    dispatch({type: GET_EVENTS_FAILURE})
  }
}

export const GET_BALANCE = 'GET_BALANCE'
export const requestBalance = _id => dispatch => {
  setInterval(async () => {
    let {data: {balance, status}} = await post('/api/user/balance', {_id})
    console.log('status', status)
    if (status === 'Done') {
      dispatch({type: GET_BALANCE, obj: balance})
    } else if (status === 'Fail User') {
      console.log('1')
      localStorage.clear()
      dispatch({type: SIGN_OUT_USER})
    }
  }, 3000)
}
