import { combineReducers } from 'redux'
import {
  ADD_EVENT_BASKET, CHANGE_ACTIVE_TYPE, CHANGE_SUM_BASKET, CHANGE_USER_FIELD, CLEAR_BASKET, CLOSE_MODAL,
  DONE_BASKET, GET_BALANCE, GET_EVENTS_FAILURE, GET_EVENTS_SUCCESS, GET_OUTCOMES_FAILURE, GET_OUTCOMES_REQUEST,
  GET_OUTCOMES_SUCCESS, GET_TOURNAMENT_FAILURE, GET_TOURNAMENT_SUCCESS, GET_TYPE_FAILURE, GET_TYPE_REQUEST,
  GET_TYPE_SUCCESS, OPEN_MODAL, REDIRECT_MODAL, REMOVE_EVENT_BASKET, SET_SELECT_BASKET, SIGN_IN_USER, SIGN_OUT_USER
}                          from '../actions'

function user (state = JSON.parse(localStorage.getItem('user')), action) {
  switch (action.type) {
    case SIGN_IN_USER:
      return {...action.user}
    case CHANGE_USER_FIELD:
      state[action.field] = action.val
      return {...state}
    case SIGN_OUT_USER:
      return false
    default:
      return state
  }
}

function modals (state = {
  isLogin: false,
  isRegistration: false,
  isRecovery: false,
  isRegistrationEnd: false,
  isTransfer: false,
  isVerification: false,
  isSuccess: false,
  isOutComes: false,
  isBasket: false,
  isAI: false,
  isFail: false,
  isDone: false,
  isDoneBasket: false,
}, action) {
  switch (action.type) {
    case CLOSE_MODAL:
      return {
        isLogin: false,
        isRegistration: false,
        isRecovery: false,
        isRegistrationEnd: false,
        isTransfer: false,
        isVerification: false,
        isSuccess: false,
        isOutComes: false,
        isBasket: false,
        isAI: false,
        isFail: false, isDone: false,
        isDoneBasket: false,
      }
    case OPEN_MODAL:
      state[action.modal] = true
      return {...state}
    case REDIRECT_MODAL:
      let newState = {
        isLogin: false,
        isRegistration: false,
        isRecovery: false,
        isRegistrationEnd: false,
        isTransfer: false,
        isVerification: false,
        isSuccess: false,
        isOutComes: false,
        isBasket: false,
        isAI: false,
        isFail: false,
        isDone: false,
        isDoneBasket: false,
      }
      newState[action.modal] = true
      return {...newState}
    default:
      return state
  }
}

function type (state = [{id: 1, name: 'Футбол', isActive: true},
  {id: 2, name: 'Теннис', isActive: false},
  {id: 3, name: 'Хоккей', isActive: false},
  {id: 4, name: 'Баскетбол', isActive: false}], action) {
  switch (action.type) {
    case GET_TYPE_SUCCESS:
      let data = action.data.map(d => ({...d, isActive: false}))
      if (data.length) {
        data[0].isActive = true
      }
      return [...data]
    case GET_TYPE_REQUEST:
      return []
    case GET_TYPE_FAILURE:
      return []
    case CHANGE_ACTIVE_TYPE:
      state = state.map(g => ({...g, isActive: false}))
      state[action.key].isActive = true
      return [...state]
    default:
      return state
  }
}

function tournament (state = [{
  id: '123',
  name: 'Первый турнир',
  сountry: 'Украина',
  type: '5c138272fb928a0513d38529'
}], action) {
  switch (action.type) {
    case GET_TOURNAMENT_SUCCESS:
      return [...action.data]
    case GET_TOURNAMENT_FAILURE:
      return []
    default:
      return state
  }
}

function events (state = [{
  _id: '123',
  data: {time: '13:00', calendar: '24.04.2018'},
  tournament: '123',
  status: 'Идет игра',
  teamOne: 'Команда А',
  teamTwo: 'Команда Б',
  score: '-',
}], action) {
  switch (action.type) {
    case GET_EVENTS_SUCCESS:
      return [...action.data]
    case GET_EVENTS_FAILURE:
      return []
    default:
      return state
  }
}

function outcomes (state = [], action) {
  switch (action.type) {
    case GET_OUTCOMES_FAILURE:
      return []
    case GET_OUTCOMES_REQUEST:
      return []
    case GET_OUTCOMES_SUCCESS:
      return [...action.data]
    default:
      return state
  }
}

function basket (state = {sum: 0, events: [], id: 1, select: {}, coefficient: 0}, action) {
  switch (action.type) {
    case ADD_EVENT_BASKET:
      let {event: {outcomes}} = action
      if (!state.events.find(e => e.text === state.select.text)) {
        state.events.push({coefficient: 0, ...state.select, outcomes, status: ''})
      }
      state.select = {}
      return {...state}
    case SET_SELECT_BASKET:
      state.select = action.select
      return {...state}
    case CHANGE_SUM_BASKET:
      state.sum = action.sum
      return {...state}
    case REMOVE_EVENT_BASKET:
      state.events.splice(action.index, 1)
      return {...state}
    case CLEAR_BASKET:
      return {sum: 0, events: [], id: 1, select: {}}
    case  DONE_BASKET:
      return {...action.basket}
    default:
      return state
  }
}

function balance (state = {fund: 0, balance: 0}, action) {
  switch (action.type) {
    case GET_BALANCE:
      return {...action.obj}
    default:
      return state
  }

}

const reducer = combineReducers({user, modals, events, type, tournament, outcomes, basket, balance})

export default reducer