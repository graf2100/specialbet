import React, { Component, Fragment }                                                                 from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch
}                                                                                                     from 'react-router-dom'
import { connect }                                                                                    from 'react-redux'
import { AboutUs, Finance, Helps, History, Home, notFound, Profile, Replenishment, Rule, Withdrawal } from './pages'
import { Models }                                                                                     from '../components'
import { requestBalance, requestEvents, requestTournament, requestType }                              from '../actions'

class App extends Component {

  componentDidMount () {
    let {requestType, requestEvents, requestTournament, requestBalance, isAuth, user} = this.props
    requestEvents()
    requestTournament()
    requestType()
    if (isAuth)
      requestBalance(user._id)
  }

  render () {
    let {isAuth} = this.props
    return (
      <Fragment>
        <Router>
          <Fragment>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/about-us/" component={AboutUs}/>
              <Route exact path="/helps/" component={Helps}/>
              <Route exact path="/rule/" component={Rule}/>
              {isAuth && <Route exact path="/profile/" component={Profile}/>}
              {isAuth && <Route exact path="/history/" component={History}/>}
              {isAuth && <Route exact path="/finance/" component={Finance}/>}
              {isAuth && <Route exact path="/replenishment/" component={Replenishment}/>}
              {isAuth && <Route exact path="/withdrawal/" component={Withdrawal}/>}
              <Route component={notFound}/>
            </Switch>
          </Fragment>
        </Router>
        <Models/>
      </Fragment>
    )
  }
}

export default connect(state => {
    let isAuth = !!state.user
    return {isAuth, user: state.user}
  }, dispatch => ({
    requestType: () => dispatch(requestType()),
    requestEvents: () => dispatch(requestEvents()),
    requestTournament: () => dispatch(requestTournament()),
    requestBalance: _id => dispatch(requestBalance(_id)),
  })
)
(App)
