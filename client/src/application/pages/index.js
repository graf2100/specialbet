import Home          from './Home'
import AboutUs       from './AboutUs'
import Profile       from './Profile'
import History       from './History'
import Finance       from './Finance'
import notFound      from './404'
import Helps         from './Helps'
import Replenishment from './Replenishment'
import Rule          from './Rule'
import Withdrawal    from './Withdrawal'

export { Home, AboutUs, Profile, History, Finance, notFound, Helps, Replenishment, Rule, Withdrawal }