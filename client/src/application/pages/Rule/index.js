import React, { Fragment } from 'react'
import { Footer, Header }  from '../../../components'

export default function Rule () {
  return (
    <Fragment>
      <Header/>
      <main className="main">
        <article>
          <div className="container container-xl">
            <div className="about-us">
              <div className="heading">
                <h1>Terms and Conditions</h1>
              </div>
              <div className="main-content">
                <h1>Types of bets:</h1>
                <p>"Single bet" - a bet on the forecast of outcome for a single event.
                </p>
                <p>The "single bet" win is equal to the product of the bet amount by the coefficient established for the
                  outcome of the event.</p>
                <p>"Express" - a bet on the prediction of the outcome of several independent events.</p>
                <p>The “express” win is equal to the product of the bet amount by the “express” coefficient, obtained by
                  multiplying the outcome coefficients of all the events included in the “express”.
                </p>
                <p>
                  In the "express" you can include any combination of outcomes of unrelated events in any sports.
                  “Express” is considered to be won if all the events included in it are correctly predicted. Losing one
                  of the outcomes means losing the entire bet of this type. The maximum rate at the rate of type
                  "Express" - 150.
                </p>
                <h1>THE MAIN TYPES OF OUTCOMES PROPOSED FOR THE CONCLUSION OF BETTING:</h1>
                <p>Bets are accepted on a clear victory of the first team (in the line denoted by the symbol "1"), a
                  draw (denoted by "X") or a victory of the second team (denoted by "2"). A bet is considered won if you
                  correctly predicted the outcome of the match.
                </p>
                <p>The victory of the first team or a draw - denoted by "1X". To win a bet on such an outcome, it is
                  necessary that the first team does not lose, that is, win or draw. There will be no draw (Victory of
                  the first or second team) - denoted by "12". To win a bet on such an outcome, it is necessary that one
                  of the contenders win, that is, that there is no draw. The victory of the second team or a draw -
                  denoted by "X2". To win a bet on such an outcome, it is necessary that the second team does not lose.
                </p>
                <p>You can bet on the event, taking into account the odds (denoted by "F"). After the result of the
                  match, the player adds the established odds to the goals of his chosen team. If after adding the
                  result of the match in favor of the team chosen by the player, the bet is considered to be won; in the
                  case of a draw (taking into account the odds) - the bet is returned, and in the "express" is
                  calculated with a coefficient of win "1". If the result of the match is in favor of the opposite team
                  - the bet is lost.</p>
                <p>Total (denoted "Total" or "T") - the number of goals scored (abandoned pucks, etc.) in a match. To
                  win, you need the correct prediction: more "B" or less "M" will be scored goals in the match.
                  Individual total teams (member) (denoted by "iTotal" or "iT") - the number of goals scored (abandoned
                  pucks, etc.) in a match by one of the competitors. To win, it is necessary to correctly indicate that
                  more than “B” or less than “M” will be scored goals in the match by the specified participant (when
                  hit in the Total, the bet is returned, and in the “express” it is calculated with a gain of “1”).
                </p>
                <p>The betting network reserves the right to offer other betting options.</p>
                <h1>FINANCIAL RESTRICTIONS:</h1>
                <p>The minimum bet on any event is: 5 USD, the maximum gain factor for the “express” bet is 150.
                  “Express”, the winning coefficient for which exceeds the maximum, is calculated with the coefficient
                  "150". At the same time, the winning amount must not exceed the maximum payout by one bet. The maximum
                  amount of the bet per event depends on the sport and event, is determined by the betting network
                  specifically for each event and for each type of bet and is subject to change without prior written
                  notice.</p>
                <p>The betting network reserves the right to limit the maximum rate for individual events, as well as
                  the introduction and removal of special restrictions on the accounts of individual players without
                  notifying and explaining the reasons.</p>
                <p>The maximum payout per bet is equal to the equivalent of 500,000 USD in the game's currency.</p>
                <h1>VIP RATE:</h1>
                <p>If you want to make a bet in the amount of 10,000 USD or more, then you can place your bet as "VIP"
                  for this you need to write a letter to our mail support@gmail.com . In order to do this, the following
                  conditions must be met:
                </p>
                <p>On your game account should be the amount of 10,000 USD</p>
                <p>The sum of the minimum "VIP rate" is eq. 10,000 USD in the game account currency.</p>
                <h1>Cash Out:</h1>
                <p>The "CashOut" option gives you the opportunity to calculate your bet.</p>
                <p>The "CashOut" option is available only for your non-calculated bets.
                </p>
                <p>The option "CashOut" will be active for bets before the event and Live.
                </p>
                <p>When betting "express bet" option "CashOut" will be available only for whole express and not for
                  individual events in it. If some event in your express bet has ended in accordance with your rate then
                  the option "CashOut" will be available to you for fixing the already won profit.</p>
                <p>The "CashOut" option is available for bets wich not closed.
                </p>
                <p>The bookmaker does not guarantee the availability of the "CashOut" option at any time.
                </p>
                <p>If your request for "CashOut" at one of the bet is successful, then your bet will be calculated
                  immediately and credited to your gaming account. The final result of the event will have no sense for
                  the calculation of this bet.</p>
                <p>The bookmaker reserves the rights to accept or reject request for "CashOut" for any sports,
                  competitions or bets.</p>

                <h2>Deposit / Withdraw:</h2>
                <p>1. The Specialone Security Service has the rights to:
                  * Refuse withdraw of funds if the amounts of deposit and withdraw of funds do not matched to the
                  amounts of placed bets (should bet with coefficient of at least 1.1 for the amount of funds deposited)
                  * Refuse to withdraw funds if the gaming account is not used for gaming purposes, and before
                  withdrawing funds you will need to verify your account.
                </p>
                <p>2. The Specialone Security Service does not recommend:
                  * transfer money between payment systems
                  * deposit to your account and withdraw funds without placing bets
                  * use the option “transfer to friend” to withdraw funds.
                </p>
                <p>In these situations the money will be returned to your account.</p>
              </div>
            </div>
          </div>
        </article>
      </main>
      <Footer/>
    </Fragment>)
};