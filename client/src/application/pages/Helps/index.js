import React, { Fragment } from 'react'
import { Footer, Header }  from '../../../components'

export default function Helps () {
  return (
    <Fragment>
      <Header/>
      <main className="main">
        <article>
          <div className="container container-xl">
            <div className="about-us">
              <div className="heading">
                <h1>Help</h1>
              </div>
              <div className="main-content">
                <p>To register for Specialone, you need to complete just a few steps: Follow the “Register” link.</p>
                <p>Fill the registration form. It is very important that you correctly and carefully fill in all the
                  required fields of the registration form. Set a “tick” confirming your coming of age and acceptance of
                  our terms and conditions.</p>
                <p>Click on the "Register" button.</p>
                <p>4. Ready!</p>
                <p>To place a bet:</p>
                <p> 1. select sport's event</p>
                <p> 2. select the outcome of the event or events</p>
                <p> 3. go to coupon</p>
                <p> 3. in the coupon, enter the amount of the bet and click on get odds.</p>
                <p> 4. In 1-5 minutes AI will select for you the best odds for your's chosen result of a sport's event
                  and press ok.</p>
                <p> 5. Press the button to bet or cancel</p>
                <p> 6. Your bet is made You can look at the bet in your account - “History of bets”</p>
                <p>P.S. If AI does not select odds then in the coupon you will see odds equal to 1.001</p>
                <p>Successful game with us Specialbet</p>
              </div>
            </div>
          </div>
        </article>
      </main>
      <Footer/>
    </Fragment>
  )
};