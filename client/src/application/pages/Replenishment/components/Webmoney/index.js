import React, { Component, Fragment } from 'react'
import { connect }                    from 'react-redux'

class Webmoney extends Component {
  constructor (props) {
    super(props)

    this.state = {
      number: '',
    }
  }

  change = (val, key) => (this.setState({[key]: val}))

  render () {
    let {onBack, submit} = this.props
    let {number} = this.state
    return (<Fragment>
      <div className={'test'}>GAMING ACCOUNT UPDATE WITH WEBMONEY
      </div>
      <div style={{width: '80%', margin: '0 10%'}}>
        <div className={'row'}>
          <div className={'col-4 mt-4'} style={{color: '#fff'}}>
            <div className="form-group">
              <label style={{fontSize: '0.9rem'}}>Amount</label>
              <input className="form-control" placeholder="" value={number} type="number" required=""
                     onChange={e => this.change(e.target.value, 'number')}/>
            </div>
            <button className="btn button button__red button__md btn-block" onClick={() => {
              submit('Ввод средств WebMoney, сумма: ' + number)
              onBack()
            }}>
              Submit
            </button>
            <p className={'d-block ml-5 mt-2'} onClick={onBack}>return to deposit</p>
          </div>
        </div>
      </div>
      <hr style={{background: '#eeee'}}/>
      <div style={{width: '80%', margin: '0 10%'}}>
        <p className={'font-weight-bold d-block mt-4'}>Attention!</p>
        <p>An email has been sent to your email with payment details for Webmoney
          After payment is made according to the requisites, send an email to support@specialone.bet mail with a screenshot of the payment or payment details. .</p>
      </div>
    </Fragment>)
  }
}

const mapStateToProps = state => {
  return {}
}
const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Webmoney)