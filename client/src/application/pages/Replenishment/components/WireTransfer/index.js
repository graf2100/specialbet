import React, { Component, Fragment } from 'react'

class WireTransfer extends Component {

  render () {
    let {onBack} = this.props
    return (<Fragment>
      <div className={'test'}>GAMING ACCOUNT UPDATE WITH WIRE TRANSFER
      </div>
      <div style={{width: '80%', margin: '0 10%'}}>
        <div className={'row'}>
          <div className={'col-8 mt-4'} style={{color: '#fff'}}>
            Bank - <strong>ING Bank N.V.</strong><br/>
            Adress of the bank - <strong>Pribinova 10, 81109, Bratislava, Slovakia</strong><br/>
            Account Holder Name - <strong>Majestic Financial UAB</strong><br/>
            Account Holder Address - <strong>Mesiniu g.5, 01133, Vilnius, Lithuania</strong><br/>
            IBAN - <strong>SK42 7300 0000 0090 0004 3394</strong><br/>
            SWIFT BIC - <strong>INGBSKBX</strong>
            <p className={'mt-2 font-weight-bold'} style={{color: '#dc3545'}}>Payment purpose - Please contact
              support@specialone.bet
            </p>
            <p className={'d-block ml-5 mt-2 '} onClick={onBack}>return to deposit</p>
          </div>
        </div>
      </div>
      <hr style={{background: '#eeee'}}/>
      <div style={{width: '80%', margin: '0 10%'}}>
        <p className={'font-weight-bold d-block mt-4'}>Attention!</p>
        <p>To successfully replenish your account, copy our WIRE TRANSFER wallet number, enter the estimated amount in the
          column, then click “send request” button. After payment, send us an email (the wallet number from which the
          transfer was made and the date of transfer) Commission - 0%</p>
      </div>
    </Fragment>)
  }
}

export default WireTransfer