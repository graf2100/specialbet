import React, { Component, Fragment } from 'react'
import { connect }                    from 'react-redux'
import Copy                           from '../../../../../images/copy.svg'

class Bitcoin extends Component {
  constructor (props) {
    super(props)
    this.state = {
      number: '1234123412341254',
      sum: 0,
    }
  }

  copy = () => {
    document.execCommand(this.state.number)
  }

  change = (val, key) => (this.setState({[key]: val}))

  render () {
    let {onBack, submit} = this.props
    let {sum} = this.state
    return (<Fragment>
      <div className={'test'}>GAMING ACCOUNT UPDATE WITH BITCOIN
      </div>
      <div style={{width: '80%', margin: '0 10%'}}>
        <div className={'row'}>
          <div className={'col-4 mt-4'} style={{color: '#fff'}}>
            <label style={{fontSize: '0.9rem'}}>Bitcoin</label>
            <div className="input-group mb-3">
              <input type="text" className="form-control" placeholder="1PjfzHuqNEWsZehUHvfraL9QniXwnEtUSJ" required=""
                     value={'1PjfzHuqNEWsZehUHvfraL9QniXwnEtUSJ'} disabled={true}/>
              <div className="input-group-append">
                <button className="btn button__red text-white" type="button"><img src={Copy} alt={'Copy'} style={{
                  position: 'relative',
                  width: '1rem',
                  top: '-3px'
                }} onClick={this.copy}/>
                </button>
              </div>
            </div>
            <div className="form-group">
              <label style={{fontSize: '0.9rem'}}>Estimated Amount</label>
              <input className="form-control" placeholder="Amount" type="number" required=""
                     onChange={e => this.change(e.target.value, 'sum')} value={sum}/>
            </div>
            <button className="btn button button__red button__md btn-block" onClick={() => {
              submit('Ввод средств Bitcoin, сумма: ' + sum)
              onBack()
            }}>
              Send request
            </button>
            <p className={'d-block ml-5 mt-2'} onClick={onBack}>return to deposit</p>
          </div>
        </div>
      </div>
      <hr style={{background: '#eeee'}}/>
      <div style={{width: '80%', margin: '0 10%'}}>
        <p className={'font-weight-bold d-block mt-4'}>Attention!</p>
        <p>To successfully replenish your account, copy our Bitcoin wallet number, enter the estimated amount in the
          column, then click “send request” button. After payment, send us an email (the wallet number from which the
          transfer was made and the date of transfer) Commission - 0%
        </p>
      </div>
    </Fragment>)
  }
}

const mapStateToProps = state => {
  let {user} = state
  return {user}
}
const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Bitcoin)