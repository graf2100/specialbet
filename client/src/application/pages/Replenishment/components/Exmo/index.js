import React, { Component, Fragment } from 'react'
import { connect }                    from 'react-redux'

class Exmo extends Component {
  constructor (props) {
    super(props)

    this.state = {
      exmo: '',
    }
  }

  change = (val, key) => (this.setState({[key]: val}))

  render () {
    let {onBack, submit} = this.props
    let {exmo} = this.state
    return (<Fragment>
      <div className={'test'}>GAMING ACCOUNT UPDATE WITH EXMO
      </div>
      <div style={{width: '80%', margin: '0 10%'}}>
        <div className={'row'}>
          <div className={'col-4 mt-4'} style={{color: '#fff'}}>
            <div className="form-group">
              <label style={{fontSize: '0.9rem'}}>Enter EXMO - code</label>
              <input className="form-control" placeholder="" value={exmo} type="text" required=""
                     onChange={e => this.change(e.target.value, 'exmo')}/>
            </div>
            <button className="btn button button__red button__md btn-block" onClick={() => {
              submit('Ввод средств Exmo: ' + exmo)
              onBack()
            }}>
              Submit
            </button>
            <p className={'d-block ml-5 mt-2'} onClick={onBack}>return to deposit</p>
          </div>
        </div>
      </div>
      <hr style={{background: '#eeee'}}/>
      <div style={{width: '80%', margin: '0 10%'}}>
        <p className={'font-weight-bold d-block mt-4'}>Attention!</p>
        <p>Enter EX-CODE and click send<br/>
          Funds saved in EX-CODE are entered into the system instantly and 0% commission
        </p>
      </div>
    </Fragment>)
  }
}

const mapStateToProps = state => {
  return {}
}
const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Exmo)