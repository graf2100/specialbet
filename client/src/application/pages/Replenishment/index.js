import React, { Component, Fragment }                  from 'react'
import axios                                           from 'axios'
import { Footer, Header }                              from '../../../components'
import { connect }                                     from 'react-redux'
import webmoney                                        from '../../../images/webmoney.png'
import qiwi                                            from '../../../images/qiwi.png'
import exmo                                            from '../../../images/exmo.png'
import mastercard                                      from '../../../images/mastercard.png'
import visa                                            from '../../../images/visa.png'
import wire                                            from '../../../images/wire transfer.png'
import bitcoin                                         from '../../../images/800px-Bitcoin_logo.svg.png'
import { Bitcoin, Exmo, Qiwi, Webmoney, WireTransfer } from './components'
import { openModal }                                   from '../../../actions'

class Replenishment extends Component {

  state = {
    type: '',
    stage: 0,
  }

  handelChangeStage = type => {
    this.setState({type, stage: 1})
  }
  submit = async text => {
    let {user} = this.props
    let {data: status} = await axios.post('/api/add/transaction/', {user: user._id, text})
    if (status === 'Done') {
      console.log(text)
    }
  }
  StageZero = () => {
    let {openModal} = this.props
    return (<Fragment>
      <div className={'test'}>
        OPERATIONS WITH FACILITIES
      </div>

      <ul className="nav nav-tabs tabs justify-content-center b-main">
        <li className="nav-item"><a className="nav-link active show b-main" href={'/replenishment'}>Deposit</a>
        </li>
        <li className="nav-item"><a className="nav-link  b-main" href={'/withdrawal'}>Withdrawal</a></li>
      </ul>
      <div style={{width: '80%', margin: '0 10%'}}>
        <div className={'row'}>
          <div className={'col-4'}>
            <div className={'card b-two'}>
              <img src={exmo} alt={'exmo'}/>
              <div className={'row mx-auto'} style={{width: '90%'}}>
                <div className={'col-6 m5'}>Deposit</div>
                <div className={'col-6 text-right font-weight-bold m5'}>USD 1-100000</div>
                <div className={'col-6 m5'}>Comission</div>
                <div className={'col-6 text-right font-weight-bold m5'}>0%</div>
                <div className={'col-6 m5'}>In processing</div>
                <div className={'col-6 text-right font-weight-bold m5'}>Instantly</div>
              </div>
              <button className="btn button button__red button__md mx-auto"
                      onClick={() => this.handelChangeStage('exmo')}>Deposit
              </button>
            </div>
          </div>
          <div className={'col-4'}>
            <div className={'card b-two'}>
              <img src={qiwi} alt={'qiwi'}/>
              <div className={'row mx-auto'} style={{width: '90%'}}>
                <div className={'col-6 m5'}>Deposit</div>
                <div className={'col-6 text-right font-weight-bold m5'}>USD 1-100000</div>
                <div className={'col-6 m5'}>Comission</div>
                <div className={'col-6 text-right font-weight-bold m5'}>0%</div>
                <div className={'col-6 m5'}>In processing</div>
                <div className={'col-6 text-right font-weight-bold m5'}>Instantly</div>
              </div>
              <button className="btn button button__red button__md mx-auto"
                      onClick={() => this.handelChangeStage('qiwi')}>Deposit
              </button>
            </div>
          </div>
          <div className={'col-4'}>
            <div className={'card b-two'}>
              <img src={webmoney} style={{width: '6rem'}} alt={'webmoney'}/>
              <div className={'row mx-auto'} style={{width: '90%'}}>
                <div className={'col-6 m5'}>Deposit</div>
                <div className={'col-6 text-right font-weight-bold m5'}>USD 1-100000</div>
                <div className={'col-6 m5'}>Comission</div>
                <div className={'col-6 text-right font-weight-bold m5'}>0%</div>
                <div className={'col-6 m5'}>In processing</div>
                <div className={'col-6 text-right font-weight-bold m5'}>Instantly</div>
              </div>
              <button className="btn button button__red button__md mx-auto"
                      onClick={() => this.handelChangeStage('webmoney')}>Deposit
              </button>
            </div>
          </div>
          <div className={'col-4'}>
            <div className={'card b-two mt-0'}>
              <img src={wire} style={{width: '3rem'}} alt={'wire'}/>
              <div className={'row mx-auto'} style={{width: '90%'}}>
                <div className={'col-6 m5'}>Deposit</div>
                <div className={'col-6 text-right font-weight-bold m5'}>USD 1-100000</div>
                <div className={'col-6 m5'}>Comission</div>
                <div className={'col-6 text-right font-weight-bold m5'}>0%</div>
                <div className={'col-6 m5'}>In processing</div>
                <div className={'col-6 text-right font-weight-bold m5'}>Instantly</div>
              </div>
              <button className="btn button button__red button__md mx-auto"
                      onClick={() => this.handelChangeStage('wiretransfer')}>Deposit
              </button>
            </div>
          </div>
          <div className={'col-4'}>
            <div className={'card b-two mt-0'}>
              <img src={bitcoin} style={{width: '5rem'}} alt={'bitcoin'}/>
              <div className={'row mx-auto'} style={{width: '90%'}}>
                <div className={'col-6 m5'}>Deposit</div>
                <div className={'col-6 text-right font-weight-bold m5'}>USD 1-100000</div>
                <div className={'col-6 m5'}>Comission</div>
                <div className={'col-6 text-right font-weight-bold m5'}>0%</div>
                <div className={'col-6 m5'}>In processing</div>
                <div className={'col-6 text-right font-weight-bold m5'}>Instantly</div>
              </div>
              <button className="btn button button__red button__md mx-auto"
                      onClick={() => this.handelChangeStage('bitcoin')}>Deposit
              </button>
            </div>
          </div>
          <div className={'col-4'}>
            <div className={'card b-two mt-0'}>
              <div className={'d-flex justify-content-center align-self-center'}>
                <img className={'d-block'} src={visa} style={{width: '2.5rem', height: '1.2rem'}} alt={'visa'}/>
                <img className={'d-block ml-4'} src={mastercard} style={{width: '2rem', height: '1.2rem'}}
                     alt={'mastercard'}/>
              </div>
              <div className={'row mx-auto'} style={{width: '90%'}}>
                <div className={'col-6 m5'}>Deposit</div>
                <div className={'col-6 text-right font-weight-bold m5'}>USD 1-100000</div>
                <div className={'col-6 m5'}>Comission</div>
                <div className={'col-6 text-right font-weight-bold m5'}>0%</div>
                <div className={'col-6 m5'}>In processing</div>
                <div className={'col-6 text-right font-weight-bold m5'}>Instantly</div>
              </div>
              <button className="btn button button__red button__md mx-auto" disabled={true}>Deposit
              </button>
            </div>
          </div>
        </div>
      </div>
    </Fragment>)
  }
  StageOne = type => {
    switch (type) {
      case 'bitcoin':
        return <Bitcoin submit={this.submit} onBack={() => this.setState({stage: 0})}/>
      case 'exmo':
        return <Exmo submit={this.submit} onBack={() => this.setState({stage: 0})}/>
      case 'wiretransfer':
        return <WireTransfer submit={this.submit} onBack={() => this.setState({stage: 0})}/>
      case 'qiwi':
        return <Qiwi submit={this.submit} onBack={() => this.setState({stage: 0})}/>
      case 'webmoney':
        return <Webmoney submit={this.submit} onBack={() => this.setState({stage: 0})}/>
    }
  }

  render () {
    let {stage, type} = this.state
    return (<Fragment>
        <Header/>
        <main className="main">

          <article>
            <div className="container container-xl">
              <div className="profile">
                <div className="main-content">
                  <div className="profile" style={{paddingBottom: '2rem'}}>
                    {stage === 0 ? this.StageZero() : this.StageOne(type)}
                  </div>
                </div>
              </div>
            </div>
          </article>
        </main>
        <Footer/>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  let {user} = state
  return {user}
}
const mapDispatchToProps = dispatch => {
  return {
    openModal: () => {
      dispatch(openModal('isSuccess'))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Replenishment)
