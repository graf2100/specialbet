import React, { Component, Fragment }     from 'react'
import axios                              from 'axios'
import moment                             from 'moment'
import { Footer, Header, ProfileRouters } from '../../../components'
import { connect }                        from 'react-redux'
import { openModal }                      from '../../../actions'

class Finance extends Component {

  state = {
    finance: [],
    page: 0
  }

  async componentDidMount () {
    let {user} = this.props
    let {data: {status, finance}} = await axios.get('/api/finance/' + user._id)
    if (status === 'Done') {
      this.setState({finance})
    }
  }

  renderItem = ({type, date, sum, note, payment}) => (<tr>
    <td>
      <div className="d-flex justify-content-between">
        <span className="mr-1">{moment(date).format('L')}</span> <span
        className="ml-1">{moment(date).format('LT')}</span>
      </div>
    </td>
    <td>
      {type} {payment}
    </td>
    <td className="text-center"><span className="nowrap">{new Intl.NumberFormat('ru-RU').format(sum)}</span></td>
    <td className="">{note}</td>
  </tr>)

  paginator = () => {
    let {page, finance} = this.state
    let arr = []
    let count = finance.length / 15
    for (let i = 0; i < count; i++) {
      arr.push(i)
    }
    return arr.map(i => <li onClick={() => this.setState({page: i})}
                            className={i === page ? 'page-item active' : 'page-item'}><a
      className="page-link">{i + 1}</a></li>)
  }

  prePage = () => {
    let {page} = this.state
    if (page !== 0) {
      page -= 1
      this.setState({page})
    }
  }

  nextPage = () => {
    let {page, finance} = this.state
    let count = finance.length / 15
    if (page < count) {
      page += 1
      this.setState({page})
    }
  }

  orderedToPay = () => {
    let {finance} = this.state
    let sum = 0
    finance.forEach(f => {
      if (f.type === 'Withdraw') {
        sum += f.sum
      }
    })
    return new Intl.NumberFormat('ru-RU').format(sum)
  }

  render () {
    let {user, openModal, balance} = this.props
    let {finance, page} = this.state
    return (<Fragment>
        <Header/>
        <main className="main">
          <article>
            <div className="container container-xl">
              <div className="profile">
                <div className="main-content">
                  <div className="profile">
                    <ProfileRouters/>
                    <div className="tab-content" id={'history-tab'}>
                      <div className="tab-pane fade active show" id="finance-tab" role="tabpanel"
                           aria-labelledby="finance-tab">
                        <div className="cards-block mx-auto">
                          <h2 className="text-center">Personal Information</h2>
                          <div className="row">
                            <div className="col-lg-6">
                              <div className="card card-block">
                                <div className="card-body d-flex flex-column justify-content-center">
                                  <div className="row">
                                    <div className="col">
                                      <div className="card-block__label">My account</div>
                                    </div>
                                    <div className="col">
                                      <div className="card-block__text text-right"><span>{user._id}</span></div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col">
                                      <div className="card-block__label">Account owner</div>
                                    </div>
                                    <div className="col">
                                      <div className="card-block__text text-right">
                                        <span>{user.lastName} {user.firstName}</span></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <div className="card card-block">
                                <div className="card-body d-flex flex-column justify-content-center">
                                  <div className="row">
                                    <div className="col">
                                      <div className="card-block__label">Unplayed amount</div>
                                    </div>
                                    <div className="col">
                                      <div className="card-block__text text-right">
                                        <span>{new Intl.NumberFormat('ru-RU').format(balance.fund)} USD</span></div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col">
                                      <div className="card-block__label">For payment</div>
                                    </div>
                                    <div className="col">
                                      <div className="card-block__text text-right">
                                        <span>{this.orderedToPay()} USD</span></div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col">
                                      <div className="card-block__label">Available balance</div>
                                    </div>
                                    <div className="col">
                                      <div className="card-block__text text-right">
                                        <span>{new Intl.NumberFormat('ru-RU').format(balance.balance)} USD</span></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <button className="btn button button__red button__transparent btn-block"
                                      onClick={() => openModal('isTransfer')}>Transfer to friend
                              </button>
                            </div>
                          </div>
                        </div>
                        <div className=" ml-4 mr-4">
                          <table className="table table-striped table-hover table-block table-finance">
                            <thead>
                            <tr>
                              <th scope="col" className="text-center">Date and time</th>
                              <th scope="col">Type of operation</th>
                              <th scope="col" className="text-center">Stake</th>
                              <th scope="col" className="">Note</th>
                            </tr>
                            </thead>
                            <tbody>
                            {finance.slice(page * 15, page * 15 + 15).map(f => this.renderItem(f))}
                            </tbody>
                          </table>
                        </div>
                        <nav>
                          <ul className="pagination pagination-block justify-content-center">
                            <li className="page-item" onClick={this.prePage}>
                              <a className="page-link prev" aria-label="Previous">
                                <span/>
                                <span className="sr-only">Previous</span>
                              </a>
                            </li>
                            {this.paginator()}
                            <li className="page-item" onClick={this.nextPage}>
                              <a className="page-link next" aria-label="Next">
                                <span/>
                                <span className="sr-only">Next</span>
                              </a>
                            </li>
                          </ul>
                        </nav>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </main>
        <Footer/>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  let {user, balance} = state
  return {user, balance}
}
const mapDispatchToProps = dispatch => {
  return {
    openModal: modal => {
      dispatch(openModal(modal))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Finance)