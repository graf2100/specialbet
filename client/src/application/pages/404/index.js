import React, { Fragment } from 'react'
import { Footer, Header }  from '../../../components'

import wallet from '../../../images/wallet.png'

export default function notFound () {
  return (<Fragment>
    <Header/>
    <main className="main text-center">
      <img src={wallet} style={{margin: '4rem 0'}} alt={'404'}/>
      <h1 style={{textTransform: 'none', fontSize: '1.5rem',marginBottom:'10rem'}}>Page not found</h1>
    </main>
    <Footer/>
  </Fragment>)
}