import React, { Component, Fragment } from 'react'
import moment                         from 'moment'
import NumberFormat                   from 'react-number-format'

import { Footer, Header, ProfileRouters } from '../../../components'
import { connect }                        from 'react-redux'
import axios                              from 'axios/index'
import { openModal, signIn }              from '../../../actions'

class Profile extends Component {

  constructor (props) {
    super(props)
    this.state = {
      firstName: props.user.firstName,
      lastName: props.user.lastName,
      phone: props.user.phone,
      email: props.user.email,
      birthday: props.user.birthday,
      pass: '',
      newPass: '',
      copyNewPass: ''
    }
  }

  handleSubmit = async e => {
    e.preventDefault()
    let {signIn} = this.props
    let {token} = this.props.user
    let {firstName, lastName, phone, email, birthday} = this.state
    let user = await axios.post('/api/user/change/', {
      firstName, lastName, phone, email, birthday, token
    })
    if (user.data.status === 'Done') {
      signIn({token: user.data.token, ...user.data.user})
      localStorage.setItem('user', JSON.stringify({token: user.data.token, ...user.data.user}))
    }
  }
  handleSubmitPass = async e => {
    e.preventDefault()
    let {token} = this.props.user
    let {pass, newPass, copyNewPass} = this.state
    if (newPass === copyNewPass) {
      let user = await axios.post('/api/user/changePass/', {
        pass, newPass, token
      })
      if (user.data.status = 'Done') {
        signIn({token: user.data.token, ...user.data.user})
        localStorage.setItem('user', JSON.stringify({token: user.data.token, ...user.data.user}))
      } else {
        alert('Пароль неверный')
      }
      this.setState({
        pass: '',
        newPass: '',
        copyNewPass: ''
      })
    } else {
      alert('пароли не совпадают')
    }

  }

  formatDate = date => {
    return moment(date).format('L')
  }

  changeDate = date => {
    let birthday = moment(date.value, 'DDMMYYYY').toDate()
    this.setState({birthday})
  }
  changeField = (value, field) => {
    this.setState({[field]: value})
  }

  render () {
    let {openModal} = this.props
    let {firstName, lastName, phone, email, birthday, pass, copyNewPass, newPass} = this.state
    return (<Fragment>
        <Header/>
        <main className="main">
          <article>
            <div className="container container-xl">
              <div className="profile">
                <div className="main-content">
                  <div className="profile">
                    <ProfileRouters/>
                    <div className="tab-content">
                      <div className="tab-pane fade active show"
                           aria-labelledby="profile-tab">
                        <form className="mx-auto" onSubmit={this.handleSubmit}>
                          <div className="form-block mx-auto">
                            <h2 className="text-center">Personal Information</h2>
                            <div className="row">
                              <div className="col-12 col-sm-6">
                                <div className="form-group">
                                  <label htmlFor="profileInputName">Last name</label>
                                  <input className="form-control" placeholder="Enter the Last name" type="text"
                                         value={firstName}
                                         onChange={e => this.changeField(e.target.value, 'firstName')}/>
                                </div>
                              </div>
                              <div className="col-12 col-sm-6">
                                <div className="form-group">
                                  <label htmlFor="profileInputSurname">First name</label>
                                  <input className="form-control" placeholder="Enter the First name" type="text"
                                         value={lastName}
                                         onChange={e => this.changeField(e.target.value, 'lastName')}
                                  />
                                </div>
                              </div>
                              <div className="col-12 col-sm-6">
                                <div className="form-group">
                                  <label htmlFor="profileInputPhone">Phone</label>
                                  <NumberFormat format="## (###) ###-####" className="form-control" mask="_"
                                                value={phone} type={'tel'}
                                                onValueChange={obj => this.changeField(obj.formattedValue, 'phone')}
                                  />
                                </div>
                              </div>
                              <div className="col-12 col-sm-6">
                                <div className="form-group">
                                  <label htmlFor="profileInputEmail">Email</label>
                                  <input className="form-control" placeholder="Введите email" type="email"
                                         value={email}
                                         onChange={e => this.changeField(e.target.value, 'email')}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-12 col-sm-6">
                                <div className="form-group">
                                  <label className="w-100" htmlFor="profileSelectBirthdateDay">Date of Birth</label>
                                  <NumberFormat format="##/##/####" placeholder="DD/MM/YYYY"
                                                mask={['D', 'D', 'M', 'M', 'Y', 'Y', 'Y', 'Y']}
                                                defaultValue={this.formatDate(birthday)}
                                                onValueChange={this.changeDate}
                                                className="form-control"/>
                                </div>
                              </div>
                              <div className="col-12 col-sm-6">
                                <div className="form-group">
                                  <label/>
                                  <button
                                    className="btn button button__red button__transparent button__md button__100 d-block"
                                    type="button" onClick={() => openModal('isVerification')}>Verification
                                  </button>
                                </div>
                              </div>
                            </div>
                            <button className="btn button button__red button__md mx-auto d-block"
                                    type="submit">Update info
                            </button>
                          </div>

                        </form>
                        <div className="divider"/>
                        <h2 className="text-center">Change password</h2>
                        <div className="form-block mx-auto">
                          <form onSubmit={this.handleSubmitPass}>
                            <div className="row">
                              <div className="form-group col-md-4">
                                <label htmlFor="profileInputPassword">Password</label>
                                <input className="form-control" id="profileInputPassword"
                                       placeholder="Enter the current password" type="password" value={pass}
                                       onChange={e => this.changeField(e.target.value, 'pass')}
                                />
                              </div>
                              <div className="form-group col-md-4">
                                <label htmlFor="profileInputPasswordRepeat">New password</label>
                                <input className="form-control" id="profileInputPasswordRepeat"
                                       placeholder="New password"
                                       type="password" value={newPass}
                                       onChange={e => this.changeField(e.target.value, 'newPass')}/>
                              </div>
                              <div className="form-group col-md-4">
                                <label htmlFor="profileInputPasswordRepeatAgain">Repeat new password</label>
                                <input className="form-control" id="profileInputPasswordRepeatAgain"
                                       placeholder="Repeat new password" type="password" value={copyNewPass}
                                       onChange={e => this.changeField(e.target.value, 'copyNewPass')}
                                />
                              </div>
                              <button className="btn button button__red button__md mx-auto d-block"
                                      type="submit">Change password
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </main>
        <Footer/>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  let {user} = state
  return {user}
}
const mapDispatchToProps = dispatch => {
  return {
    signIn: user => {
      dispatch(signIn(user))
    },
    openModal: modal => {
      dispatch(openModal(modal))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
