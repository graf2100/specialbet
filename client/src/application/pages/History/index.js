import React, { Component, Fragment }     from 'react'
import axios                              from 'axios'
import moment                             from 'moment'
import { Footer, Header, ProfileRouters } from '../../../components'
import { connect }                        from 'react-redux'

class History extends Component {

  state = {
    finance: [],
    bets: [],
    page: 0
  }

  async componentDidMount () {
    await this.requestFinance()
    await this.requestBets()
  }

  requestFinance = async () => {
    let {user} = this.props
    let {data: {status, finance}} = await axios.get('/api/finance/' + user._id)
    if (status === 'Done') {
      this.setState({finance})
    }
  }
  requestBets = async () => {
    let {user} = this.props
    let {data: {status, bets}} = await axios.get('/api/getBets/' + user._id)
    if (status === 'Done') {
      this.setState({bets})
    }
  }

  paginator = () => {
    let {page, bets} = this.state
    let arr = []
    let count = bets.length / 15
    for (let i = 0; i < count; i++) {
      arr.push(i)
    }
    return arr.map(i => <li onClick={() => this.setState({page: i})}
                            className={i === page ? 'page-item active' : 'page-item'}><a
      className="page-link">{i + 1}</a></li>)
  }

  prePage = () => {
    let {page} = this.state
    if (page !== 0) {
      page -= 1
      this.setState({page})
    }
  }

  nextPage = () => {
    let {page, bets} = this.state
    let count = bets.length / 15
    if (page < count) {
      page += 1
      this.setState({page})
    }
  }

  orderedToPay = () => {
    let {finance} = this.state
    let sum = 0
    finance.forEach(f => {
      if (f.type === 'Withdraw') {
        sum += f.sum
      }
    })
    return new Intl.NumberFormat('ru-RU').format(sum)
  }
  renderBets = () => {
    let {bets, page} = this.state
    let data = []
    bets = bets.slice(page * 15, page * 15 + 15)
    bets.forEach(bet => {
      data.push(this.bets(bet))
      data.push(...bet.events.map(event => this.event(event)))
    })
    return [...data]
  }
  cashout = _id => async e => {
    e.preventDefault()
    let {data: {status}} = await axios.post('/api/cashout/', {_id})
    if (status === 'Done') {
      await this.requestBets()
    }
  }

  event = ({coefficient, data, outcomes, status, text}) => (<tr>
    <td className="text-center"/>
    <td>
      <div className="d-flex justify-content-between">
        <span className="mr-1">{data.calendar}</span> <span className="ml-1">{data.time}</span>
      </div>
    </td>
    <td>
      {text}: {outcomes}
    </td>
    <td className="text-center">{coefficient}</td>
    <td className="text-center">{status}</td>
  </tr>)

  bets = ({_id, date, sum, result, isDone, coefficient, isСashOut}) => (<tr>
    <td className="text-center">№ {_id}</td>
    <td>
      <div className="d-flex justify-content-between">
        <span className="mr-1">{moment(date).format('L')}</span> <span
        className="ml-1">{moment(date).format('LT')}</span>
      </div>
    </td>
    <td>
      <div
        className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center">
        <span>Stake: {new Intl.NumberFormat('ru-RU').format(sum)} usd</span>
        {(!isDone && !isСashOut) && <button
          className="btn button button__white button__transparent button__xsss" onClick={this.cashout(_id)}>Cashout
        </button>}

      </div>
    </td>
    <td className="text-center">{coefficient}</td>
    <td className="text-center"> {new Intl.NumberFormat('ru-RU').format(result)} usd</td>
  </tr>)

  render () {
    let {user,balance} = this.props
    return (<Fragment>
        <Header/>
        <main className="main">
          <article>
            <div className="container container-xl">
              <div className="profile">
                <div className="main-content">
                  <div className="profile">
                    <ProfileRouters/>
                    <div className="tab-content" id={'history-tab'}>
                      <div className="tab-pane fade active show"
                           aria-labelledby="profile-tab">
                        <form className="">
                          <div className="cards-block mx-auto">
                            <h2 className="text-center">Personal Information</h2>
                            <div className="row">
                              <div className="col-lg-6">
                                <div className="card card-block">
                                  <div className="card-body d-flex flex-column justify-content-center">
                                    <div className="row">
                                      <div className="col">
                                        <div className="card-block__label">My account</div>
                                      </div>
                                      <div className="col">
                                        <div className="card-block__text text-right"><span>{user._id}</span></div>
                                      </div>
                                    </div>
                                    <div className="row">
                                      <div className="col">
                                        <div className="card-block__label">Account owner</div>
                                      </div>
                                      <div className="col">
                                        <div className="card-block__text text-right">
                                          <span>{user.lastName} {user.firstName}</span></div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-6">
                                <div className="card card-block">
                                  <div className="card-body d-flex flex-column justify-content-center">
                                    <div className="row">
                                      <div className="col">
                                        <div className="card-block__label">Unplayed amount</div>
                                      </div>
                                      <div className="col">
                                        <div className="card-block__text text-right"><span>{new Intl.NumberFormat('ru-RU').format(balance.fund)} USD</span></div>
                                      </div>
                                    </div>
                                    <div className="row">
                                      <div className="col">
                                        <div className="card-block__label">For payment</div>
                                      </div>
                                      <div className="col">
                                        <div className="card-block__text text-right">
                                          <span>{this.orderedToPay()} USD</span></div>
                                      </div>
                                    </div>
                                    <div className="row">
                                      <div className="col">
                                        <div className="card-block__label">Available balance</div>
                                      </div>
                                      <div className="col">
                                        <div className="card-block__text text-right">
                                          <span>{new Intl.NumberFormat('ru-RU').format(balance.balance)} USD</span></div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="table-responsive">
                            <table className="table table-striped table-hover table-block table-bets">
                              <thead>
                              <tr>
                                <th scope="col"/>
                                <th scope="col" className="text-center">Date and time</th>
                                <th scope="col">Event and gathering</th>
                                <th scope="col" className="text-center">Coefficient</th>
                                <th scope="col" className="text-center">Result</th>
                              </tr>
                              </thead>
                              <tbody>
                              {this.renderBets()}
                              </tbody>
                            </table>
                          </div>
                          <nav>
                            <ul className="pagination pagination-block justify-content-center">
                              <li className="page-item" onClick={this.prePage}>
                                <a className="page-link prev" aria-label="Previous">
                                  <span/>
                                  <span className="sr-only">Previous</span>
                                </a>
                              </li>
                              {this.paginator()}
                              <li className="page-item" onClick={this.nextPage}>
                                <a className="page-link next" aria-label="Next">
                                  <span/>
                                  <span className="sr-only">Next</span>
                                </a>
                              </li>
                            </ul>
                          </nav>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </main>
        <Footer/>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  let {user,balance} = state
  return {user,balance}
}
const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(History)