import React, { Component, Fragment } from 'react'

class TabelEvent extends Component {

  renderTournament = (contry, name) => (<div className={'col-12'}>{contry ? contry + ' :' : ''} {name}</div>)

  renderEvent = ({_id, data, status, teamOne, teamTwo, score, isActive}, tournament, onAdd) => isActive ? (
    <div className={'col-12 text-dark'} onClick={() => onAdd({data, text: `${tournament}. ${teamOne} - ${teamTwo}`})}
         key={'event-' + _id}>
      <div className={'row  bg-white text-center readHover'} style={{cursor: 'default'}}>
        <div className={'col-2 tabelEvent align-middle pt-2 pb-2'}>
          {data.time} {data.calendar}
        </div>
        <div className={'col-4 tabelEvent align-middle text-right pt-2 pb-2'}>
          {teamOne}
        </div>
        <div className={'col-4 tabelEvent align-middle text-left pt-2 pb-2'}>
          {teamTwo}
        </div>
        <div className={'col-2 tabelEvent align-middle pt-2 pb-2'}>
          <button type="button" className="btn btn-danger btn-sm position-absolute"
                  style={{marginTop: '-2.5%', marginLeft: '-37%'}}>Place bets
          </button>
        </div>
      </div>
    </div>) : undefined

  render () {
    let {tournaments, openModal, events} = this.props
    return tournaments.length ? (
      <div className="table-responsive">
        <div className={'ml-4 mr-4'}>
          <ul className="nav">
            <li className="nav-item" style={{background: '#15245b'}}>
              <a className="nav-link">All</a>
            </li>
          </ul>
          <div style={{background: '#15245b', color: ''}}>
            {tournaments.map((tournament, index) => <Fragment key={'tournament-' + index}>
              {events.filter(e => e.tournament === tournament._id.toString()).length ? this.renderTournament(tournament.сountry, tournament.name) : undefined}
              {events.filter(e => e.tournament === tournament._id.toString()).map(e => this.renderEvent(e, tournament.name, openModal))}
            </Fragment>)}
          </div>
        </div>
      </div>) : null
  }
}

export default TabelEvent