import React, { Component, Fragment }                                               from 'react'
import { Footer, Header }                                                           from '../../../components'
import { TabelEvent }                                                               from './components'
import banner                                                                       from '../../../images/banner.png'
import { connect }                                                                  from 'react-redux'
import { changeType, openModal, requestEvents, requestTournament, setSelectBasket } from '../../../actions'

class Home extends Component {

  constructor (props) {
    super(props)

    this.iframe = React.createRef()
  }

  state = {
    games: [
      {label: 'Футбол', isActive: true},
      {label: 'Теннис', isActive: false},
      {label: 'Хоккей', isActive: false},
      {label: 'Баскетбол', isActive: false},
      {label: 'Футзал', isActive: false},
      {label: 'Бокс', isActive: false}]
  }

  componentDidMount () {
    this.props.request()
  }

  render () {
    let {type, tournaments, changeType, openModal,events} = this.props
    return (
      <Fragment>
        <Header/>
        <main className="main">
          <article>
            <h1 className="sr-only">Special Bet</h1>
            <div className="container container-lg">
              <div className="main-banner"><img alt="main-banner" className="img-fluid" src={banner}/>
              </div>
              <div className="matches">
                <ul className="nav nav-tabs nav-fill tabs justify-content-start justify-content-md-between">
                  {type.map((g, i) => <li className="nav-item" key={'nav-item-' + i}
                                          onClick={() => changeType(i)}>
                    <a className={g.isActive ? 'nav-link active' : 'nav-link'} href="javascript:void(0);">{g.name}</a>
                  </li>)}
                </ul>
                <div className="tab-content" style={{background: '#111d3d'}}>
                  <div className="tab-pane fade show active">
                    <TabelEvent tournaments={tournaments} events={events} openModal={openModal}/>
                  </div>
                </div>
              </div>
            </div>
          </article>
          <Footer/>
        </main>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  let {type, tournament, events} = state
  let idType = type.find(t => t.isActive)
  idType = idType ? idType._id : 0
  let tournaments = tournament.filter(e => e.type === idType)
  return {type, tournaments, events}
}
const mapDispatchToProps = dispatch => {
  return {
    request: () => {
      requestTournament()
      requestEvents()
    },
    changeType: k => {
      dispatch(changeType(k))
    },
    openModal: event => {
      dispatch(setSelectBasket(event))
      dispatch(openModal('isOutComes'))
    },

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)