import React, { Component, Fragment } from 'react'
import { Footer, Header }             from '../../../components'

export default class AboutUs extends Component {

  render () {
    return (
      <Fragment>
        <Header/>
        <main className="main">
          <article>
            <div className="container container-xl">
              <div className="about-us">
                <div className="heading">
                  <h1>About us</h1>
                </div>
                <div className="main-content">
                  <h2>Specailbet</h2>
                  <p>This is a sports trading platform. We are a major player in the international gaming industry,
                    licensed and regulated by the Gaming Corporation (PAGCOR). SP company provides an opportunity to
                    place bets on sporting events since 2017 at any place and at any time. Here you will find your
                    personal path to success: the best coefficients and high limits for any sport event, fast and
                    comfortable withdrawal of funds using various payment methods. The Specialbet concept was developed
                    by a group of industry experts with over a decade of experience. Its every detail was carefully
                    planned and after several hours of discussions between our development team. The first few functions
                    that we have introduced are: artificial intelligence, which allows you to choose the best bookmaker
                    and the best coefficient for your chosen outcome of a sporting event. SP is a huge selection of
                    outcomes for any sporting event you have chosen on our site. Our artificial intelligence allows you
                    to instantly select the best offer on the sports betting market for your outcome. The highest limits
                    for sports betting, SP stands out because it constantly offers the highest limits for sports
                    betting, which allowed us to attract professional players. A distinctive feature of Specailbet from
                    other platforms is that the company sets the same limit on any outcome of the match, regardless of
                    the favorite and the rate of your chosen sporting event.
                  </p>
                  <h2>Security</h2>
                  <p>Your security and privacy is a big importance for us. Specailbet has set a goal to ensure maximum
                    protection of users' personal data from loss, manipulation and illegal access to this data by third
                    parties. We use modern SSL encryption on all pages of the site that contain personal information of
                    the user, and on our servers - the most modern firewalls. All these measures ensure the high
                    security of our platform, and allow SP to store and process personal data of its customers in the
                    most reliable way.
                  </p>
                  <h2>Support</h2>
                  <p>The high level of service is the hallmark of our company. Specailbet provides excellent customer
                    support to its users. Any questions you may have, our professional and friendly customer support
                    staff is gready to help you 24/7. With any question you are interested in, you can contact our
                    support team 24/7<br/>
                    <a href={'mailto:support@specialone.bet'}>support@specialone.bet</a>
                    <br/><a href={'tel:+447443150050'}>+447443150050</a>
                  </p>
                  <h2>Why us</h2>
                  <p>The SP team consists of highly qualified and dedicated professionals who make every effort to
                    create the most reliable and secure platform, with an honest, responsible and comfortable gaming
                    atmosphere. Thanks to the desire of our company to continuously improving its services, the odds of
                    winnings and the conditions for playing Specailbet are better than most other platforms.
                  </p>
                  <p>For those who prefer using their time efficiently, –SP is the ideal solution. It does not matter
                    whether you are new to our site or an experienced player in search of a reliable bookmaker office -
                    We are sure that you will enjoy playing with us. Our team will do everything possible to make you
                    satisfied by choosing us as your bookmaker. The best choose the best.
                  </p>
                </div>
              </div>
            </div>
          </article>
        </main>
        <Footer/>
      </Fragment>
    )
  }
};
