import React, { Component, Fragment } from 'react'

class WireTransfer extends Component {

  render () {
    let {onBack} = this.props
    return (<Fragment>
      <div className={'test'}>GAMING ACCOUNT UPDATE WITH WIRE TRANSFER
      </div>
      <div style={{width: '80%', margin: '0 10%'}}>
        <div className={'row'}>
          <div className={'col-8 mt-4'} style={{color: '#fff'}}>
            <p className={'d-block ml-5 mt-2 '} onClick={onBack}>return to withdrawal</p>
          </div>
        </div>
      </div>
      <hr style={{background: '#eeee'}}/>
      <div style={{width: '80%', margin: '0 10%'}}>
        <p className={'font-weight-bold d-block mt-4'}>Attention!</p>
        <p>For additional questions, please contact us</p>
      </div>
    </Fragment>)
  }
}

export default WireTransfer