import Bitcoin      from './Bitcoin'
import Exmo         from './Exmo'
import WireTransfer from './WireTransfer'
import Qiwi         from './Qiwi'
import Webmoney     from './Webmoney'

export { Bitcoin, Exmo, WireTransfer, Qiwi, Webmoney }