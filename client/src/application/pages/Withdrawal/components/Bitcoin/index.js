import React, { Component, Fragment } from 'react'
import { connect }                    from 'react-redux'

class Bitcoin extends Component {
  constructor (props) {
    super(props)
    this.state = {
      number: '1234123412341254',
      sum: 0,
    }
  }

  copy = () => {
    document.execCommand(this.state.number)
  }

  change = (val, key) => (this.setState({[key]: val}))

  render () {
    let {onBack, submit} = this.props
    let {sum} = this.state
    return (<Fragment>
      <div className={'test'}>GAMING ACCOUNT UPDATE WITH BITCOIN
      </div>
      <div style={{width: '80%', margin: '0 10%'}}>
        <div className={'row'}>
          <div className={'col-4 mt-4'} style={{color: '#fff'}}>
            <div className="form-group">
              <label style={{fontSize: '0.9rem'}}>Amount</label>
              <input className="form-control" placeholder="Amount" type="number" required=""
                     onChange={e => this.change(e.target.value, 'sum')} value={sum}/>
            </div>
            <button className="btn button button__red button__md btn-block" onClick={() => {
              submit('Вывод средств Bitcoin, сумма: ' + sum)
              onBack()
            }}>
              Send request
            </button>
            <p className={'d-block ml-5 mt-2'} onClick={onBack}>return to withdrawal</p>
          </div>
        </div>
      </div>
      <hr style={{background: '#eeee'}}/>
      <div style={{width: '80%', margin: '0 10%'}}>
        <p className={'font-weight-bold d-block mt-4'}>Attention!</p>
        <p>Enter the amount and number of the Bitcoin wallet and wait for an answer to the mail</p>
      </div>
    </Fragment>)
  }
}

const mapStateToProps = state => {
  let {user} = state
  return {user}
}
const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Bitcoin)