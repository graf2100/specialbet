import React       from 'react'
import { NavLink } from 'react-router-dom'

const profileRouters = () => {
  return (<ul className="nav nav-tabs tabs justify-content-center">
    <li className="nav-item">
      <NavLink className="nav-link" to={'/profile/'} activeClassName="active show">Account</NavLink>
    </li>
    <li className="nav-item">
      <NavLink className="nav-link" to={'/history/'} activeClassName="active show">Betting history</NavLink>
    </li>
    <li className="nav-item">
      <NavLink className="nav-link" to={'/finance/'} activeClassName="active show">Finance</NavLink>
    </li>
  </ul>)
}

export default profileRouters