import React, { Component } from 'react'

import cart                   from '../../../../images/cart.png'
import { openModal, signOut } from '../../../../actions'
import { connect }            from 'react-redux'
import { Link }               from 'react-router-dom'

class User extends Component {
  state = {
    menu: false,
  }

  showMenu = () => {
    let {menu} = this.state
    menu = !menu
    this.setState({menu})
    setTimeout(() => { this.setState({menu: false})}, 10000)
  }
  output = () => {
    localStorage.clear()
    this.props.signOut()
  }

  render () {
    let {isAuth, openModal, user, basket} = this.props
    let {menu} = this.state
    return isAuth ? (
      <div className={'authorization'}>
        <div className="cart d-flex">
          <div className="cart__icon" onClick={() => openModal('isBasket')}><img alt="cart" src={cart}/></div>
          <div className="cart__title">
            Bet slip:
          </div>
          <div className="cart__total">
            {basket.events.length}
          </div>
          <div className={menu ? 'dropdown dropdown-block show' : 'dropdown dropdown-block'}>
            <button className="btn dropdown-toggle align-items-center d-flex align-self-stretch" onClick={this.showMenu}
                    type="button">
              <span className="user-avatar"/>
              <span className="user-name">{user.firstName + ' ' + user.lastName}</span>
            </button>
            <div className={menu ? 'dropdown-menu show' : 'dropdown-menu'}>
              <Link className={'dropdown-item'} style={{position: 'initial'}} to={'/profile/'}>Account</Link>
              <Link className={'dropdown-item'} style={{position: 'initial'}} to={'/history/'}>Betting history</Link>
              <Link className={'dropdown-item'} style={{position: 'initial'}} to={'/finance/'}>Finance</Link>
              <Link className={'dropdown-item'} style={{position: 'initial'}} to={'/replenishment/'}>Deposit</Link>
              <Link className={'dropdown-item'} style={{position: 'initial'}} to={'/withdrawal'}>Withdrawal</Link>
              <a className="dropdown-item" id={'logOut'} style={{position: 'initial'}} href="javascript:void(0)"
                 onClick={this.output}>Log Out</a>
            </div>
          </div>
        </div>
      </div>) : (
      <div className="authorization">
        <button className="btn button button__red button__transparent button__xss"
                onClick={() => openModal('isLogin')}>Login
        </button>
        <button className="btn button button__red button__xs"
                onClick={() => openModal('isRegistration')}>Registration
        </button>
      </div>
    )
  }
}

const mapStateToProps = state => {
  let {user, basket} = state
  let isAuth = !!user
  return {user, isAuth, basket}
}
const mapDispatchToProps = dispatch => {
  return {
    openModal: modal => {
      dispatch(openModal(modal))
    },
    signOut: () => {
      dispatch(signOut())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(User)
