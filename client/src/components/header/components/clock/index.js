import React, { Component } from 'react'
import moment from 'moment'
import 'moment/locale/ru'

moment.locale('ru')

export default class Clock extends Component {

  constructor (props) {
    super(props)
    this.state = {
      date: moment().format('L'),
      clock: moment().format('LT'),
    }
  }

  componentDidMount () {
    setInterval(() => {
      let date = moment().format('L')
      let clock = moment().format('LT')
      this.setState({date, clock})
    }, 1000)
  }

  render () {
    let {date, clock} = this.state
    return (<div className="time d-flex">
      <div className="time__date">
        {date}
      </div>
      <div className="time__clock">
        {clock}
      </div>
    </div>)
  }
}