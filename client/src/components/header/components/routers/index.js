import React, { Component } from 'react'
import { Link }             from 'react-router-dom'
import { withRouter }       from 'react-router'

class Routers extends Component {
  routers = [
    {label: 'Home', router: '/'},
    /* {label: 'Ставки', router: '/bets/'},
     {label: 'Результаты', router: '/result/'},*/
    {label: 'Help', router: '/helps/'},
    {label: 'About us', router: '/about-us/'},
    {label: 'Rules', router: '/rule/'}
  ]

  render () {
    let {match} = this.props
    return (<nav className="navbar navbar-expand-md navigation">
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mx-auto">
          {this.routers.map((r, k) => <li className={match.url === r.router ? 'nav-item active' : 'nav-item'}
                                          key={'nav-item-' + k}>
            <Link className={'nav-link'} to={r.router}>{r.label} </Link>
          </li>)}
        </ul>
      </div>
    </nav>)
  }
}

export default withRouter(Routers)