import React, { Component } from 'react'

import { Clock, Routers, User } from './components'

import logo from '../../images/logo.png'

export default class Header extends Component {

  state = {}

  render () {
    return (<header>
      <div className="infobar d-flex flex-lg-row justify-content-lg-between align-items-center flex-wrap">
        <a className="navbar-brand logo align-items-center" href="/">
          <img alt="logo" src={logo}/>
        </a>
        <Clock/>
        <User/>
      </div>
      <Routers/>
    </header>)
  }
}