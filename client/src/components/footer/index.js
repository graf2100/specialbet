import React      from 'react'
import mastercard from '../../images/mastercard.png'
import visa       from '../../images/visa.png'
import webmoney   from '../../images/webmoney.png'
import qiwi       from '../../images/qiwi.png'
import exmo       from '../../images/exmo.png'
import logo       from '../../images/logo.png'
import sign       from '../../images/sign.png'

export default function Footer () {
  return (<footer className="footer">
    <div className="footer__wrapper">
      <div
        className="footer__top d-flex flex-column flex-xl-row justify-content-xl-between align-items-center flex-wrap">
        <div className="footer__logo"><img alt="logo" src={logo}/></div>
        <div className="footer__partners flex-xl-fill flex-wrap">
          <img alt="exmo" src={exmo}/>
          <img alt="qiwi" src={qiwi}/>
          <img alt="webmoney" src={webmoney}/>
          <img alt="visa" src={visa}/>
          <img alt="mastercard" src={mastercard}/></div>
        <div className="footer__majority-sign"><img alt="majority-sign" className="img-responsive" src={sign}/>
        </div>
      </div>
    </div>
  </footer>)
}