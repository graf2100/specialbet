import React, { Component } from 'react'
import close                from '../../../images/close.png'

class RegistrationEnd extends Component {

  render () {
    let {isShow, onClose} = this.props
    return (
      <div
        className={isShow ? 'd-block modal modal-block fade show b-shadow animated fast fadeIn' : 'd-none modal modal-block fade animated fadeOut'}
        role="dialog" tabIndex="-1" onClick={this.handleClose}>
        <div className="modal-dialog modal-block__registration-end animated fast fadeInDown" role="document">
          <div className="modal-content">
            <div className="modal-header text-center">
              <h5 className="modal-title w-100">Complete registration</h5>
              <button className="close" type="button" onClick={() => onClose('isLogin')}>
                <img alt="modal-close" src={close}/>
              </button>
            </div>
            <div className="modal-body">
              <form className="form-block mx-auto">
                <p className="modal-text">To complete the registration and start winning, follow the link that we sent
                  you in the mail! The link will be active for 45 minutes.</p>
                <p className="modal-text">
                  Did not get the email? Check to see if it got into spam, or try to request another one.</p>
                <button className="btn button button__red button__md mx-auto d-block" onClick={this.handleClose}
                        type="button">Resubmit
                </button>
              </form>
            </div>
          </div>
          <div className="modal-footer"/>
        </div>
      </div>
    )
  }
}

export default RegistrationEnd