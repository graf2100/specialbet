import React, { Component } from 'react'

import close from '../../../images/close.png'
import axios from 'axios/index'

class Transfer extends Component {

  state = {
    id: '',
    sum: '',
  }

  handleChange = (e, k) => {
    if (k === 'isRemember') {
      let {isRemember} = this.state
      this.setState({isRemember: !isRemember})
    } else {
      this.setState({[k]: e.target.value})
    }
  }
  handleClose = e => {
    let {onClose} = this.props
    if (e.target.getAttribute('role')) {
      onClose('isTransfer')
    }
  }
  submit = async e => {
    e.preventDefault()
    let {id, sum} = this.state
    let {onClose, user} = this.props
    let {token} = user
    if (id && sum && token) {
      try {
        let user = await axios.post('/api/transfer', {
          id, sum, token
        })
        if (user.data.status === 'Done') {
          onClose('isTransfer')
        } else {
          onClose('isTransfer')
        }
      } catch (e) {
        console.log(e)
        onClose('isTransfer')
      }
    }
  }

  render () {
    let {isShow, onClose} = this.props
    let {id, sum} = this.state
    return (<div
      className={isShow ? 'd-block modal modal-block fade show b-shadow animated fast fadeIn' : 'd-none modal modal-block fade animated fadeOut'}
      role="dialog" tabIndex="-1" onClick={this.handleClose}>
      <div className="modal-dialog modal-block__login animated fast fadeInDown" role="document">
        <div className="modal-content">
          <div className="modal-header text-center">
            <h5 className="modal-title w-100">Money transfer</h5>
            <button className="close" type="button" onClick={() => onClose('isTransfer')}>
              <img alt="modal-close" src={close}/>
            </button>
          </div>
          <div className="modal-body">
            <form className="form-block mx-auto" onSubmit={this.submit}>
              <div className="row">
                <div className="col-12">
                  <div className="form-group">
                    <input className="form-control" placeholder="ID" type="text" value={id}
                           onChange={e => this.handleChange(e, 'id')} required/>
                  </div>
                  <div className="form-group">
                    <input className="form-control" placeholder="Stake" type="number" value={sum}
                           onChange={e => this.handleChange(e, 'sum')} required/>
                  </div>
                </div>
              </div>
              <button className="btn button button__red button__md mx-auto d-block" type="submit">Submit
              </button>
            </form>
          </div>
          <div className="modal-footer"/>
        </div>
      </div>
    </div>)
  }
}

export default Transfer