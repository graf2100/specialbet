import React, { Component } from 'react'
import close                from '../../../images/close.png'

class OutComes extends Component {

  state = {
    active: '',
    index: 0,
  }

  handleClose = e => {
    let {onClose} = this.props
    if (e.target.getAttribute('role')) {
      onClose('isOutComes')
    }
  }
  handelSelect = index => {
    this.setState({index})
  }
  onActive = active => this.setState({active})

  submit = () => {
    let {onClose, data, addEvent} = this.props
    let {active, index} = this.state
    if (active) {
      onClose('isOutComes')
      addEvent(data[index].label + ' ' + active)
    }
  }

  renderItems () {
    let {active, index} = this.state
    let {data} = this.props
    if (data[index]) {
      if (data[index].values)
        console.log(data[index].values.map(val => val))
      return data[index].values.map((val, index) => (
        <div className={'col-4 mt-3 '} key={'outcomes-' + val + index}>
          <div onClick={() => this.onActive(val)}
               className={val === active ? 'mx-auto pt-3 active' : 'mx-auto pt-3'}
               style={{width: 60, height: 60, userSelect: 'none', cursor: 'pointer'}}>{val}</div>
        </div>))
    }
  }

  render () {
    let {active, index} = this.state
    let {isShow, onClose, data} = this.props
    return (
      <div
        className={isShow ? 'd-block modal modal-block fade show b-shadow animated fast fadeIn' : 'd-none modal modal-block fade animated fadeOut'}
        role="dialog" tabIndex="-1" onClick={this.handleClose}>
        <div className="modal-dialog modal-block__login animated fast fadeInDown" role="document">
          <div className="modal-content">
            <div className="modal-header text-center">
              <button className="close" type="button" onClick={() => onClose('isOutComes')}>
                <img alt="modal-close" src={close}/>
              </button>
            </div>
            <div className="modal-body text-center">
              <h1 className={'mt-3'}><strong>Choice of outcome</strong></h1>
              <div className={'row'}>
                <div className={'col-2'}/>
                <div className={'col-8'}>
                  <div className="form-group mt-3">
                    <select className="form-control" onChange={(e) => this.handelSelect(e.target.value)}>
                      {data.map((d, i) => <option selected={index === i} value={i}
                                                  key={d.label}>{d.label}</option>)}
                    </select>
                  </div>
                  <div className={'mt-3 blockOutComes'} style={{background: '#fff', overflowX: 'hidden'}}>
                    <div className={'row text-center'}>
                      {this.renderItems()}
                    </div>
                  </div>
                  <button className="btn button button__red button__md btn-block mt-3" onClick={this.submit}>Add to bet
                  </button>
                </div>
                <div className={'col-2'}/>
              </div>
            </div>
            <div className="modal-footer"/>
          </div>
        </div>
      </div>
    )
  }
}

export default OutComes