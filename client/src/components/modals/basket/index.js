import React, { Component } from 'react'
import close                from '../../../images/close.png'

class Basket extends Component {

  handleClose = e => e.target.getAttribute('role') ? this.props.onClose('isBasket') : undefined

  renderEvent = (e, i) => {
    let {removeEvent} = this.props
    return (<tr key={'tr-' + e.id}>
      <td>
        <div className="d-flex justify-content-between"><span className="mr-1">{e.data.calendar}</span> <span
          className="ml-1">{e.data.time}</span></div>
      </td>
      <td>{e.text}</td>
      <td className="text-center"><span className="nowrap">{e.outcomes ? e.outcomes : null}</span></td>
      <td className="text-center"/>
      <td>
        <button className="btn button button__red button__transparent border-0 m-0" onClick={() => removeEvent(i)}><img
          alt="close"
          src={close}/>
        </button>
      </td>
    </tr>)
  }

  render () {
    let {isShow, onClose, user, basket, changeSum, send, balance} = this.props
    return user && (
      <div
        className={isShow ? 'd-block modal modal-block fade show b-shadow animated fast fadeIn' : 'd-none modal modal-block fade animated fadeOut'}
        role="dialog" tabIndex="-1" onClick={this.handleClose}>
        <div className="modal-dialog modal-block__bet animated fast fadeInDown" role="document">
          <div className="modal-content">
            <div className="modal-header text-center">
              <h5 className="modal-title w-100">Bet № {basket.id}</h5>
              <button aria-label="Close" className="close" type="button" onClick={() => onClose('isBasket')}><img
                alt="modal-close" src={close}/></button>
            </div>
            <div className="modal-body">
              <form className="form-block mx-auto">
                <div className="cards-block mx-auto mb-3">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="card card-block m-0">
                        <div className="card-body d-flex flex-column justify-content-center">
                          <div className="row">
                            <div className="col">
                              <div className="card-block__label">My account</div>
                            </div>
                            <div className="col">
                              <div className="card-block__text text-right"><span>{user._id}</span></div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col">
                              <div className="card-block__label">Account owner</div>
                            </div>
                            <div className="col">
                              <div className="card-block__text text-right"><span>{user.lastName} {user.firstName}</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="card card-block m-0">
                        <div className="card-body d-flex flex-column justify-content-center">
                          <div className="row">
                            <div className="col">
                              <div className="card-block__label">Available balance</div>
                            </div>
                            <div className="col">
                              <div className="card-block__text text-right">
                                <span>{new Intl.NumberFormat('ru-RU').format(balance.balance)} USD</span>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col">
                              <div className="card-block__label">Unplayed amount</div>
                            </div>
                            <div className="col">
                              <div className="card-block__text text-right">
                                <span>{new Intl.NumberFormat('ru-RU').format(balance.fund)} USD</span></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="table-responsive">
                  <table className="table table-striped table-hover table-block table-bets">
                    <thead>
                    <tr>
                      <th scope="col" className="text-center">Date and time</th>
                      <th scope="col">Event</th>
                      <th scope="col" className="text-center">Result</th>
                      <th scope="col" className="">Coefficient</th>
                      <th scope="col" className=""/>
                    </tr>
                    </thead>
                    <tbody>
                    {basket.events.map((e, i) => this.renderEvent(e, i))}
                    </tbody>
                  </table>
                </div>
                <div className="total d-flex justify-content-end mt-4 mb-4">
                  <div className="total__label">Stake</div>
                  <input className={'total__text bg-transparent text-white'} type={'number'} onChange={changeSum}
                         value={basket.sum}/>
                </div>
                <button className="btn button button__red button__md mx-auto d-block" data-dismiss="modal"
                        disabled={basket.sum > balance.balance}
                        type="button" onClick={send}>Receive coefficient
                </button>
              </form>
            </div>
            <div className="modal-footer"/>
          </div>
        </div>
      </div>
    )
  }
}

export default Basket