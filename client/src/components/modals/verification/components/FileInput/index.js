import React, { Component, Fragment } from 'react'
import axios                          from 'axios'
import Clip                           from '../../../../../images/clipRed.svg'
import Setting                        from '../../../../../images/setting.svg'
import * as firebase                  from 'firebase'

let config = {
  apiKey: 'AIzaSyD4ACbMb1gb739juofDolz1ZIIEgcVuTiU',
  authDomain: 'specilabet.firebaseapp.com',
  databaseURL: 'https://specilabet.firebaseio.com',
  projectId: 'specilabet',
  storageBucket: 'specilabet.appspot.com',
  messagingSenderId: '669716610725'
}

firebase.initializeApp(config)

class FileInput extends Component {
  state = {
    isLoad: false,
    isComplete: false,
    progress: 0,
  }

  upload = (e) => {
    let {user} = this.props
    let file = e.target.files[0]
    let ref = firebase.storage().ref('images/' + user._id + '/' + file.name)
    let task = ref.put(file)

    task.on('state_changed', snapshot => {
      let progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
      this.setState({progress, isLoad: true})
    }, err => {console.log(err)}, () => {
      task.snapshot.ref.getDownloadURL().then(async url => {
        let data = await   axios.post('/api/upload/images', {url, user: user._id})
        if (data) {
          this.setState({isComplete: true, isLoad: false})
        }
      })

    })
  }

  render () {
    let {isLoad, progress, isComplete} = this.state
    return (<Fragment>
      <label className={isComplete ? 'upload uploadDone' : 'upload'}><img className={isLoad === true ? 'rot' : ''}
                                                                          src={isLoad ? Setting : Clip}/> {isLoad ? progress + ' %' : isComplete ? 'Uploading' : 'Upload'}
        <input accept="image/png, image/jpeg" type={'file'}
               className={'invisible position-absolute'} onChange={this.upload}/>
      </label>
    </Fragment>)
  }
}

export default FileInput