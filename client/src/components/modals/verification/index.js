import React, { Component } from 'react'
import close                from '../../../images/close.png'

import { FileInput } from './components'

class Verification extends Component {

  handleClose = e => {
    let {onClose} = this.props
    if (e.target.getAttribute('role')) {
      onClose('isVerification')
    }
  }

  render () {
    let {isShow, onClose, user} = this.props
    return (
      <div
        className={isShow ? 'd-block modal modal-block fade show b-shadow animated fast fadeIn' : 'd-none modal modal-block fade animated fadeOut'}
        role="dialog" tabIndex="-1" onClick={this.handleClose}>
        <div className="modal-dialog modal-block__password-recovery animated fast fadeInDown" role="document"
             style={{minWidth: '60%'}}>
          <div className="modal-content">
            <div className="modal-header text-center">
              <h5 className="modal-title w-100">Verification</h5>
              <button className="close" type="button" onClick={() => onClose('isVerification')}>
                <img alt="modal-close" src={close}/>
              </button>
            </div>
            <div className="modal-body text-center">
              <p>Documents for verification:</p>
              <div style={{
                width: '80%',
                margin: '0 10%'
              }} className={'text-left'}>
                <form>
                  <p>1. Identification:<br/>Please upload a photo or color scan of your passport or driver's license.
                  </p>
                  <FileInput user={user}/>
                </form>
                <button className="btn button button__red button__md mx-auto d-block" type="submit"
                        onClick={() => onClose('isVerification')}>Save
                </button>
              </div>
            </div>
          </div>
          <div className="modal-footer"/>
        </div>
      </div>
    )
  }
}

export default Verification