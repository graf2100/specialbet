import React, { Component } from 'react'
import axios                from 'axios'
import close                from '../../../images/close.png'
import NumberFormat         from 'react-number-format'
import moment               from 'moment/moment'

class Registration extends Component {
  state = {
    email: '',
    pass: '',
    copyPass: '',
    phone: '',
    last: '',
    first: '',
    birthday: '',
    is18: false,
    isError: false
  }

  handaleChange = (val, k) => {
    if (k === 'is18') {
      let {is18} = this.state
      this.setState({is18: !is18})
    } else {
      this.setState({[k]: val})
    }
  }

  handleClose = e => {
    let {onClose} = this.props
    if (e.target.getAttribute('role')) {
      onClose('isRegistration')
    }
  }

  formatDate = date => {
    return moment(date).format('L')
  }

  changeDate = date => {
    let birthday = moment(date.value, 'DDMMYYYY').toDate()
    this.setState({birthday})
  }

  handleSubmit = async e => {
    e.preventDefault()
    let {email, pass, copyPass, phone, last, first, is18, birthday} = this.state
    let {redirect, signIn} = this.props
    if (email && pass && copyPass && phone && last && first && is18) {
      let user = await axios.post('/api/signup/', {
        email, password: pass, phone, lastName: last, firstName: first, birthday
      })
      if (user.data.status === 'Done') {
        redirect('isRegistrationEnd')
      } else {
        this.setState({isError: true})
      }
    }
  }

  render () {
    let {isShow, onClose, redirect} = this.props
    let {email, pass, copyPass, phone, last, first, is18, birthday, isError} = this.state
    return (
      <div
        className={isShow ? 'd-block modal modal-block fade show b-shadow animated fast fadeIn' : 'd-none modal modal-block fade'}
        role="dialog"
        tabIndex="-1" onClick={this.handleClose}>
        <div className="modal-dialog modal-block__registration animated fast fadeInDown" role="document">
          <div className="modal-content">
            <div className="modal-header text-center">
              <h5 className="modal-title w-100">Registration</h5>
              <button className="close" type="button" onClick={() => onClose('isRegistration')}>
                <img alt="modal-close" src={close}/>
              </button>
            </div>
            <div className="modal-body">
              {isError && <div className="alert alert-danger" style={{width: '70%', left: '15%'}} role="alert">
                This mail is already taken!
              </div>}
              <form className="form-block mx-auto" onSubmit={this.handleSubmit}>
                <div className="row">
                  <div className="col-12 col-sm-6">
                    <div className="form-group">
                      <label>Email</label>
                      <input className="form-control" placeholder="Enter the e-mail" type="email" value={email}
                             onChange={e => this.handaleChange(e.target.value, 'email')} required/>
                      <small className="form-text valid d-none">Validation successfull</small>
                    </div>
                    <div className="form-group">
                      <label>Password</label>
                      <input className="form-control" placeholder="No fewer than eight signs" type="password"
                             value={pass}
                             onChange={e => this.handaleChange(e.target.value, 'pass')}
                             required/>
                      <small className="form-text invalid d-none">Please provide a valid password.</small>
                    </div>
                    <div className="form-group">
                      <label>Repeat Password</label>
                      <input className="form-control" placeholder="No fewer than eight signs" type="password"
                             value={copyPass}
                             onChange={e => this.handaleChange(e.target.value, 'copyPass')}
                             required/>
                    </div>
                  </div>
                  <div className="col-12 col-sm-6">
                    <div className="form-group">
                      <label>First name</label>
                      <input className="form-control" placeholder="Enter the First name" type="text"
                             value={last}
                             onChange={e => this.handaleChange(e.target.value, 'last')}
                             required/>
                    </div>
                    <div className="form-group">
                      <label>Last name</label>
                      <input className="form-control" placeholder="Enter the Last name" type="text"
                             value={first}
                             onChange={e => this.handaleChange(e.target.value, 'first')}
                             required/>
                    </div>
                    <div className="form-group">
                      <label>Phone</label>
                      <NumberFormat format="## (###) ###-####" className="form-control" mask="_"
                                    placeholder="__ (___) ___-____"
                                    value={phone} type={'tel'}
                                    onValueChange={obj => this.handaleChange(obj.formattedValue, 'phone')}
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-sm-6">
                    <div className="custom-control custom-checkbox checkbox-block">
                      <input className="custom-control-input" id="is18" type="checkbox"
                             onChange={e => this.handaleChange(e.target.value, 'is18')} checked={is18} required/>
                      <label htmlFor="is18" className="custom-control-label">
                        I am over 18 years old and I agree with the Offer Agreement published on this site</label>
                    </div>
                  </div>
                  <div className="col-12 col-sm-6">
                    <div className="form-group">
                      <label className="w-100">Date of Birth</label>
                      <NumberFormat format="##/##/####" placeholder="DD/MM/YYYY"
                                    mask={['D', 'D', 'M', 'M', 'Y', 'Y', 'Y', 'Y']}
                                    defaultValue={this.formatDate(birthday)}
                                    onValueChange={this.changeDate}
                                    className="form-control"/>
                    </div>
                  </div>
                </div>
                <input className="btn button button__red button__md mx-auto d-block" type="submit"
                       value={'Confirm'}/>
                <div className="text-center">
                  <a className="link" href="javascript:void(0)" onClick={() => redirect('isLogin')}>
                    Already a member?</a>
                  <a className="link link__red" href="javascript:void(0)" onClick={() => redirect('isLogin')}>Login</a>
                </div>
              </form>
            </div>
            <div className="modal-footer"/>
          </div>
        </div>
      </div>)
  }
}

export default Registration