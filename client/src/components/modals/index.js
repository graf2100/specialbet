import React, { Component, Fragment } from 'react'
import { connect }                    from 'react-redux'
import Registration                   from './registration'
import Login                          from './login'
import Recovery                       from './recovery'
import RegistrationEnd                from './registrationEnd'
import Done                           from './done'
import DoneBasket                     from './DoneBasket'
import Transfer                       from './transfer'
import Verification                   from './verification'
import Fail                           from './fail'
import {
  addEventBasket, changeSum, clearBasket, closeModal, doneBasket, openModal, redirectModal, removeEvent,
  requestBalance, requestOutComes, signIn
}                                     from '../../actions'
import Success                        from './success'
import OutComes                       from './outcomes'
import Basket                         from './basket'
import AI                             from './ai'
import io                             from 'socket.io-client'

class Models extends Component {

  state = {date: new Date()}

  componentDidMount () {
    let {requestOutComes, redirect, user, clearBasket, doneBasket} = this.props
    requestOutComes()

    this.socket = io()
    this.socket.on('getBasket', data => {
      console.log(data)
    })
    this.socket.on('failBasket', basket => {
      if (user) {
        if (basket.user === user._id)
          redirect('isFail')
        clearBasket()
      }
    })
    this.socket.on('doneBasket', basket => {
      if (basket.user === user._id) {
        doneBasket(basket.basket)
        redirect('isDone')
      }
    })
  }

  addBasket = () => {
    let {basket, redirect, user} = this.props
    this.socket.emit('addBasket', {basket, user: user._id})
    this.setState({date: new Date()})
    redirect('isAI')
  }
  sendBasket = () => {
    let {basket, user} = this.props
    this.socket.emit('sendBets', {basket, user: user._id})
  }

  filterOutComes = () => {
    let {outcomes, type} = this.props
    if (outcomes) {
      return outcomes.filter(o => o.type === type)
    } else {
      return []
    }
  }

  render () {
    let {date} = this.state
    let {isLogin, TYPE, isAI, balance, isFail, isDoneBasket, tournament, removeEvent, isRegistration, isRegistrationEnd, isRecovery, events, changeSum, addEventBasket, isTransfer, isVerification, isSuccess, closeModal, clearBasket, redirect, signIn, user, isOutComes, isDone, basket, isBasket} = this.props
    return <Fragment>
      <Login isShow={isLogin} onClose={closeModal} redirect={redirect} signIn={signIn}/>
      <Registration isShow={isRegistration} onClose={closeModal} redirect={redirect} signIn={signIn}/>
      <Recovery isShow={isRecovery} onClose={closeModal}/>
      <RegistrationEnd isShow={isRegistrationEnd} onClose={closeModal}/>
      <Transfer isShow={isTransfer} onClose={closeModal} user={user}/>
      <Verification isShow={isVerification} user={user} onClose={closeModal}/>
      <Success isShow={isSuccess} onClose={closeModal}/>
      <Done isShow={isDone} onClose={closeModal} redirect={redirect}/>
      <AI isShow={isAI} date={date}/>
      <Fail isShow={isFail} onClose={closeModal}/>
      <OutComes isShow={isOutComes} onClose={closeModal} data={this.filterOutComes()}
                addEvent={outcomes => {
                  addEventBasket({outcomes})
                }}/>

      <Basket isShow={isBasket} onClose={closeModal} removeEvent={removeEvent} user={user} basket={basket}
              events={events} type={TYPE}
              tournament={tournament} changeSum={changeSum} send={this.addBasket} balance={balance}/>
      <DoneBasket isShow={isDoneBasket} onClose={(e) => {
        clearBasket()
        closeModal(e)
      }} user={user} basket={basket}
                  events={events} type={TYPE}
                  tournament={tournament} send={() => {
        this.sendBasket()
        clearBasket()
        closeModal('isDoneBasket')
      }} balance={balance}/>
    </Fragment>
  }
}

const mapStateToProps = state => {
  let {user, balance, type, outcomes, basket, tournament, events, modals: {isLogin, isAI, isRegistration, isRecovery, isRegistrationEnd, isTransfer, isDone, isVerification, isFail, isSuccess, isDoneBasket, isOutComes, isBasket}} = state
  let TYPE = type
  type = type.find(t => t.isActive)
  type = type ? type._id : 0
  return {
    isLogin,
    isRegistration,
    isRecovery,
    isRegistrationEnd,
    isTransfer,
    isVerification,
    events,
    isAI,
    user,
    balance,
    isSuccess,
    isOutComes,
    isBasket,
    isFail,
    isDone,
    outcomes,
    isDoneBasket,
    tournament,
    type, basket, TYPE
  }
}
const mapDispatchToProps = dispatch => {
  return {
    closeModal: () => dispatch(closeModal()),
    redirect: to => dispatch(redirectModal(to)),
    signIn: user => {
      dispatch(signIn(user))
      dispatch(requestBalance(user._id))
    },
    requestOutComes: () => dispatch(requestOutComes()),
    addEventBasket: event => {
      dispatch(addEventBasket(event))
      dispatch(openModal('isBasket'))
    },
    changeSum: e => dispatch(changeSum(e.target.value)),
    removeEvent: key => dispatch(removeEvent(key)),
    clearBasket: () => dispatch(clearBasket()),
    doneBasket: basket => dispatch(doneBasket(basket))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Models)