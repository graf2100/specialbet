import React, { Component } from 'react'
import moment               from 'moment'
import TBet                 from '../../../images/ai/12bet-logo.png'
import OneBet               from '../../../images/ai/1bet.png'
import ElBet                from '../../../images/ai/188BET_logo.png'
import Mension              from '../../../images/ai/mension.png'
import Pinnacle             from '../../../images/ai/pinnacle-logo.png'
import Dafabet              from '../../../images/ai/dafabet_logo.png'
import Sbobet               from '../../../images/ai/sbobet.png'
import bet365               from '../../../images/ai/bet365.png'
import betvictor            from '../../../images/ai/betvictor.png'

class AI extends Component {
  state = {
    index: 0,
    date: moment().format('l')
  }

  componentDidMount () {
    setInterval(() => {
      this.setState({index: Math.round(Math.random() * (8))})
    }, 500)
    this.timerID = setInterval(
      () => this.tick(),
      1000
    )
  }

  componentWillUnmount () {
    clearInterval(this.timerID)
  }

  tick () {
    let {date} = this.props
    let start = new Date().getTime()
    let dateEnd = moment(date).add(5, 'm').toDate().getTime()

    let difference = dateEnd - start
    if (difference <= 0) {
      this.setState({
        date: '00:00'
      })
    } else {
      this.setState({
        date: moment(difference).format('mm:ss')
      })
    }
  }

  animate = flag => flag ? 'img-fluid w-100 animated fast pulse' : 'img-fluid w-100'

  render () {
    let {isShow} = this.props
    let {index, date} = this.state
    return (
      <div
        className={isShow ? 'd-block modal modal-block fade show b-shadow animated fast fadeIn' : 'd-none modal modal-block fade animated fadeOut'}
        role="dialog" tabIndex="-1">
        <div className="modal-dialog modal-block__bet animated fast fadeInDown" role="document">
          <div className="modal-content">
            <div className="modal-header text-center">
              <div className={'w-100'}>
                <h3 className="modal-title w-75" style={{marginLeft: '8rem'}}>
                  Our artificial intelligence will choose for you the optimal ratio</h3>
              </div>
            </div>
            <div className="modal-body text-center">
              <div className={'w-75 mx-auto'}>
                <div className={'row'}>
                  <div className={'col-4'}>
                    <img src={TBet} className={this.animate(index === 0)} alt={'12Bet'}/>
                  </div>
                  <div className={'col-4'}>
                    <img src={betvictor} className={this.animate(index === 1)} alt={'12Bet'}/>
                  </div>
                  <div className={'col-4'}>
                    <img src={bet365} className={this.animate(index === 2)} alt={'12Bet'}/>
                  </div>
                  <div className={'col-4'}>
                    <img src={Sbobet} className={this.animate(index === 3)} alt={'12Bet'}/>
                  </div>
                  <div className={'col-4'}>
                    <img src={Dafabet} className={this.animate(index === 4)} alt={'12Bet'}/>
                  </div>
                  <div className={'col-4'}>
                    <img src={ElBet} className={this.animate(index === 5)} alt={'12Bet'}/>
                  </div>
                  <div className={'col-4'}>
                    <img src={Pinnacle} className={this.animate(index === 6)} alt={'12Bet'}/>
                  </div>
                  <div className={'col-4'}>
                    <img src={Mension} className={this.animate(index === 7)} alt={'12Bet'}/>
                  </div>
                  <div className={'col-4'}>
                    <img src={OneBet} className={this.animate(index === 8)} alt={'12Bet'}/>
                  </div>
                </div>
              </div>
              <h4 className={'text-white font-weight-light mt-3'} style={{fontSize:'2rem'}}>Timer: <span
                style={{color: '#c61d19',fontWeight:'400!important' }}>{date}</span></h4>
            </div>
            <div className="modal-footer"/>
          </div>
        </div>
      </div>)
  }
}

export default AI
