import React, { Component } from 'react'
import close                from '../../../images/close.png'
import axios                from 'axios'

class Login extends Component {

  state = {
    login: '',
    password: '',
    isRemember: true,
    isError: false
  }

  handleChange = (e, k) => {
    if (k === 'isRemember') {
      let {isRemember} = this.state
      this.setState({isRemember: !isRemember})
    } else {
      this.setState({[k]: e.target.value})
    }
  }
  handleClose = e => {
    let {onClose} = this.props
    if (e.target.getAttribute('role')) {
      onClose('isLogin')
    }
  }
  forgotPass = () => {
    if (this.state.login) {
      this.props.redirect('isRecovery')
    }
  }
  submit = async e => {
    e.preventDefault()
    let {login, password, isRemember} = this.state
    let {signIn, onClose} = this.props
    if (login && password && isRemember) {
      let user = await axios.post('/api/auth', {
        email: login, password
      })
      if (user.data.status === 'Done') {
        signIn({token: user.data.token, ...user.data.user})
        localStorage.setItem('user', JSON.stringify({token: user.data.token, ...user.data.user}))
        onClose('isLogin')
      } else {
        this.setState({isError: true})
      }
    }
  }

  render () {
    let {isShow, onClose, redirect} = this.props
    let {login, password, isRemember, isError} = this.state
    return (
      <div
        className={isShow ? 'd-block modal modal-block fade show b-shadow animated fast fadeIn' : 'd-none modal modal-block fade animated fadeOut'}
        role="dialog" tabIndex="-1" onClick={this.handleClose}>
        <div className="modal-dialog modal-block__login animated fast fadeInDown" role="document">
          <div className="modal-content">
            <div className="modal-header text-center">
              <h5 className="modal-title w-100">Login</h5>
              <button className="close" type="button" onClick={() => onClose('isLogin')}>
                <img alt="modal-close" src={close}/>
              </button>
            </div>
            <div className="modal-body">
              {isError && <div className="alert alert-danger" style={{width: '70%', left: '15%'}} role="alert">
                Incorrect email and password!
              </div>}
              <form className="form-block mx-auto" onSubmit={this.submit}>
                <div className="row">
                  <div className="col-12">
                    <div className="form-group">
                      <input className="form-control" placeholder="Email" type="email" value={login}
                             onChange={e => this.handleChange(e, 'login')} required/>
                    </div>
                    <div className="form-group">
                      <input className="form-control" placeholder="Пароль" type="password" value={password}
                             onChange={e => this.handleChange(e, 'password')} required/>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-5">
                    <div className="custom-control custom-checkbox checkbox-block">
                      <input className="custom-control-input" id={'remember'} type="checkbox"/>
                      <label className="custom-control-label" htmlFor="remember" checked={isRemember}>Remember</label>
                    </div>
                  </div>
                  <div className="col-7 text-right">
                    <a className="link" style={{fontSize: 10}} href="javascript:void(0)" onClick={this.forgotPass}>Forgot
                      your password?</a>
                  </div>
                </div>
                <button className="btn button button__red button__md mx-auto d-block" type="submit">Login
                </button>
                <div className="row">
                  <div className="mx-auto">
                    <a className="link" href="javascript:void(0)" onClick={() => redirect('isRegistration')}>No game
                      account yet?</a>
                    <a className="link link__red" href="javascript:void(0)"
                       onClick={() => redirect('isRegistration')}>Registration</a>
                  </div>
                </div>
              </form>
            </div>
            <div className="modal-footer"/>
          </div>
        </div>
      </div>
    )
  }
}

export default Login