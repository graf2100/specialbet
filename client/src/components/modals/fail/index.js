import React, { Component } from 'react'
import close                from '../../../images/close.png'
import cancel               from '../../../images/cancel.svg'

class Fail extends Component {

  handleClose = e => {
    let {onClose} = this.props
    if (e.target.getAttribute('role')) {
      onClose('isFail')
    }
  }

  render () {
    let {isShow, onClose} = this.props
    return (
      <div
        className={isShow ? 'd-block modal modal-block fade show b-shadow animated fast fadeIn' : 'd-none modal modal-block fade animated fadeOut'}
        role="dialog" tabIndex="-1" onClick={this.handleClose}>
        <div className="modal-dialog modal-block__coefficient animated fast fadeInDown" role="document">
          <div className="modal-content">
            <div className="modal-header text-center">
              <img className="success-icon mx-auto" style={{width: 100}} src={cancel} alt="cancel"/>
              <button aria-label="Close" className="close" data-dismiss="modal" type="button"><img alt="modal-close"
                                                                                                   src={close}/>
              </button>
            </div>
            <div className="modal-body">
              <form className="form-block mx-auto">
                <p className="modal-text text-center">Unfortunately, we did not find the optimal ratio for you!</p>
                <button className="btn button button__red button__xs mx-auto d-block"
                        type="button" onClick={() => onClose('isFail')}>OK
                </button>
              </form>
            </div>
            <div className="modal-footer"/>
          </div>
        </div>
      </div>
    )
  }
}

export default Fail