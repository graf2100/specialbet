import React                            from 'react'
import ReactDOM                         from 'react-dom'

import 'bootstrap/dist/css/bootstrap.min.css'
import './index.css'
import './animate.css'
import App                              from './application'
import { Provider }                     from 'react-redux'
import thunkMiddleware                  from 'redux-thunk'
import { applyMiddleware, createStore } from 'redux'
import { createLogger }                 from 'redux-logger'
import reducers                         from './reducers'

const loggerMiddleware = createLogger()

const store = createStore(
  reducers,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware,
  )
)

ReactDOM.render(<Provider store={store}>
  <App/>
</Provider>, document.getElementById('root'))

