import People   from '@material-ui/icons/People'
import Event    from '@material-ui/icons/Event'
import Payment  from '@material-ui/icons/Payment'
import Schedule from '@material-ui/icons/Schedule'
import Money    from '@material-ui/icons/Money'
import Star     from '@material-ui/icons/Star'

import LibraryBooks from '@material-ui/icons/LibraryBooks'
import Accounts     from '../views/Accounts/Accounts.jsx'
import Events       from '../views/Events/Events.jsx'
import Finance      from '../views/Finance/Finance.jsx'
import Request      from '../views/Request/Request.jsx'
import Bets         from '../views/Bets/Bets.jsx'
import Funds        from '../views/funds/Funds.jsx'
import Сashout      from '../views/Сashout/Сashout.jsx'
import Outcomes     from '../views/Outcomes/Outcomes.jsx'

const dashboardRoutes = [
  {
    path: '/accounts',
    sidebarName: 'Счета',
    navbarName: 'Счета',
    icon: People,
    component: Accounts
  },
  {
    path: '/bets',
    sidebarName: 'Ставки',
    navbarName: 'Ставки',
    icon: Schedule,
    component: Bets
  }, {
    path: '/events',
    sidebarName: 'События',
    navbarName: 'События',
    icon: Event,
    component: Events
  }, {
    path: '/outcomes',
    sidebarName: 'Исходы',
    navbarName: 'Исходы',
    icon: Star,
    component: Outcomes
  },
  {
    path: '/request',
    sidebarName: 'Запрос на коэффиц.',
    navbarName: 'Запрос на коэффиц.',
    icon: 'content_paste',
    component: Request
  },
  {
    path: '/сashout',
    sidebarName: 'Запрос на Cashout',
    navbarName: 'Запрос на Cashout',
    icon: LibraryBooks,
    component: Сashout
  },
  {
    path: '/icons',
    sidebarName: 'Финансы',
    navbarName: 'Финансы',
    icon: Payment,
    component: Finance
  }, {
    path: '/funds',
    sidebarName: 'Ввод и Вывод',
    navbarName: 'Ввод и Вывод',
    icon: Money,
    component: Funds
  },
  {redirect: true, path: '/', to: '/accounts', navbarName: 'Redirect'}
]

export default dashboardRoutes
