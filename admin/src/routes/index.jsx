import Dashboard from '../layouts/Dashboard/Dashboard.jsx'
import Login     from '../layouts/Login/index.jsx'

const isAuthenticated = (component, isAuthenticated) => {
  return isAuthenticated ? component : Login
}
const indexRoutes = [{path: '/login', component: Login}, {
  path: '/',
  component: isAuthenticated(Dashboard, true)
}]

export default indexRoutes
