import { get, post } from 'axios'

const key = 'myKeysTop2019'

export const PUSH_USERS = 'PUSH_USERS'
export const REQUEST_USERS = 'REQUEST_USERS'
export const VERIFICATION_USERS = 'VERIFICATION_USERS'
export const REMOVE_USERS = 'REMOVE_USERS'

export const requestUsers = () => async display => {
  display({type: REQUEST_USERS})
  let {data: {status, users}} = await post('/api/get/users', {key})
  if (status === 'Done') {
    console.log(users)
    display({type: PUSH_USERS, users})
  }
}
export const verificationUser = _id => ({type: VERIFICATION_USERS, _id})
export const removeUser = _id => ({type: REMOVE_USERS, _id})

export const GET_TYPE_REQUEST = 'GET_TYPE_REQUEST'
export const GET_TYPE_SUCCESS = 'GET_TYPE_SUCCESS'
export const GET_TYPE_FAILURE = 'GET_TYPE_FAILURE'
export const REMOVE_TYPE = 'REMOVE_TYPE'
export const ADD_TYPE = 'ADD_TYPE'

export const requestType = () => async dispatch => {
  let {data: {data, status}} = await get('/api/type')
  if (status === 'Done') {
    dispatch({type: GET_TYPE_SUCCESS, data: data})
  } else {
    dispatch({type: GET_TYPE_FAILURE})
  }
}
export const removeType = _id => ({type: REMOVE_TYPE, _id})
export const addType = obj => ({type: ADD_TYPE, obj})

export const GET_TOURNAMENT_SUCCESS = 'GET_TOURNAMENT_SUCCESS'
export const GET_TOURNAMENT_FAILURE = 'GET_TOURNAMENT_FAILURE'
export const REMOVE_TOURNAMENT = 'REMOVE_TOURNAMENT'
export const UPDATE_TOURNAMENT = 'UPDATE_TOURNAMENT'
export const ADD_TOURNAMENT = 'ADD_TOURNAMENT'

export const requestTournament = () => async dispatch => {
  let {data: {data, status}} = await get('/api/tournament')
  if (status === 'Done') {
    dispatch({type: GET_TOURNAMENT_SUCCESS, data: data})
  } else {
    dispatch({type: GET_TOURNAMENT_FAILURE})
  }
}
export const removeTournament = _id => ({type: REMOVE_TOURNAMENT, _id})
export const addTournament = obj => ({type: ADD_TOURNAMENT, obj})
export const updateTournament = obj => ({type: UPDATE_TOURNAMENT, obj})

export const GET_EVENTS_SUCCESS = 'GET_EVENTS_SUCCESS'
export const GET_EVENTS_FAILURE = 'GET_EVENTS_FAILURE'
export const REMOVE_EVENTS = 'REMOVE_EVENTS'
export const UPDATE_EVENTS = 'UPDATE_EVENTS'
export const ADD_EVENTS = 'ADD_EVENTS'
export const ACTIVE_EVENTS = 'ACTIVE_EVENTS'

export const requestEvents = () => async dispatch => {
  let {data: {data, status}} = await get('/api/events')
  if (status === 'Done') {
    dispatch({type: GET_EVENTS_SUCCESS, data: data})
  } else {
    dispatch({type: GET_EVENTS_FAILURE})
  }
}
export const removeEvents = _id => ({type: REMOVE_EVENTS, _id})
export const addEvents = obj => ({type: ADD_EVENTS, obj})
export const updateEvents = obj => ({type: UPDATE_EVENTS, obj})
export const activeEvents = _id => ({type: ACTIVE_EVENTS, _id})

export const GET_OUTCOMES_REQUEST = 'GET_OUTCOMES_REQUEST'
export const GET_OUTCOMES_SUCCESS = 'GET_OUTCOMES_SUCCESS'
export const GET_OUTCOMES_FAILURE = 'GET_OUTCOMES_FAILURE'

export const requestOutComes = () => async dispatch => {
  dispatch({type: GET_OUTCOMES_REQUEST})
  let {data: {data, status}} = await get('/api/outcomes')
  if (status === 'Done') {
    dispatch({type: GET_OUTCOMES_SUCCESS, data: data})
  } else {
    dispatch({type: GET_OUTCOMES_FAILURE})
  }
}