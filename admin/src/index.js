import axios                               from 'axios'
import React                               from 'react'
import ReactDOM                            from 'react-dom'
import { createBrowserHistory }            from 'history'
import { Redirect, Route, Router, Switch } from 'react-router-dom'

import './assets/css/material-dashboard-react.css?v=1.5.0'
import { Provider }                        from 'react-redux'
import thunkMiddleware                     from 'redux-thunk'
import { applyMiddleware, createStore }    from 'redux'
import { createLogger }                    from 'redux-logger'
import reducers                            from './reducers'

import Dashboard from './layouts/Dashboard/Dashboard.jsx'
import Login     from './layouts/Login/index'

axios.defaults.baseURL = 'https://specialone.bet';

const hist = createBrowserHistory()

const loggerMiddleware = createLogger()

const store = createStore(
  reducers,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware,
  )
)

ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <Switch>
        <Route path={'/login'} component={Login}/>
        <Route render={props => (
          localStorage.getItem('user')
            ? <Dashboard {...props} />
            : <Redirect to={{pathname: '/login', state: {from: props.location}}}/>
        )}/>
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
)
