import React, { Component } from 'react'
import moment               from 'moment'
import GridItem             from '../../components/Grid/GridItem.jsx'
import GridContainer        from '../../components/Grid/GridContainer.jsx'
import Table                from '../../components/Table/Table.jsx'
import Card                 from '../../components/Card/Card.jsx'
import CardBody             from '../../components/Card/CardBody.jsx'
import Button               from '../../components/CustomButtons/Button.jsx'

import { withStyles } from '@material-ui/core/styles'
import TextField      from '@material-ui/core/TextField'
import Modal          from '@material-ui/core/Modal'
import axios          from 'axios/index'

const styles = theme => {
  return ({
    root: {
      ...theme.mixins.gutters(),
      padding: theme.spacing.unit * 2,

    },
    text: {
      marginTop: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * 2,
      fontSize: '1.25rem'
    },
    button: {
      padding: 0
    },
    paper: {
      position: 'absolute',
      width: theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      margin: '5rem calc(50% - 200px)'
    },
    textField: {
      width: '100%',
    },
    menu: {
      width: '100%',
    },
  })
}

class Сashout extends Component {

  state = {
    bets: [],
    open: false,
    sum: 0,
    id: ''
  }

  async componentDidMount () {
    await this.reqCashout()
  }

  reqCashout = async () => {
    let {data: {status, bets}} = await axios.get('/api/cashout/myKeysTop2019')
    if (status === 'Done') {
      this.setState({bets})
    }
  }

  renderBets () {
    let {bets} = this.state
    let d = []
    bets.forEach(basket => {
      d.push([basket._id, <p
        style={{color: '#f44336'}}>{basket.user}</p>, moment(basket.date).format('L'), `Сумма: ${basket.sum} usd`, '', basket.coefficient, `${basket.result} usd`,
        <Button color="danger" size="sm" onClick={() => this.setState({open: true, sum: 0, id: basket._id})}
                round>Сashout</Button>])
      d.push(...basket.events.map(event => this.renderEvent(event)))
    })
    return d
  }

  renderEvent (event) {
    return ['', '', event.data.calendar + ' ' + event.data.time, event.text, event.outcomes, event.coefficient, event.status]
  }

  submit = async e => {
    let {id, sum} = this.state
    e.preventDefault()
    let {data: {status}} = await axios.post('/api/active/cashout/', {id, sum, key: 'myKeysTop2019'})
    if (status === 'Done') {
      await this.reqCashout()
    }
    this.handleClose()
  }
  handleChange = name => event => this.setState({
    [name]: event.target.value,
  })
  handleClose = () => this.setState({
    open: false,
  })

  render () {
    let {classes} = this.props
    let {open, sum} = this.state
    return (<GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardBody>
            <Table
              tableHeaderColor="danger"
              tableHead={['№ ставки', '№ счета', 'Время запроса', 'Событие', 'Исход', 'Коэф.', 'Результат', '']}
              tableData={this.renderBets()}
            />
          </CardBody>
          <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
                 open={open}
                 onClose={this.handleClose}>
            <div className={classes.paper}>
              Сashout
              <form onSubmit={this.submit}>
                <TextField
                  label="Сумма"
                  value={sum}
                  className={classes.textField}
                  onChange={this.handleChange('sum')}
                  margin="normal"
                  type={'number'}
                />
                <Button style={{marginTop: 16}} color="danger" type={'submit'} round>Сохранить</Button>
              </form>
            </div>
          </Modal>
        </Card>
      </GridItem>
    </GridContainer>)
  }
}

export default withStyles(styles)(Сashout)