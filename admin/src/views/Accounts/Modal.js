import React, { Component, Fragment } from 'react'

import { withStyles } from '@material-ui/core/styles'
import Modal          from '@material-ui/core/Modal'

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: '60%',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
    top: `5ch`,
    left: `20%`,
    overflow: 'scroll',
    height: '80ch'
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
})

class AccountsModal extends Component {
  render () {
    let {classes, close, open, images} = this.props
    return (<Fragment>
      <Modal
        open={open}
        onClose={() => close()}>
        <div className={classes.paper}>
          {images.map(img => (
            <img style={{
              width: '100%',
              height: '100%'
            }} src={img}/>
          ))}
        </div>
      </Modal>
    </Fragment>)
  }
}

export default withStyles(styles)(AccountsModal)