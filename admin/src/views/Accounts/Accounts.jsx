import React, { Component } from 'react'
import Switch               from '@material-ui/core/Switch'
import { connect }          from 'react-redux'
import moment               from 'moment'
import axios                from 'axios'

import GridItem                                       from '../../components/Grid/GridItem.jsx'
import GridContainer                                  from '../../components/Grid/GridContainer.jsx'
import Table                                          from '../../components/Table/Table.jsx'
import Card                                           from '../../components/Card/Card.jsx'
import CardBody                                       from '../../components/Card/CardBody.jsx'
import Button                                         from '../../components/CustomButtons/Button.jsx'
import { removeUser, requestUsers, verificationUser } from '../../actions'
import Modal                                          from './Modal'

class Accounts extends Component {

  state = {id: '', open: false, images: []}

  componentDidMount () {
    this.props.request()
  }

  removeUser = async _id => {
    let {data: {status}} = await axios.post('api/remove/user/', {_id, key: 'myKeysTop2019'})
    if (status === 'Done') {
      this.props.remove(_id)
    }
  }
  verificationUser = async _id => {
    let {data: {status}} = await axios.post('api/verification/user/', {_id, key: 'myKeysTop2019'})
    if (status === 'Done') {
      this.props.verification(_id)
    }
  }

  openModal = id => async () => {
    let {data: {status, imgs}} = await axios.post('/api/find/images', {user: id.toString()})
    if (status === 'Done') {
      if (imgs.length)
        this.setState({id, open: true, images: imgs.map(i => i.url)})
    }
  }

  closeModal = () => this.setState({open: false})

  renderUser ({_id, lastName, firstName, phone, balance, email, birthday, isVerification,isImages}) {
    return [_id, lastName + ' ' + firstName, phone, email, moment(birthday).format('l'), balance + ' р.',
      <Switch color={'default'} size="sm" checked={isVerification} onChange={() => this.verificationUser(_id)}/>,
      <Button color="danger" size="sm" disabled={isImages} round onClick={this.openModal(_id)}>Поподробнее</Button>,
      <Button onClick={() => this.removeUser(_id)} color="danger" size="sm"
              round>X</Button>]
  }

  render () {
    let {users} = this.props
    let {open, images} = this.state
    return (<GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardBody>
              <Table
                tableHeaderColor="danger"
                tableHead={['№ Счета', 'Имя, Фамилия', 'Телефон', 'Email', 'Дата рождения', 'Баланс', 'Верификация', '', '']}
                tableData={users.map(user => this.renderUser(user))}
              />
            </CardBody>
          </Card>
          <Modal open={open} close={this.closeModal} images={images}/>
        </GridItem>
      </GridContainer>
    )
  }
}

const mapStateToProps = state => {
  let {users} = state
  return {users}
}
const mapDispatchToProps = dispatch => {
  return {
    request: () => {
      dispatch(requestUsers())
    },
    remove: _id => {
      dispatch(removeUser(_id))
    },
    verification: _id => {
      dispatch(verificationUser(_id))
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Accounts)
