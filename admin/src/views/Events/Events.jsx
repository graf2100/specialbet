import React, { Component } from 'react'
import { connect }          from 'react-redux'

import GridItem                                          from '../../components/Grid/GridItem.jsx'
import GridContainer                                     from '../../components/Grid/GridContainer.jsx'
import Games                                             from '@material-ui/icons/Games'
import Security                                          from '@material-ui/icons/Security'
import Ballot                                            from '@material-ui/icons/Ballot'
import Scanner                                           from '@material-ui/icons/Scanner'
import Tabs                                              from '../../components/CustomTabs/CustomTabs.jsx'
import { Game, Scraping, Tournament, Type }              from './components'
import { requestEvents, requestTournament, requestType } from '../../actions'

class Events extends Component {
  componentDidMount () {
    this.props.request()
  }

  render () {
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Tabs
            title="Ставки:"
            headerColor="danger"
            tabs={[
              {
                tabName: 'Сканер',
                tabIcon: Scanner,
                tabContent: <Scraping/>
              },
              {
                tabName: 'Игры',
                tabIcon: Games,
                tabContent: <Game/>
              }, {
                tabName: 'Турниры',
                tabIcon: Security,
                tabContent: <Tournament/>
              }, {
                tabName: 'Виды спорта',
                tabIcon: Ballot,
                tabContent: <Type/>
              }
            ]}
          />
        </GridItem>
      </GridContainer>
    )
  }
}

const mapStateToProps = state => {

  return {}
}
const mapDispatchToProps = dispatch => {
  return {
    request: () => {
      dispatch(requestType())
      dispatch(requestTournament())
      dispatch(requestEvents())
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Events)