import React, { Component, Fragment } from 'react'
import { connect }                    from 'react-redux'
import axios                          from 'axios/index'

import Modal           from '@material-ui/core/Modal'
import MenuItem        from '@material-ui/core/MenuItem'
import TextField       from '@material-ui/core/TextField'
import { withStyles }  from '@material-ui/core/styles'
import Grid            from '@material-ui/core/Grid'
import TablePagination from '@material-ui/core/TablePagination'
import Add             from '@material-ui/icons/Add'

import Table                                                 from '../../../components/Table/Table.jsx'
import Button                                                from '../../../components/CustomButtons/Button.jsx'
import { addTournament, removeTournament, updateTournament } from '../../../actions'

import count from '../../../convertcsv'

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    margin: '5rem calc(50% - 200px)'
  },
  textField: {
    width: '100%',
  },
  menu: {
    width: '100%',
  },
})

class Tournament extends Component {
  state = {
    open: false,
    name: '',
    сountry: '',
    type: '',
    edit: {
      open: false,
      _id: '',
      name: '',
      сountry: '',
      type: '',
    }
  }
  removeTournament = async _id => {
    let {data: {status}} = await axios.post('api/remove/tournament/', {_id, key: 'myKeysTop2019'})
    if (status === 'Done') {
      this.props.remove(_id)
    }
  }

  addTournament = async e => {
    e.preventDefault()
    let {name, country, type} = this.state
    if (name && country && type) {
      let {data: {status, obj}} = await axios.post('api/add/tournament/', {name, country, type, key: 'myKeysTop2019'})
      if (status === 'Done') {
        this.setState({name: '', country: '', type: '', open: false})
        this.props.add(obj)
      }
    }
  }
  updateTournament = async e => {
    e.preventDefault()
    let {name, country, type, _id} = this.state.edit
    if (name && country && type && _id) {
      let {data: {status, obj}} = await axios.post('api/update/tournament/', {
        _id,
        name,
        country,
        type,
        key: 'myKeysTop2019'
      })
      if (status === 'Done') {
        this.setState({edit: {name: '', country: '', type: '', open: false}})
        this.props.update(obj)
      }
    }
  }

  handleClose = () => this.setState({open: false})
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  }
  handleEditClose = () => this.setState({
    edit: {
      open: false,
      _id: '',
      name: '',
      сountry: '',
      type: '',
    }
  })

  handleChangeEdit = name => event => {
    let {edit} = this.state
    edit[name] = event.target.value
    this.setState({edit})
  }

  openEdit = _id => {
    let {tournament} = this.props
    let obj = tournament.find(t => t._id === _id)
    if (obj) {
      this.setState({edit: {...obj, type: obj.idType, сountry: obj.сountry, open: true}})
    }
  }

  render () {
    let {tournament, classes, types} = this.props
    let {open, name, country, type, edit} = this.state
    return (<Fragment>
      <Grid container
            spacing={16} direction="row"
            justify="flex-end"
            alignItems="center">
        <Grid item>
          <Button type="button" color="danger" round onClick={() => this.setState({open: true})}>
            <Add/>
            Добавить событие
          </Button>
        </Grid>
      </Grid>
      <Table
        tableHeaderColor="danger"
        tableHead={['№', 'Название', 'Странна', 'Вид спорта', '']}
        tableData={tournament.map(t => [t._id, t.name, t.сountry, t.type, <Fragment>
          <Button color="danger" size="sm" onClick={() => this.openEdit(t._id)} round>Изменить</Button>
          <Button color="danger" style={{marginLeft: '2rem'}} onClick={() => this.removeTournament(t._id)} size="sm"
                  round>X</Button>
        </Fragment>])
        }
      />
      <TablePagination
        rowsPerPageOptions={[25, 50, 100]}
        component="div"
        count={100}
        rowsPerPage={0}
        page={25}
        backIconButtonProps={{
          'aria-label': 'Previous Page',
        }}
        nextIconButtonProps={{
          'aria-label': 'Next Page',
        }}

      />
      <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
             open={open}
             onClose={this.handleClose}>
        <div className={classes.paper}>
          Добавить Турниры:
          <form onSubmit={this.addTournament}>
            <TextField
              label="Имя"
              value={name}
              className={classes.textField}
              onChange={this.handleChange('name')}
              margin="normal"
            />
            <TextField
              select
              label="Вид спорта"
              className={classes.textField}
              value={type}
              onChange={this.handleChange('type')}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              margin="normal"
            >
              {types.map(option => (
                <MenuItem key={option._id} value={option._id}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              select
              label="Странна"
              className={classes.textField}
              value={country}
              onChange={this.handleChange('country')}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              margin="normal"
            >
              {count.map(option => (
                <MenuItem key={option.country_id} value={option.title_en}>
                  {option.title_en}
                </MenuItem>
              ))}
            </TextField>
            <Button style={{marginTop: 16}} color="danger" type={'submit'} round>Сохранить</Button>
          </form>
        </div>
      </Modal>
      <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
             open={edit.open}
             onClose={this.handleEditClose}>
        <div className={classes.paper}>
          Изменить турнир:
          <form onSubmit={this.updateTournament}>
            <TextField
              label="Имя"
              value={edit.name}
              className={classes.textField}
              onChange={this.handleChangeEdit('name')}
              margin="normal"
            />
            <TextField
              select
              label="Вид спорта"
              className={classes.textField}
              value={edit.type}
              onChange={this.handleChangeEdit('type')}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              margin="normal"
            >
              {types.map(option => (
                <MenuItem key={option._id} value={option._id}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              select
              label="Странна"
              className={classes.textField}
              value={edit.country}
              onChange={this.handleChangeEdit('country')}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              margin="normal">
              {count.map(option => (
                <MenuItem key={option.country_id} value={option.title_en}>
                  {option.title_en}
                </MenuItem>
              ))}
            </TextField>
            <Button style={{marginTop: 16}} color="danger" type={'submit'} round>Сохранить</Button>
          </form>
        </div>
      </Modal>
    </Fragment>)
  }
}

const mapStateToProps = state => {
  let {type, tournament} = state
  console.log(type)
  let chekType = t => {
    let f = type.find(_t => _t._id === t.type)
    if (f) {
      return f.name
    } else {
      return ''
    }
  }
  tournament = tournament.map(t => ({...t, type: chekType(t), idType: t.type}))
  return {types: type, tournament}
}
const mapDispatchToProps = dispatch => {
  return {
    remove: _id => {
      dispatch(removeTournament(_id))
    },
    add: obj => {
      dispatch(addTournament(obj))
    },
    update: obj => {
      dispatch(updateTournament(obj))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Tournament))