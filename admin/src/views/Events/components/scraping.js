import React, { Component, Fragment } from 'react'
import io                             from 'socket.io-client'

import Grid             from '@material-ui/core/Grid'
import { withStyles }   from '@material-ui/core/styles'
import FormGroup        from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox         from '@material-ui/core/Checkbox'
import Scanner          from '@material-ui/icons/Scanner'
import Add              from '@material-ui/icons/Add'
import red              from '@material-ui/core/colors/red'
import Button           from '../../../components/CustomButtons/Button.jsx'
import CircularProgress from '@material-ui/core/CircularProgress'

import EnhancedTable from './tables'

class Scraping extends Component {

  state = {
    types: [{label: 'Basketball', value: 'basketbol', isActive: false},
      {label: 'Football', value: 'futbol', isActive: false},
      {label: 'Tennis', value: 'tennis', isActive: false},
      {label: 'Boxing', value: 'boks', isActive: false},
      {label: 'Ice Hockey', value: 'khokkejj', isActive: false},
      {label: 'Futsal', value: 'futzal', isActive: false},

    ],
    disabled: true,
    loading: false,
    tournaments: []
  }

  socket

  componentDidMount () {
    this.socket = io('https://specialone.bet')
    this.socket.on('connect', () => {
      this.setState({disabled: false,})
    })
    this.socket.on('scraping', data => {

      data = data.map(t => {
        if (t.tournament.length) {
          return {
            ...t,
            tournament: t.tournament.map(tour => ({...tour, games: tour.games.map(g => ({...g, isActive: true}))}))
          }
        } else {
          return {...t}
        }
      })

      this.setState({tournaments: data, disabled: false, loading: false})

    })

    this.socket.on('addEvents', data => {
      this.setState({tournaments: [], disabled: false, loading: false})
    })
  }

  scraping = () => {
    let {types} = this.state

    types = types.filter(type => type.isActive)
    if (types) {
      this.socket.emit('scraping', types.map(type => type.value))
      this.setState({disabled: true, loading: true, tournaments: []})
    }
  }
  handleChange = name => event => {
    let {types} = this.state
    let index = types.findIndex(type => type.value === name)
    types[index].isActive = event.target.checked
    this.setState({types})
  }

  handleAdd = () => {
    let {tournaments} = this.state
    tournaments = tournaments.map(t => ({
      ...t,
      tournament: t.tournament.map(tour => ({...tour, games: tour.games.filter(g => g.isActive)}))
    }))
    this.socket.emit('addEvents', tournaments)
  }

  handelActiveGame = data => {
    let {tournaments} = this.state
    tournaments[data.tournamentsIndex].tournament[data.index].games[data.gameIndex].isActive = !tournaments[data.tournamentsIndex].tournament[data.index].games[data.gameIndex].isActive
    this.setState({tournaments})
  }

  render () {
    let {disabled, types, tournaments, loading} = this.state
    let {classes} = this.props
    return (<Fragment>
        <Grid container
              spacing={16} direction="row"
              justify="space-between"
              alignItems="center">
          {types.map(type => (
            <Grid item key={type.value}>
              <FormGroup row>
                <FormControlLabel
                  control={
                    <Checkbox
                      value={type.value}
                      checked={type.isActive}
                      onChange={this.handleChange(type.value)}
                      classes={{
                        root: classes.root,
                        checked: classes.checked,
                      }}
                    />
                  }
                  label={type.label}
                />
              </FormGroup>
            </Grid>
          ))}


          <Grid item>
            <Button type="button" color="danger" round disabled={disabled} onClick={() => this.scraping()}>
              <Scanner/>
              Сканировать
            </Button>
          </Grid>
        </Grid>

        {loading && <CircularProgress className={classes.progress} size={70} thickness={5}/>}
        {tournaments.map((t, tournamentsIndex) => t.tournament.map((tour, index) => tour.games.length ?
          <EnhancedTable games={tour.games}
                         text={tour.description.join('. ')}
                         handelActiveGame={data => this.handelActiveGame({
                           ...data,
                           index, tournamentsIndex
                         })}/> : undefined))}

        {tournaments.length ? (<Grid container
                                     spacing={16} direction="row"
                                     justify="flex-end"
                                     alignItems="center">
          <Grid item>
            <Button type="button" color="danger" round disabled={disabled} onClick={() => this.handleAdd()}>
              <Add/>
              Добавить
            </Button>
          </Grid>
        </Grid>) : undefined}
      </Fragment>
    )
  }
}

export default withStyles({
  root: {
    color: red[500],
    '&$checked': {
      color: red[400],
    },
    checked: {}
  }, progress: {
    margin: '10rem calc(50% - 35px)',
    color: '#e53935',
  },
})(Scraping)