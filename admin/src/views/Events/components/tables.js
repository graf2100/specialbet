import React, { Fragment } from 'react'
import PropTypes           from 'prop-types'
import { withStyles }      from '@material-ui/core/styles'
import Table               from '@material-ui/core/Table'
import TableBody           from '@material-ui/core/TableBody'
import TableCell           from '@material-ui/core/TableCell'
import TableHead           from '@material-ui/core/TableHead'
import TableRow            from '@material-ui/core/TableRow'
import TableSortLabel      from '@material-ui/core/TableSortLabel'
import Toolbar             from '@material-ui/core/Toolbar'
import Typography          from '@material-ui/core/Typography'
import Checkbox            from '@material-ui/core/Checkbox'
import { lighten }         from '@material-ui/core/styles/colorManipulator'

function desc (a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function stableSort (array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index])
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0])
    if (order !== 0) return order
    return a[1] - b[1]
  })
  return stabilizedThis.map(el => el[0])
}

function getSorting (order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy)
}

const rows = [
  {id: 'date', numeric: false, disablePadding: true, label: 'Дата'},
  {id: 'teamOne', numeric: false, disablePadding: true, label: 'Участник 1'},
  {id: 'teamTwo', numeric: false, disablePadding: true, label: 'Участник 2'},
]

class EnhancedTableHead extends React.Component {
  render () {
    const {onSelectAllClick, numSelected, rowCount} = this.props

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {rows.map(row => {
            return (
              <TableCell
                key={row.id}
                align={row.numeric ? 'right' : 'left'}
                padding={row.disablePadding ? 'none' : 'default'}
              >
                <TableSortLabel>
                  {row.label}
                </TableSortLabel>
              </TableCell>
            )
          }, this)}
        </TableRow>
      </TableHead>
    )
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  rowCount: PropTypes.number.isRequired,
}

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
})

let EnhancedTableToolbar = props => {
  const {text, classes} = props
  return (
    <Toolbar className={classes.root}>
      <div className={classes.title}>
        <Typography color={'primary'} variant="h5">
          {text}
        </Typography>
      </div>
    </Toolbar>
  )
}

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  text: PropTypes.string.isRequired,
}

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar)

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
})

class EnhancedTable extends React.Component {
  constructor (props) {
    super(props)

    let games = props.games.map(game => this.createData(game.date, game.teamOne, game.teamTwo))
    this.state = {
      order: 'asc',
      orderBy: 'calories',
      selected: [...games.map(n => n.id)],
      data: [
        ...games
      ],
      page: 0,
      rowsPerPage: 1,
    }
  }

  counter = 0

  createData (date, teamOne, teamTwo) {
    this.counter += 1
    return {id: this.counter, date, teamOne, teamTwo}
  }

  handleSelectAllClick = event => {
    this.state.data.forEach((d, index) => {
      this.props.handelActiveGame({gameIndex: index})
    })
    if (event.target.checked) {
      this.setState(state => ({selected: state.data.map(n => n.id)}))
      return
    }
    this.setState({selected: []})
  }

  handleClick = (event, id) => {
    const {selected} = this.state
    const selectedIndex = selected.indexOf(id)
    let newSelected = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      )
    }

    this.setState({selected: newSelected})

    if (selectedIndex !== -1) {
      this.props.handelActiveGame({gameIndex: selectedIndex})
    } else {
      this.props.handelActiveGame({gameIndex: id - 1})
    }
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1

  render () {
    const {classes, text} = this.props
    const {data, order, orderBy, selected, rowsPerPage, page} = this.state
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage)

    return (
      <Fragment>
        <EnhancedTableToolbar numSelected={selected.length} text={text}/>
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              onSelectAllClick={this.handleSelectAllClick}
              rowCount={data.length}
            />
            <TableBody>
              {stableSort(data, getSorting(order, orderBy)).map(n => {
                const isSelected = this.isSelected(n.id)
                return (
                  <TableRow
                    hover
                    onClick={event => this.handleClick(event, n.id)}
                    role="checkbox"
                    aria-checked={isSelected}
                    tabIndex={-1}
                    key={n.id}
                    selected={isSelected}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox checked={isSelected}/>
                    </TableCell>
                    <TableCell component="th" scope="row" padding="none">
                      {n.date}
                    </TableCell>
                    <TableCell align="right">{n.teamOne}</TableCell>
                    <TableCell align="right">{n.teamTwo}</TableCell>
                  </TableRow>
                )
              })}
              {emptyRows > 0 && (
                <TableRow style={{height: 49 * emptyRows}}>
                  <TableCell colSpan={6}/>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
      </Fragment>

    )
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  handelActiveGame: PropTypes.func.isRequired,
  games: PropTypes.array.isRequired,
  text: PropTypes.string.isRequired,
}

export default withStyles(styles)(EnhancedTable)