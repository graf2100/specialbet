import React, { Component, Fragment } from 'react'
import { connect }                    from 'react-redux'
import axios                          from 'axios'

import Clear                   from '@material-ui/icons/Clear'
import Add                     from '@material-ui/icons/Add'
import ListItem                from '@material-ui/core/ListItem'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText            from '@material-ui/core/ListItemText'
import List                    from '@material-ui/core/List'
import IconButton              from '@material-ui/core/IconButton'
import TextField               from '@material-ui/core/TextField'
import Grid                    from '@material-ui/core/Grid'

import { addType, removeType, requestType } from '../../../actions'

class Type extends Component {

  state = {
    open: false,
    name: '',
  }



  removeType = async _id => {
    let {data: {status}} = await axios.post('api/remove/type/', {_id, key: 'myKeysTop2019'})
    if (status === 'Done') {
      this.props.remove(_id)
    }
  }
  addType = async e => {
    e.preventDefault()
    let {name} = this.state
    if (name) {
      let {data: {status, obj}} = await axios.post('api/add/type/', {name, key: 'myKeysTop2019'})
      if (status === 'Done') {
        this.setState({name: ''})
        this.props.add(obj)
      }
    }
  }

  componentDidMount () {
    this.props.request()
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  }

  render () {
    let {name} = this.state
    let {type} = this.props
    return (<Fragment>
      <Grid container
            spacing={16} direction="row"
            justify="center"
            alignItems="center">
        <Grid item>
          <List dense={false}>
            {type.map(t => <ListItem>
              <ListItemText
                primary={t.name}
              />
              <ListItemSecondaryAction>
                <IconButton aria-label="Delete" onClick={() => this.removeType(t._id)}>
                  <Clear/>
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>)}
          </List>
          <form onSubmit={this.addType}>
            <TextField
              label="Имя"
              value={name}
              onChange={this.handleChange('name')}
              style={{width: 324, marginLeft: 24}}
            />
            <IconButton aria-label="Add" type={'submit'}>
              <Add/>
            </IconButton>
          </form>
        </Grid>
      </Grid>

    </Fragment>)
  }
}

const mapStateToProps = state => {
  let {type, tournament} = state
  return {type, tournament}
}
const mapDispatchToProps = dispatch => {
  return {
    request: () => {
      dispatch(requestType())
    },
    remove: _id => {
      dispatch(removeType(_id))
    },
    add: obj => {
      dispatch(addType(obj))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Type)