import React, { Component, Fragment } from 'react'
import { connect }                    from 'react-redux'
import Switch                         from '@material-ui/core/Switch'
import Grid                           from '@material-ui/core/Grid'
import TextField                      from '@material-ui/core/TextField'
import TablePagination                from '@material-ui/core/TablePagination'
import MenuItem                       from '@material-ui/core/MenuItem'
import Modal                          from '@material-ui/core/Modal'
import { withStyles }                 from '@material-ui/core/styles'
import Add                            from '@material-ui/icons/Add'

import Table                                                   from '../../../components/Table/Table.jsx'
import Button                                                  from '../../../components/CustomButtons/Button.jsx'
import { activeEvents, addEvents, removeEvents, updateEvents } from '../../../actions'
import axios                                                   from 'axios/index'

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    margin: '5rem calc(50% - 200px)'
  },
  textField: {
    width: '100%',
  },
  menu: {
    width: '100%',
  },
})

class Game extends Component {
  state = {
    open: false,
    time: '',
    calendar: '',
    typeActive: '',
    tour: '',
    teamOne: '',
    teamTwo: '',
    edit: {
      open: false,
      _id: '',
      time: '',
      calendar: '',
      tour: '',
      teamOne: '',
      teamTwo: '',
    },
    page: 0,
    rowsPerPage: 25,
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  }
  handleClose = () => this.setState({open: false})
  addTournament = async e => {
    e.preventDefault()
    let {time, calendar, tour, teamTwo, teamOne} = this.state
    if (time && calendar && tour && teamTwo && teamOne) {
      let {data: {status, obj}} = await axios.post('api/add/events/', {
        time,
        calendar,
        tour,
        teamTwo,
        teamOne,
        key: 'myKeysTop2019'
      })
      if (status === 'Done') {
        this.setState({
          _id: '',
          time: '',
          calendar: '',
          tour: '',
          teamOne: '',
          teamTwo: '',
          open: false
        })
        this.props.add(obj)
      }
    }
  }
  removeTournament = async _id => {
    let {data: {status}} = await axios.post('api/remove/events/', {_id, key: 'myKeysTop2019'})
    if (status === 'Done') {
      this.props.remove(_id)
    }
  }
  active = async _id => {
    let {data: {status}} = await axios.post('api/active/events/', {_id, key: 'myKeysTop2019'})
    if (status === 'Done') {
      this.props.active(_id)
    }
  }

  handleEditClose = () => this.setState({
    edit: {
      open: false,
      _id: '',
      time: '',
      calendar: '',
      tour: '',
      teamOne: '',
      teamTwo: '',
    }
  })

  handleChangeEdit = name => event => {
    let {edit} = this.state
    edit[name] = event.target.value
    this.setState({edit})
  }

  openEdit = _id => {
    let {events} = this.props
    let obj = events.find(t => t._id === _id)
    if (obj) {
      this.setState({
        edit: {
          ...obj,
          time: obj.data.time,
          calendar: obj.data.calendar,
          tour: obj.tournament._id,
          open: true
        }
      })
    }
  }

  updateEvent = async e => {
    e.preventDefault()
    let {time, calendar, tour, teamTwo, teamOne, _id} = this.state.edit
    if (time && calendar && tour && teamTwo && teamOne && _id) {
      let {data: {status, obj}} = await axios.post('api/update/events/', {
        _id,
        time, calendar, tour, teamTwo, teamOne,
        key: 'myKeysTop2019'
      })
      if (status === 'Done') {
        this.setState({edit: {name: '', country: '', type: '', open: false}})

        this.props.update(obj)
      }
    }
  }

  handleChangePage = (event, page) => {
    this.setState({page})
  }

  handleChangeRowsPerPage = event => {
    this.setState({rowsPerPage: event.target.value})
  }

  render () {
    let {events, type, tournament, classes} = this.props
    let {open, time, typeActive, calendar, tour, teamOne, teamTwo, edit, rowsPerPage, page} = this.state
    return (<Fragment>
        <Grid container
              spacing={16} direction="row"
              justify="flex-end"
              alignItems="center">

          <Grid item>
            <Button type="button" color="danger" onClick={() => this.setState({open: true})} round>
              <Add/>
              Добавить событие
            </Button>
          </Grid>
        </Grid>
        <Table
          tableHeaderColor="danger"
          tableHead={['Дата и время', 'Турнир', 'Учасник 1', 'Учасник 2', 'Отображение', '', '']}
          tableData={events.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(e =>
            [e.data ? e.data.time + ' ' + e.data.calendar : '', e.tournament ? e.tournament.name : '', e.teamOne, e.teamTwo,
              <Switch checked={e.isActive} onChange={() => this.active(e._id)} color={'default'} size="sm"/>,
              <Button color="danger" size="sm" round onClick={() => this.openEdit(e._id)}>Изменить</Button>,
              <Button color="danger" size="sm" round onClick={() => this.removeTournament(e._id)}>X</Button>])}
        />
        <TablePagination
          rowsPerPageOptions={[25, 50, 100]}
          component="div"
          count={events.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
        <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
               open={open}
               onClose={this.handleClose}>
          <div className={classes.paper}>
            Добавить событие:
            <form onSubmit={this.addTournament}>
              <TextField
                label="Дата"
                value={calendar}
                className={classes.textField}
                onChange={this.handleChange('calendar')}
                margin="normal"
              />
              <TextField
                label="Время"
                value={time}
                className={classes.textField}
                onChange={this.handleChange('time')}
                margin="normal"
              />
              <TextField
                select
                label="Вид спорта"
                className={classes.textField}
                value={typeActive}
                onChange={this.handleChange('typeActive')}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                margin="normal"
              >
                {type.map(option => (
                  <MenuItem key={option._id} value={option.name}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
              <TextField
                select
                label="Турнир"
                className={classes.textField}
                value={tour}
                onChange={this.handleChange('tour')}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                margin="normal"
              >
                {tournament.filter(t => t.name.includes(typeActive)).map(option => (
                  <MenuItem key={option._id} value={option._id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
              <TextField
                label="Учасник 1"
                value={teamOne}
                className={classes.textField}
                onChange={this.handleChange('teamOne')}
                margin="normal"
              /> <TextField
              label="Учасник 2"
              value={teamTwo}
              className={classes.textField}
              onChange={this.handleChange('teamTwo')}
              margin="normal"
            />
              <Button style={{marginTop: 16}} color="danger" type={'submit'} round>Сохранить</Button>
            </form>
          </div>
        </Modal>
        <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
               open={edit.open}
               onClose={this.handleEditClose}>
          <div className={classes.paper}>
            Изменить турнир:
            <form onSubmit={this.updateEvent}>
              <TextField
                label="Дата"
                value={edit.calendar}
                className={classes.textField}
                onChange={this.handleChangeEdit('calendar')}
                margin="normal"
              />
              <TextField
                label="Время"
                value={edit.time}
                className={classes.textField}
                onChange={this.handleChangeEdit('time')}
                margin="normal"
              />
              <TextField
                select
                label="Турнир"
                className={classes.textField}
                value={edit.tour}
                onChange={this.handleChangeEdit('tour')}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                margin="normal"
              >
                {tournament.map(option => (
                  <MenuItem key={option._id} value={option._id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
              <TextField
                label="Учасник 1"
                value={edit.teamOne}
                className={classes.textField}
                onChange={this.handleChangeEdit('teamOne')}
                margin="normal"
              /> <TextField
              label="Учасник 2"
              value={edit.teamTwo}
              className={classes.textField}
              onChange={this.handleChangeEdit('teamTwo')}
              margin="normal"
            />
              <Button style={{marginTop: 16}} color="danger" type={'submit'} round>Сохранить</Button>
            </form>
          </div>
        </Modal>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  let {type, tournament, events} = state
  events = events.map(e => ({...e, tournament: tournament.find(t => t._id === parseInt(e.tournament))})).reverse()
  return {type, tournament, events}
}
const mapDispatchToProps = dispatch => {
  return {
    remove: _id => {
      dispatch(removeEvents(_id))
    },
    add: obj => {
      dispatch(addEvents(obj))
    },
    update: obj => {
      dispatch(updateEvents(obj))
    },
    active: _id => {
      dispatch(activeEvents(_id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Game))