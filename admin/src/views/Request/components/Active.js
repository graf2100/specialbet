import React, { Component, Fragment } from 'react'
import io                             from 'socket.io-client'
import Table                          from '../../../components/Table/Table.jsx'
import Button                         from '../../../components/CustomButtons/Button.jsx'
import * as moment                    from 'moment'
import 'moment/locale/ru'

const styleInput = {
  boxShadow: '0 2px 2px 0 rgba(244, 67, 54, 0.14), 0 3px 1px -2px rgba(244, 67, 54, 0.2), 0 1px 5px 0 rgba(244, 67, 54, 0.12)',
  color: '#f44336',
  borderRadius: 30,
  padding: '0.40625rem 1.25rem',
  fontSize: '0.6875rem',
  lineHeight: 1.5,
  border: '1px solid #f44336a3',
  width: 40
}

class Active extends Component {

  state = {
    baskets:[]
  }

  constructor (props) {
    super(props)

    moment.locale('ru')
  }

  componentDidMount () {
    this.socket = io('https://specialone.bet')
    this.socket.on('connect', () => {
      this.socket.emit('getBasket', 1)
    })
    this.socket.on('getBasket', baskets => {
      console.log(baskets)
      this.setState({baskets})
    })
  }

  changeCoefficient = (key, index) => e => {
    this.socket.emit('setCoefficient', {key, index, val: e.target.value})
  }

  sendBasket = key => () => {
    this.socket.emit('sendBasket', key)
  }

  renderBaskets () {
    let {baskets} = this.state
    let data = []
    baskets.forEach((basket, key) => {
      data.push([<p
        style={{color: '#f44336'}}>{basket.user}</p>, `Сумма: ${basket.basket.sum} usd`, '',
        <p style={{color: '#f44336'}}>{basket.coefficient}</p>,
        <Button color="danger" size="sm" onClick={this.sendBasket(key)} round
                disabled={!basket.coefficient > 0}>Применить</Button>,
        <p
          style={{color: '#f44336'}}>{moment(basket.timeEnd).fromNow()}</p>])
      data.push(...basket.basket.events.map((event, index) => this.renderEvent(event.text, event.outcomes, event.coefficient, [key, index])))
    })
    return data
  }

  renderEvent = (text, outcomes, coefficient, set) => ['', text, outcomes,
    <input value={coefficient} style={styleInput}
           type={'number'} step="0.1" onChange={this.changeCoefficient(...set)}/>, '', '']

  render () {
    return (
      <Fragment>
        <Table
          tableHeaderColor="danger"
          tableHead={['№ счета', 'Событие', 'Исход', 'Коэф.', '', '']}
          tableData={this.renderBaskets()}
        />
      </Fragment>)
  }
}

export default Active