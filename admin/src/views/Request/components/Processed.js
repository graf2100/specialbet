import React, { Component, Fragment } from 'react'
import TablePagination                from '@material-ui/core/TablePagination'
import Table                          from '../../../components/Table/Table.jsx'
import Button                         from '../../../components/CustomButtons/Button.jsx'

const styleInput = {
  boxShadow: '0 2px 2px 0 rgba(244, 67, 54, 0.14), 0 3px 1px -2px rgba(244, 67, 54, 0.2), 0 1px 5px 0 rgba(244, 67, 54, 0.12)',
  color: '#f44336',
  borderRadius: 30,
  padding: '0.40625rem 1.25rem',
  fontSize: '0.6875rem',
  lineHeight: 1.5,
  border: '1px solid #f44336a3',
  width: 40
}

class Processed extends Component {
  render () {
    return (
      <Fragment>
        <Table
          tableHeaderColor="danger"
          tableHead={['№ счета', 'Событие', 'Исход', 'Коэф.']}
          tableData={[
            [<p style={{color: '#f44336'}}>3310290</p>, 'Сумма: 20,00 usd', '', <p style={{color: '#f44336'}}>14.6</p>],
            ['', 'Футбол. Лига Европы УЕФА. Ариенал Л. - Милан', 'П1', <p style={{color: '#f44336'}}>2.6</p>],
            ['', 'Футбол. Лига Европы УЕФА. Ариенал Л. - Милан', 'П1', <p style={{color: '#f44336'}}>2.6</p>],
            ['', 'Футбол. Лига Европы УЕФА. Ариенал Л. - Милан', 'П1', <p style={{color: '#f44336'}}>2.6</p>],    [<p style={{color: '#f44336'}}>3310290</p>, 'Сумма: 20,00 usd', '', <p style={{color: '#f44336'}}>14.6</p>],
            ['', 'Футбол. Лига Европы УЕФА. Ариенал Л. - Милан', 'П1', <p style={{color: '#f44336'}}>2.6</p>],
            ['', 'Футбол. Лига Европы УЕФА. Ариенал Л. - Милан', 'П1', <p style={{color: '#f44336'}}>2.6</p>],
            ['', 'Футбол. Лига Европы УЕФА. Ариенал Л. - Милан', 'П1', <p style={{color: '#f44336'}}>2.6</p>],    [<p style={{color: '#f44336'}}>3310290</p>, 'Сумма: 20,00 usd', '', <p style={{color: '#f44336'}}>14.6</p>],
            ['', 'Футбол. Лига Европы УЕФА. Ариенал Л. - Милан', 'П1', <p style={{color: '#f44336'}}>2.6</p>],
            ['', 'Футбол. Лига Европы УЕФА. Ариенал Л. - Милан', 'П1', <p style={{color: '#f44336'}}>2.6</p>],
            ['', 'Футбол. Лига Европы УЕФА. Ариенал Л. - Милан', 'П1', <p style={{color: '#f44336'}}>2.6</p>],


          ]}
        />
        <TablePagination
          rowsPerPageOptions={[25, 50, 100]}
          component="div"
          count={100}
          rowsPerPage={0}
          page={25}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}

        />
      </Fragment>)
  }
}

export default Processed