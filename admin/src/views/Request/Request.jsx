import React         from 'react'
import CompareArrows from '@material-ui/icons/CompareArrows'
import Done          from '@material-ui/icons/Done'

import GridItem              from '../../components/Grid/GridItem.jsx'
import GridContainer         from '../../components/Grid/GridContainer.jsx'
import Tabs                  from '../../components/CustomTabs/CustomTabs.jsx'
import { Active, Processed } from './components'


function Request () {

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Tabs
          title="Запрос на коэффициенты:"
          headerColor="danger"
          tabs={[
            {
              tabName: 'Активные',
              tabIcon: CompareArrows,
              tabContent: (<Active/>)
            },
         /*   {
              tabName: 'Обработанные',
              tabIcon: Done,
              tabContent: (<Processed/>)
            }*/
          ]}
        />
      </GridItem>
    </GridContainer>
  )
}

export default Request
