import React, { Component } from 'react'
import { post }             from 'axios'
import Grid                 from '@material-ui/core/Grid'
import TextField            from '@material-ui/core/TextField'
import { withStyles }       from '@material-ui/core/styles'
import Modal                from '@material-ui/core/Modal'
import MenuItem             from '@material-ui/core/MenuItem'
import Add                  from '@material-ui/icons/Add'
import moment               from 'moment'
import Delete               from '@material-ui/icons/Delete'

import GridItem      from '../../components/Grid/GridItem.jsx'
import GridContainer from '../../components/Grid/GridContainer.jsx'
import Table         from '../../components/Table/Table.jsx'
import Card          from '../../components/Card/Card.jsx'
import CardBody      from '../../components/Card/CardBody.jsx'
import Button        from '../../components/CustomButtons/Button.jsx'

const styles = theme => {
  return ({
    root: {
      ...theme.mixins.gutters(),
      padding: theme.spacing.unit * 2,

    },
    text: {
      marginTop: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * 2,
      fontSize: '1.25rem'
    },
    button: {
      padding: 0
    },
    paper: {
      position: 'absolute',
      width: theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      margin: '5rem calc(50% - 200px)'
    },
    textField: {
      width: '100%',
    },
    menu: {
      width: '100%',
    },
  })
}

class Finance extends Component {
  state = {
    finance: [],
    pay: 0,
    replenishment: 0,
    open: false,
    type: '',
    id: '',
    sum: 0,
    note: '',
    payment: ''
  }

  async componentDidMount () {
    await this.getFinance()
    await this.getPayFinance()
    await this.getReplenishmentFinance()
  }

  getFinance = async () => {
    let {data: {finance, status}} = await post('/api/get/finance/', {key: 'myKeysTop2019'})
    if (status === 'Done') {
      this.setState({finance})
    } else {
      console.log('Error')
    }
  }

  getPayFinance = async () => {
    let {data: {pay, status}} = await post('/api/pay/finance/', {key: 'myKeysTop2019'})
    if (status === 'Done') {
      this.setState({pay: new Intl.NumberFormat('ru-RU').format(pay)})
    } else {
      console.log('Error')
    }
  }

  getReplenishmentFinance = async () => {
    let {data: {replenishment, status}} = await post('/api/replenishment/finance/', {key: 'myKeysTop2019'})
    if (status === 'Done') {
      this.setState({replenishment: new Intl.NumberFormat('ru-RU').format(replenishment)})
    } else {
      console.log('Error')
    }
  }

  addFinance = async () => {
    let {type, id, sum, note, payment} = this.state
    let {data: {finance, status}} = await post('/api/add/finance/', {
      key: 'myKeysTop2019', type, id, sum: parseInt(sum), note, payment
    })
    if (status === 'Done') {
      this.setState({finance})
    } else {
      console.log('Error')
    }
  }
  openModal = () => {
    this.setState({
      open: true, type: '',
      id: '',
      sum: 0,
      note: '',
      payment: ''
    })
  }
  handleClose = () => this.setState({
    open: false,
  })
  handleChange = name => event => this.setState({
    [name]: event.target.value,
  })
  submit = async e => {
    e.preventDefault()
    this.setState({open: false})
    await this.addFinance()
    await this.getPayFinance()
    await this.getReplenishmentFinance()
  }

  remove = _id => async () => {
    let {data: {status}} = await post('/api/remove/finance/', {
      key: 'myKeysTop2019', _id
    })
    if (status === 'Done') {
      await  this.getFinance()
    } else {
      console.log('Error')
    }
  }

  render () {
    let {classes} = this.props
    let {finance, pay, replenishment, type, id, sum, note, payment, open} = this.state
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardBody>
              <Grid container
                    spacing={16} direction="row"
                    justify="space-between"
                    alignItems="center">
                <Grid item>
                  <h4>Выплачено: <small style={{color: '#f44336'}}>{pay} р.</small> Пополнения: <small
                    style={{color: '#f44336'}}>{replenishment} р.</small></h4>
                </Grid>
                <Grid item>
                  <Button type="button" color="danger" onClick={this.openModal} round>
                    <Add/>
                    Добавить транзакцию
                  </Button>
                </Grid>
              </Grid>
              <Table
                tableHeaderColor="danger"
                tableHead={['Дата и время', 'Вид операции', 'Платежная система', '№ счета', 'Сумма', 'Примечание', '']}
                tableData={finance.map(f => [moment(f.date).format('L'), f.type, f.payment, f.id, f.sum, f.note,
                  <Delete onClick={this.remove(f._id)}/>])}
              />
              <br/>
              <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
                     open={open}
                     onClose={this.handleClose}>
                <div className={classes.paper}>
                  Транзакция
                  <form onSubmit={this.submit}>
                    <TextField
                      select
                      label="Вид операции"
                      className={classes.textField}
                      value={type}
                      onChange={this.handleChange('type')}
                      SelectProps={{
                        MenuProps: {
                          className: classes.menu,
                        },
                      }}
                      margin="normal"
                    >
                      {['Withdraw', 'Deposit'].map(t => (
                        <MenuItem key={t} value={t}>
                          {t}
                        </MenuItem>
                      ))}
                    </TextField>
                    <TextField
                      select
                      label="Платежная система"
                      className={classes.textField}
                      value={payment}
                      onChange={this.handleChange('payment')}
                      SelectProps={{
                        MenuProps: {
                          className: classes.menu,
                        },
                      }}
                      margin="normal"
                    >
                      {['EXMO', 'QIWI', 'WebMoney', 'Wire Transfer', 'BitCoin', 'Visa & Mastercard',].map(t => (
                        <MenuItem key={t} value={t}>
                          {t}
                        </MenuItem>
                      ))}
                    </TextField>
                    <TextField
                      label="№ счета"
                      value={id}
                      className={classes.textField}
                      onChange={this.handleChange('id')}
                      margin="normal"
                    />
                    <TextField
                      label="Сумма"
                      value={sum}
                      className={classes.textField}
                      type={'number'}
                      onChange={this.handleChange('sum')}
                      margin="normal"
                    />
                    <TextField
                      label="Примечание"
                      value={note}
                      className={classes.textField}
                      onChange={this.handleChange('note')}
                      margin="normal"
                    />
                    <Button style={{marginTop: 16}} color="danger" type={'submit'} round>Добавить</Button>
                  </form>
                </div>
              </Modal>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    )
  }
}

export default withStyles(styles)(Finance)