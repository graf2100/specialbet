import React, { Component } from 'react'
import { get, post }        from 'axios'
import { withStyles }       from '@material-ui/core/styles'
import moment               from 'moment'
import Delete               from '@material-ui/icons/Delete'

import GridItem      from '../../components/Grid/GridItem.jsx'
import GridContainer from '../../components/Grid/GridContainer.jsx'
import Table         from '../../components/Table/Table.jsx'
import Card          from '../../components/Card/Card.jsx'
import CardBody      from '../../components/Card/CardBody.jsx'

const styles = theme => {
  return ({
    root: {
      ...theme.mixins.gutters(),
      padding: theme.spacing.unit * 2,

    },
    text: {
      marginTop: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * 2,
      fontSize: '1.25rem'
    },
    button: {
      padding: 0
    },
    paper: {
      position: 'absolute',
      width: theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      margin: '5rem calc(50% - 200px)'
    },
    textField: {
      width: '100%',
    },
    menu: {
      width: '100%',
    },
  })
}

class Funds extends Component {
  state = {
    transaction: []
  }

  async componentDidMount () {
    await this.getTransaction()
  }

  getTransaction = async () => {
    let {data: {transaction, status}} = await get('/api/transaction/')
    if (status === 'Done') {
      this.setState({transaction})
    } else {
      console.log('Error')
    }
  }

  remove = _id => async () => {
    let {data: {status}} = await post('/api/remove/transaction/', {
      key: 'myKeysTop2019', _id
    })
    if (status === 'Done') {
      await  this.getTransaction()
    } else {
      console.log('Error')
    }
  }

  render () {
    let {transaction} = this.state
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardBody>
              <Table
                tableHeaderColor="danger"
                tableHead={['№', 'Номер игрока', 'Описание', 'Дата', '']}
                tableData={transaction.map(f => [f._id, f.user, f.text, moment(f.date).format('L'),
                  <Delete onClick={this.remove(f._id)}/>])}
              />
              <br/>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    )
  }
}

export default withStyles(styles)(Funds)