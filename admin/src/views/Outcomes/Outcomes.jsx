import React, { Component, Fragment } from 'react'
import Paper                          from '@material-ui/core/Paper'
import { withStyles }                 from '@material-ui/core/styles'
import Typography                     from '@material-ui/core/Typography'
import Grid                           from '@material-ui/core/Grid'
import GridItem                       from '../../components/Grid/GridItem.jsx'
import GridContainer                  from '../../components/Grid/GridContainer.jsx'
import Button                         from '../../components/CustomButtons/Button.jsx'
import { connect }                    from 'react-redux'
import Add                            from '@material-ui/icons/Add'
import Delete                         from '@material-ui/icons/Delete'
import { requestOutComes, }           from '../../actions'
import TextField                      from '@material-ui/core/TextField'
import Modal                          from '@material-ui/core/Modal'
import axios                          from 'axios/index'
import MenuItem                       from '@material-ui/core/MenuItem'

const styles = theme => {
  return ({
    root: {
      ...theme.mixins.gutters(),
      padding: theme.spacing.unit * 2,

    },
    text: {
      marginTop: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * 2,
      fontSize: '1.25rem'
    },
    button: {
      padding: 0
    },
    paper: {
      position: 'absolute',
      width: theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      margin: '5rem calc(50% - 200px)'
    },
    textField: {
      width: '100%',
    },
    menu: {
      width: '100%',
    },
  })
}

class Outcomes extends Component {
  state = {open: false, typeId: '', valCategory: '', openItem: false, category: '', text: '', valItem: ''}

  removeIndex = (_id, index) => async () => {
    let {data: {status}} = await axios.post('api/remove/ItemOutcomes/', {
      index, _id,
      key: 'myKeysTop2019'
    })
    if (status === 'Done') {
      this.props.request()
      this.handleClose()
    }
  }
  remove = _id => async () => {
    let {data: {status}} = await axios.post('api/remove/Outcomes/', {
      _id,
      key: 'myKeysTop2019'
    })
    if (status === 'Done') {
      this.props.request()
      this.handleClose()
    }
  }
  openAddItem = (category, text) => () => this.setState({openItem: true, category, text})

  openModal = () => this.setState({open: true})

  handleChange = name => event => this.setState({
    [name]: event.target.value,
  })

  submitItem = async e => {
    e.preventDefault()
    let {category, valItem} = this.state
    let {data: {status}} = await axios.post('api/add/ItemOutcomes/', {
      category, valItem,
      key: 'myKeysTop2019'
    })
    if (status === 'Done') {
      this.props.request()
      this.handleClose()
    }

  }
  submit = async e => {
    e.preventDefault()
    let {typeId, valCategory} = this.state
    let {data: {status}} = await axios.post('api/add/Outcomes/', {
      typeId, valCategory,
      key: 'myKeysTop2019'
    })
    if (status === 'Done') {
      this.props.request()
      this.handleClose()
    }

  }

  handleClose = () => this.setState({
    openItem: false,
    category: '',
    valItem: '',
    text: '',
    open: false,
    typeId: '',
    valCategory: '',
  })

  render () {
    const {openItem, valItem, text, valCategory, open, typeId} = this.state
    const {classes, outcomes, type} = this.props
    return (<GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Paper className={classes.root}>
          <Button type="button" color="danger" onClick={this.openModal}>Добавить вид исхода</Button>
          {type.map((t, index) => outcomes.filter(outcome => outcome.type === t._id).map(outcome =>
            <Fragment key={index + 'out'}>
              <Grid container
                    direction="row"
                    justify="space-between"
                    alignItems="center">
                <Grid item xs={4}>
                  <Typography
                    className={classes.text}>{t.name} - {outcome.label}:</Typography>
                </Grid>
                <Grid item xs={1}>
                  <Delete onClick={this.remove(outcome._id)}/>
                </Grid>
              </Grid>
              <Grid container spacing={8}>
                {outcome.values.map((val, index) => <Grid key={'item' + index} item xs={1}
                                                          onClick={this.removeIndex(outcome._id, index)}>
                  {val}
                </Grid>)}
                <Grid item xs={1} onClick={this.openAddItem(outcome._id, t.name + ' - ' + outcome.label + ':')}>
                  <Add/>
                </Grid>
              </Grid>
            </Fragment>)
          )}

          <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
                 open={openItem}
                 onClose={this.handleClose}>
            <div className={classes.paper}>
              {text}
              <form onSubmit={this.submitItem}>
                <TextField
                  label="Название"
                  value={valItem}
                  className={classes.textField}
                  onChange={this.handleChange('valItem')}
                  margin="normal"
                />
                <Button style={{marginTop: 16}} color="danger" type={'submit'} round>Сохранить</Button>
              </form>
            </div>
          </Modal>

          <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
                 open={open}
                 onClose={this.handleClose}>
            <div className={classes.paper}>
              Вид исхода
              <form onSubmit={this.submit}>
                <TextField
                  select
                  label="Вид спорта"
                  className={classes.textField}
                  value={typeId}
                  onChange={this.handleChange('typeId')}
                  SelectProps={{
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}
                  margin="normal"
                >
                  {type.map(t => (
                    <MenuItem key={t._id} value={t._id}>
                      {t.name}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  label="Название"
                  value={valCategory}
                  className={classes.textField}
                  onChange={this.handleChange('valCategory')}
                  margin="normal"
                />
                <Button style={{marginTop: 16}} color="danger" type={'submit'} round>Сохранить</Button>
              </form>
            </div>
          </Modal>
        </Paper>
      </GridItem>
    </GridContainer>)
  }
}

const mapStateToProps = state => {
  let {outcomes, type} = state
  return {outcomes, type}
}
const mapDispatchToProps = dispatch => {
  return {
    request: () => {
      dispatch(requestOutComes())
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Outcomes))
