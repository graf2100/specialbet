import React                    from 'react'
import PlayCircleFilledOutlined from '@material-ui/icons/PlayCircleFilledOutlined'
import Pause                    from '@material-ui/icons/Pause'

import GridItem         from '../../components/Grid/GridItem.jsx'
import GridContainer    from '../../components/Grid/GridContainer.jsx'
import Tabs             from '../../components/CustomTabs/CustomTabs.jsx'
import { Play, Played } from './components'

function Bets () {
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Tabs
          title="Ставки:"
          headerColor="danger"
          tabs={[
            {
              tabName: 'Играют',
              tabIcon: PlayCircleFilledOutlined,
              tabContent: (<Play/>)
            },
            {
              tabName: 'Сыграли',
              tabIcon: Pause,
              tabContent: (<Played/>)
            }
          ]}
        />
      </GridItem>
    </GridContainer>
  )
}

export default Bets
