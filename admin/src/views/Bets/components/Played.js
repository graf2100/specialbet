import React, { Component, Fragment } from 'react'
import axios                          from 'axios'

import * as moment from 'moment'

import Table from '../../../components/Table/Table.jsx'

class Played extends Component {

  state = {
    bets: []
  }

  async componentDidMount () {
    let {data: {status, bets}} = await axios.get('/api/bets/myKeysTop2019')
    if (status === 'Done') {
      this.setState({bets})
    }
  }

  renderBets () {
    let {bets} = this.state
    let d = []
    bets.forEach(basket => {
      d.push([basket._id, <p
        style={{color: '#f44336'}}>{basket.user}</p>, moment(basket.date).format('L'), `Сумма: ${basket.sum} usd`, '', basket.coefficient, `${basket.result} usd`,])
      d.push(...basket.events.map(event => this.renderEvent(event)))
    })
    return d
  }

  renderEvent (event) {
    return ['', '', event.data.calendar + ' ' + event.data.time, event.text, event.outcomes, event.coefficient, event.status]
  }

  render () {
    return (
      <Fragment>
        <Table
          tableHeaderColor="danger"
          tableHead={['№ ставки', '№ счета', 'Дата и время', 'Событие и исход', 'Исход', 'Коэф.', 'Результат']}
          tableData={this.renderBets()}
        />
      </Fragment>)
  }
}

export default Played