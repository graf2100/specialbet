import React, { Component, Fragment } from 'react'
import io                             from 'socket.io-client'

import Select   from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'

import Button from '../../../components/CustomButtons/Button.jsx'

import * as moment from 'moment'

import Table from '../../../components/Table/Table.jsx'

class Play extends Component {

  state = {
    bets: []
  }

  componentDidMount () {
    this.socket = io('https://specialone.bet')
    this.socket.on('connect', () => {
      this.socket.emit('getBets', 1)
    })
    this.socket.on('getBets', bets => {
      this.setState({bets})
    })
  }

  renderBets () {
    let {bets} = this.state
    let data = []
    bets.forEach((basket, key) => {
      if (basket.user) {
        data.push([basket._id, <p
          style={{color: '#f44336'}}>{basket.user}</p>, moment(basket.date).format('L'), `Сумма: ${basket.sum} usd`, '', basket.coefficient, `${basket.result} usd`,])
        data.push(...basket.events.map((event, index) => this.renderEvent(event, index, basket._id)))
        data.push(['', '', '', '', '', '',
          <Button type="button" color="danger" size={'sm'} onClick={this.doneBet(basket._id)}>
            Сохранить
          </Button>])
      }
    })
    return data
  }

  mySelect = (status, index, id) => (
    <Select onChange={e => this.changeStatus(e, index, id)} displayEmpty value={status}>
      <MenuItem value={''}>Нет результата</MenuItem>
      <MenuItem value={'Return'}>Return</MenuItem>
      <MenuItem value={'Played'}>Played</MenuItem>
      <MenuItem value={'Lose'}>Lose</MenuItem>
    </Select>)

  doneBet = (_id) => () => {
    this.socket.emit('doneBet', _id)
  }
  changeStatus = (e, index, id) => {
    this.socket.emit('setStatusBet', {index, id, val: e.target.value})
  }

  renderEvent (event, index, id) {
    return ['', '', event.data.calendar + ' ' + event.data.time, event.text, event.outcomes, event.coefficient, this.mySelect(event.status, index, id)]
  }

  render () {
    return (
      <Fragment>
        <Table
          tableHeaderColor="danger"
          tableHead={['№ ставки', '№ счета', 'Дата и время', 'Событие и исход', 'Исход', 'Коэф.', 'Результат']}
          tableData={this.renderBets()}
        />
        <br/>
      </Fragment>)
  }
}

export default Play