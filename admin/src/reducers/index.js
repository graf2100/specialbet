import { combineReducers }                                                  from 'redux'
import {
  ACTIVE_EVENTS, ADD_EVENTS, ADD_TOURNAMENT, ADD_TYPE, GET_EVENTS_FAILURE, GET_EVENTS_SUCCESS,
  GET_TOURNAMENT_FAILURE, GET_TOURNAMENT_SUCCESS, GET_TYPE_FAILURE, GET_TYPE_REQUEST, GET_TYPE_SUCCESS, PUSH_USERS,
  REMOVE_EVENTS, REMOVE_TOURNAMENT, REMOVE_TYPE, REMOVE_USERS, REQUEST_USERS, UPDATE_EVENTS, UPDATE_TOURNAMENT,
  VERIFICATION_USERS
}                                                                           from '../actions'
import { GET_OUTCOMES_FAILURE, GET_OUTCOMES_REQUEST, GET_OUTCOMES_SUCCESS } from '../actions'

function users (state = [], action) {
  switch (action.type) {
    case REQUEST_USERS:
      return []
    case PUSH_USERS:
      return [...action.users]
    case REMOVE_USERS: {
      let i = state.findIndex(s => s._id === action._id)
      state.splice(i, 1)
      state = JSON.parse(JSON.stringify(state))
      return [...state]
    }
    case VERIFICATION_USERS: {
      let i = state.findIndex(s => s._id === action._id)
      state[i].isVerification = !state[i].isVerification
      state = JSON.parse(JSON.stringify(state))
      return [...state]
    }
    default:
      return state
  }
}

function type (state = [{id: 1, name: 'Футбол'},
  {id: 2, name: 'Теннис'},
  {id: 3, name: 'Хоккей'},
  {id: 4, name: 'Баскетбол'}], action) {
  switch (action.type) {
    case GET_TYPE_SUCCESS:
      return [...action.data]
    case GET_TYPE_REQUEST:
      return []
    case GET_TYPE_FAILURE:
      return []
    case REMOVE_TYPE:
      let i = state.findIndex(s => s._id === action._id)
      state.splice(i, 1)
      state = JSON.parse(JSON.stringify(state))
      return [...state]
    case  ADD_TYPE:
      return [...state, action.obj]
    default:
      return state
  }
}

function tournament (state = [{
  id: '123',
  name: 'Первый турнир',
  сountry: 'Украина',
  type: '5c138272fb928a0513d38529'
}], action) {
  switch (action.type) {
    case GET_TOURNAMENT_SUCCESS:
      return [...action.data]
    case GET_TOURNAMENT_FAILURE:
      return []
    case REMOVE_TOURNAMENT: {
      let i = state.findIndex(s => s._id === action._id)
      state.splice(i, 1)
      state = JSON.parse(JSON.stringify(state))
      return [...state]
    }
    case UPDATE_TOURNAMENT:
      let i = state.findIndex(s => s._id === action.obj._id)
      state.splice(i, 1, action.obj)
      state = JSON.parse(JSON.stringify(state))
      return [...state]
    case  ADD_TOURNAMENT:
      return [...state, action.obj]
    default:
      return state
  }
}

function events (state = [{
  _id: '123',
  data: {time: '13:00', calendar: '24.04.2018'},
  tournament: '123',
  status: 'Идет игра',
  teamOne: 'Команда А',
  teamTwo: 'Команда Б',
  isActive: false,
  score: '-',
}], action) {
  switch (action.type) {
    case GET_EVENTS_SUCCESS:
      return [...action.data]
    case GET_EVENTS_FAILURE:
      return []
    case REMOVE_EVENTS: {
      let i = state.findIndex(s => s._id === action._id)
      state.splice(i, 1)
      state = JSON.parse(JSON.stringify(state))
      return [...state]
    }
    case UPDATE_EVENTS:
      let i = state.findIndex(s => s._id === action.obj._id)
      state.splice(i, 1, action.obj)
      state = JSON.parse(JSON.stringify(state))
      return [...state]
    case  ADD_EVENTS:
      return [...state, action.obj]
    case ACTIVE_EVENTS: {
      let i = state.findIndex(s => s._id === action._id)
      state[i].isActive = !state[i].isActive
      state = JSON.parse(JSON.stringify(state))
      return [...state]
    }
    default:
      return state
  }
}
function outcomes (state = [], action) {
  switch (action.type) {
    case GET_OUTCOMES_FAILURE:
      return []
    case GET_OUTCOMES_REQUEST:
      return []
    case GET_OUTCOMES_SUCCESS:
      return [...action.data]
    default:
      return state
  }
}


const reducer = combineReducers({users, type, events, tournament,outcomes})

export default reducer