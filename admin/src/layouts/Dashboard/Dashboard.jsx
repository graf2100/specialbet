/* eslint-disable */
import React                       from 'react'
import PropTypes                   from 'prop-types'
import { Redirect, Route, Switch } from 'react-router-dom'
import PerfectScrollbar            from 'perfect-scrollbar'
import 'perfect-scrollbar/css/perfect-scrollbar.css'
import withStyles                  from '@material-ui/core/styles/withStyles'
import Header                      from '../../components/Header/Header.jsx'
import Sidebar                     from '../../components/Sidebar/Sidebar.jsx'
import { connect }                 from 'react-redux'

import dashboardRoutes from '../../routes/dashboard.jsx'

import dashboardStyle from '../../assets/jss/material-dashboard-react/layouts/dashboardStyle.jsx'

import image                                             from '../../assets/img/justin-chrn-1228465-unsplash.jpg'
import { requestEvents, requestTournament, requestType,requestOutComes } from '../../actions'

const switchRoutes = (
  <Switch>
    {dashboardRoutes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key}/>
      return <Route path={prop.path} component={prop.component} key={key}/>
    })}
  </Switch>
)

class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      mobileOpen: false
    }
    this.resizeFunction = this.resizeFunction.bind(this)
  }

  handleDrawerToggle = () => {
    this.setState({mobileOpen: !this.state.mobileOpen})
  }

  getRoute () {
    return this.props.location.pathname !== '/maps'
  }

  resizeFunction () {
    if (window.innerWidth >= 960) {
      this.setState({mobileOpen: false})
    }
  }

  componentDidMount () {
    if (navigator.platform.indexOf('Win') > -1) {
      const ps = new PerfectScrollbar(this.refs.mainPanel)
    }
    window.addEventListener('resize', this.resizeFunction)
    this.props.request()
  }

  componentDidUpdate (e) {
    if (e.history.location.pathname !== e.location.pathname) {
      this.refs.mainPanel.scrollTop = 0
      if (this.state.mobileOpen) {
        this.setState({mobileOpen: false})
      }
    }
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.resizeFunction)
  }

  render () {
    const {classes, ...rest} = this.props
    return (
      <div className={classes.wrapper}>
        <Sidebar
          routes={dashboardRoutes}
          logoText={'Special Bet'}
          image={image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color="red"
          {...rest}
        />
        <div className={classes.mainPanel} ref="mainPanel">
          <Header
            routes={dashboardRoutes}
            handleDrawerToggle={this.handleDrawerToggle}
            {...rest}
          />
          {this.getRoute() ? (
            <div className={classes.content}>
              <div className={classes.container}>{switchRoutes}</div>
            </div>
          ) : (
            <div className={classes.map}>{switchRoutes}</div>
          )}
        </div>
      </div>
    )
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
}

const mapStateToProps = state => {

  return {}
}
const mapDispatchToProps = dispatch => {
  return {
    request: () => {
      dispatch(requestType())
      dispatch(requestTournament())
      dispatch(requestEvents())
      dispatch(requestOutComes())
    },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(dashboardStyle)(App))
