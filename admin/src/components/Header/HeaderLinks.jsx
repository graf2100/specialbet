import React             from 'react'
import classNames        from 'classnames'
import io                from 'socket.io-client'
// @material-ui/core components
import withStyles        from '@material-ui/core/styles/withStyles'
import MenuItem          from '@material-ui/core/MenuItem'
import MenuList          from '@material-ui/core/MenuList'
import Grow              from '@material-ui/core/Grow'
import Paper             from '@material-ui/core/Paper'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Hidden            from '@material-ui/core/Hidden'
import Poppers           from '@material-ui/core/Popper'
// @material-ui/icons
import Notifications     from '@material-ui/icons/Notifications'
// core components
import Button            from '../../components/CustomButtons/Button.jsx'

import headerLinksStyle from '../../assets/jss/material-dashboard-react/components/headerLinksStyle.jsx'
import axios            from 'axios/index'

class HeaderLinks extends React.Component {
  state = {
    open: false,
    cashout: 0,
    baskets: 0,
  }
  handleToggle = () => {
    this.setState(state => ({open: !state.open}))
  }

  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return
    }

    this.setState({open: false})
  }

  componentDidMount () {
    this.socket = io('https://specialone.bet')
    this.socket.on('connect', () => {
      this.socket.emit('getBasket', 1)
    })
    this.socket.on('getBasket', baskets => {
      this.setState({baskets:baskets.length})
    })


    setInterval(async () => {
      let {data: {status, bets}} = await axios.get('/api/cashout/myKeysTop2019')
      if (status === 'Done') {
        this.setState({cashout: bets.length})
      }
    }, 5000)
  }

  render () {
    const {classes} = this.props
    const {open, baskets, cashout} = this.state
    return (
      <div>
        <div className={classes.manager}>
          <Button
            buttonRef={node => {
              this.anchorEl = node
            }}
            color={window.innerWidth > 959 ? 'transparent' : 'white'}
            justIcon={window.innerWidth > 959}
            simple={!(window.innerWidth > 959)}
            aria-owns={open ? 'menu-list-grow' : null}
            aria-haspopup="true"
            onClick={this.handleToggle}
            className={classes.buttonLink}
          >
            <Notifications className={classes.icons}/>
            {(baskets + cashout) ? <span className={classes.notifications}>{baskets + cashout}</span>:null}
            <Hidden mdUp implementation="css">
              <p onClick={this.handleClick} className={classes.linkText}>
                Notification
              </p>
            </Hidden>
          </Button>
          <Poppers
            open={open}
            anchorEl={this.anchorEl}
            transition
            disablePortal
            className={
              classNames({[classes.popperClose]: !open}) +
              ' ' +
              classes.pooperNav
            }
          >
            {({TransitionProps, placement}) => (
              <Grow
                {...TransitionProps}
                id="menu-list-grow"
                style={{
                  transformOrigin:
                    placement === 'bottom' ? 'center top' : 'center bottom'
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={this.handleClose}>
                    <MenuList role="menu">
                      <MenuItem
                        onClick={(e) => {
                          this.handleClose(e)
                          document.location.href = '/request'
                        }}
                        className={classes.dropdownItem}
                      >
                        Запрос на коэффиц. - {baskets}
                      </MenuItem>
                      <MenuItem
                        onClick={(e) => {
                          this.handleClose(e)
                          document.location.href = '/сashout'
                        }}
                        className={classes.dropdownItem}
                      >
                        Запрос на Cashout - {cashout}
                      </MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Poppers>
        </div>
      </div>
    )
  }
}

export default withStyles(headerLinksStyle)(HeaderLinks)
