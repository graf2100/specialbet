const path = require('path')

module.exports = {
  server: {
    port: process.env.PORT || 80,
    https: process.env.HTTPS ||443,
    html: path.join(__dirname, '..', 'client', 'build'),
    admin: path.join(__dirname, '..', 'admin', 'build'),
    secret: 'Nub1yUCm6svxkXJPqIdXmq9uroHCCJ6x',
    mail: {
      user: 'special1bet@gmail.com',
      pass: 'bodyaramos1993'
    },
    LINK_TO_ACTIVE_USER: 'http://specialone.bet/api/active/',
    DATABASE: 'mongodb://localhost/specialbet'
  }
}
